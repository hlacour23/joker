/********************************************************************************
** Form generated from reading UI file 'MidiToolWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MIDITOOLWINDOW_H
#define UI_MIDITOOLWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "PhCommonUI/PhMediaPanel.h"

QT_BEGIN_NAMESPACE

class Ui_MidiToolWindow
{
public:
    QAction *actionSet_TC_In;
    QAction *actionSet_TC_Out;
    QAction *actionPreferences;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QCheckBox *writeMtcCheckBox;
    QGroupBox *writerGroupBox;
    QVBoxLayout *verticalLayout_3;
    QLabel *writerInfoLabel;
    PhMediaPanel *writerMediaPanel;
    QCheckBox *writerLoopPlaybackCheckBox;
    QCheckBox *readMtcCheckBox;
    QGroupBox *readerGroupBox;
    QVBoxLayout *verticalLayout_2;
    QLabel *readerInfoLabel;
    QLabel *readerTimeCodeLabel;
    QLabel *readerFpsLabel;
    QLabel *delayLabel;
    QMenuBar *menuBar;
    QMenu *menuControls;

    void setupUi(QMainWindow *MidiToolWindow)
    {
        if (MidiToolWindow->objectName().isEmpty())
            MidiToolWindow->setObjectName(QStringLiteral("MidiToolWindow"));
        MidiToolWindow->resize(523, 384);
        MidiToolWindow->setStyleSheet(QLatin1String("QGroupBox {\n"
"	background: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 rgba(60, 60, 60, 255), stop:1 rgba(0, 0, 0, 255));\n"
"    border: 1px solid gray;\n"
"    border-radius: 5px;\n"
"    margin-top: 3ex; /* leave space at the top for the title */\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    subcontrol-position: top center; /* position at the top center */\n"
"    padding: 0 10px;\n"
"}\n"
"\n"
"\n"
"QLabel {\n"
"	color: white;\n"
"}\n"
""));
        actionSet_TC_In = new QAction(MidiToolWindow);
        actionSet_TC_In->setObjectName(QStringLiteral("actionSet_TC_In"));
        actionSet_TC_Out = new QAction(MidiToolWindow);
        actionSet_TC_Out->setObjectName(QStringLiteral("actionSet_TC_Out"));
        actionPreferences = new QAction(MidiToolWindow);
        actionPreferences->setObjectName(QStringLiteral("actionPreferences"));
        centralWidget = new QWidget(MidiToolWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        writeMtcCheckBox = new QCheckBox(centralWidget);
        writeMtcCheckBox->setObjectName(QStringLiteral("writeMtcCheckBox"));
        writeMtcCheckBox->setChecked(false);

        verticalLayout->addWidget(writeMtcCheckBox);

        writerGroupBox = new QGroupBox(centralWidget);
        writerGroupBox->setObjectName(QStringLiteral("writerGroupBox"));
        writerGroupBox->setStyleSheet(QLatin1String("QCheckBox {\n"
"	color: white;\n"
"}\n"
""));
        verticalLayout_3 = new QVBoxLayout(writerGroupBox);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        writerInfoLabel = new QLabel(writerGroupBox);
        writerInfoLabel->setObjectName(QStringLiteral("writerInfoLabel"));
        writerInfoLabel->setMaximumSize(QSize(16777215, 20));
        writerInfoLabel->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(writerInfoLabel);

        writerMediaPanel = new PhMediaPanel(writerGroupBox);
        writerMediaPanel->setObjectName(QStringLiteral("writerMediaPanel"));

        verticalLayout_3->addWidget(writerMediaPanel);

        writerLoopPlaybackCheckBox = new QCheckBox(writerGroupBox);
        writerLoopPlaybackCheckBox->setObjectName(QStringLiteral("writerLoopPlaybackCheckBox"));

        verticalLayout_3->addWidget(writerLoopPlaybackCheckBox);


        verticalLayout->addWidget(writerGroupBox);

        readMtcCheckBox = new QCheckBox(centralWidget);
        readMtcCheckBox->setObjectName(QStringLiteral("readMtcCheckBox"));
        readMtcCheckBox->setChecked(false);

        verticalLayout->addWidget(readMtcCheckBox);

        readerGroupBox = new QGroupBox(centralWidget);
        readerGroupBox->setObjectName(QStringLiteral("readerGroupBox"));
        verticalLayout_2 = new QVBoxLayout(readerGroupBox);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        readerInfoLabel = new QLabel(readerGroupBox);
        readerInfoLabel->setObjectName(QStringLiteral("readerInfoLabel"));
        readerInfoLabel->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(readerInfoLabel);

        readerTimeCodeLabel = new QLabel(readerGroupBox);
        readerTimeCodeLabel->setObjectName(QStringLiteral("readerTimeCodeLabel"));
        QFont font;
        font.setFamily(QStringLiteral("Arial Unicode MS"));
        font.setPointSize(45);
        font.setStrikeOut(false);
        readerTimeCodeLabel->setFont(font);
        readerTimeCodeLabel->setText(QStringLiteral("00:00:00:00"));
        readerTimeCodeLabel->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(readerTimeCodeLabel);

        readerFpsLabel = new QLabel(readerGroupBox);
        readerFpsLabel->setObjectName(QStringLiteral("readerFpsLabel"));
        readerFpsLabel->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(readerFpsLabel);


        verticalLayout->addWidget(readerGroupBox);

        delayLabel = new QLabel(centralWidget);
        delayLabel->setObjectName(QStringLiteral("delayLabel"));
        delayLabel->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(delayLabel);

        MidiToolWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MidiToolWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 523, 22));
        menuControls = new QMenu(menuBar);
        menuControls->setObjectName(QStringLiteral("menuControls"));
        MidiToolWindow->setMenuBar(menuBar);

        menuBar->addAction(menuControls->menuAction());
        menuControls->addAction(actionSet_TC_In);
        menuControls->addAction(actionSet_TC_Out);
        menuControls->addAction(actionPreferences);

        retranslateUi(MidiToolWindow);

        QMetaObject::connectSlotsByName(MidiToolWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MidiToolWindow)
    {
        MidiToolWindow->setWindowTitle(QApplication::translate("MidiToolWindow", "Midi Tool", Q_NULLPTR));
        actionSet_TC_In->setText(QApplication::translate("MidiToolWindow", "Set TC In...", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionSet_TC_In->setShortcut(QApplication::translate("MidiToolWindow", "Ctrl+Shift+I", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionSet_TC_Out->setText(QApplication::translate("MidiToolWindow", "Set TC Out...", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionSet_TC_Out->setShortcut(QApplication::translate("MidiToolWindow", "Ctrl+Shift+O", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionPreferences->setText(QApplication::translate("MidiToolWindow", "Preferences...", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionPreferences->setShortcut(QApplication::translate("MidiToolWindow", "Ctrl+P", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        writeMtcCheckBox->setText(QApplication::translate("MidiToolWindow", "Write MTC", Q_NULLPTR));
        writerGroupBox->setTitle(QApplication::translate("MidiToolWindow", "Writer:", Q_NULLPTR));
        writerInfoLabel->setText(QApplication::translate("MidiToolWindow", "00:00:00:00 => 00:00:00:00", Q_NULLPTR));
        writerLoopPlaybackCheckBox->setText(QApplication::translate("MidiToolWindow", "Loop", Q_NULLPTR));
        readMtcCheckBox->setText(QApplication::translate("MidiToolWindow", "Read MTC", Q_NULLPTR));
        readerGroupBox->setTitle(QApplication::translate("MidiToolWindow", "Reader:", Q_NULLPTR));
        readerInfoLabel->setText(QApplication::translate("MidiToolWindow", "0x since 00:00:00:00", Q_NULLPTR));
        readerFpsLabel->setText(QApplication::translate("MidiToolWindow", "0 fps", Q_NULLPTR));
        delayLabel->setText(QApplication::translate("MidiToolWindow", "0 ms", Q_NULLPTR));
        menuControls->setTitle(QApplication::translate("MidiToolWindow", "Controls", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MidiToolWindow: public Ui_MidiToolWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MIDITOOLWINDOW_H

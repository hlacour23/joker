/****************************************************************************
** Meta object code from reading C++ file 'MidiToolWindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.9)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../MidiToolWindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MidiToolWindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.9. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MidiToolWindow_t {
    QByteArrayData data[23];
    char stringdata0[384];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MidiToolWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MidiToolWindow_t qt_meta_stringdata_MidiToolWindow = {
    {
QT_MOC_LITERAL(0, 0, 14), // "MidiToolWindow"
QT_MOC_LITERAL(1, 15, 28), // "on_actionSet_TC_In_triggered"
QT_MOC_LITERAL(2, 44, 0), // ""
QT_MOC_LITERAL(3, 45, 29), // "on_actionSet_TC_Out_triggered"
QT_MOC_LITERAL(4, 75, 30), // "on_actionPreferences_triggered"
QT_MOC_LITERAL(5, 106, 27), // "on_writeMtcCheckBox_clicked"
QT_MOC_LITERAL(6, 134, 7), // "checked"
QT_MOC_LITERAL(7, 142, 26), // "on_readMtcCheckBox_clicked"
QT_MOC_LITERAL(8, 169, 10), // "onGoToTime"
QT_MOC_LITERAL(9, 180, 6), // "PhTime"
QT_MOC_LITERAL(10, 187, 4), // "time"
QT_MOC_LITERAL(11, 192, 19), // "onWriterTimeChanged"
QT_MOC_LITERAL(12, 212, 19), // "updateTCTypeSetting"
QT_MOC_LITERAL(13, 232, 14), // "PhTimeCodeType"
QT_MOC_LITERAL(14, 247, 6), // "tcType"
QT_MOC_LITERAL(15, 254, 17), // "updateRateSetting"
QT_MOC_LITERAL(16, 272, 6), // "PhRate"
QT_MOC_LITERAL(17, 279, 4), // "rate"
QT_MOC_LITERAL(18, 284, 19), // "onReaderTimeChanged"
QT_MOC_LITERAL(19, 304, 19), // "onReaderRateChanged"
QT_MOC_LITERAL(20, 324, 14), // "updateFpsLabel"
QT_MOC_LITERAL(21, 339, 6), // "onTick"
QT_MOC_LITERAL(22, 346, 37) // "on_writerLoopPlaybackCheckBox..."

    },
    "MidiToolWindow\0on_actionSet_TC_In_triggered\0"
    "\0on_actionSet_TC_Out_triggered\0"
    "on_actionPreferences_triggered\0"
    "on_writeMtcCheckBox_clicked\0checked\0"
    "on_readMtcCheckBox_clicked\0onGoToTime\0"
    "PhTime\0time\0onWriterTimeChanged\0"
    "updateTCTypeSetting\0PhTimeCodeType\0"
    "tcType\0updateRateSetting\0PhRate\0rate\0"
    "onReaderTimeChanged\0onReaderRateChanged\0"
    "updateFpsLabel\0onTick\0"
    "on_writerLoopPlaybackCheckBox_toggled"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MidiToolWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   84,    2, 0x08 /* Private */,
       3,    0,   85,    2, 0x08 /* Private */,
       4,    0,   86,    2, 0x08 /* Private */,
       5,    1,   87,    2, 0x08 /* Private */,
       7,    1,   90,    2, 0x08 /* Private */,
       8,    1,   93,    2, 0x08 /* Private */,
      11,    1,   96,    2, 0x08 /* Private */,
      12,    1,   99,    2, 0x08 /* Private */,
      15,    1,  102,    2, 0x08 /* Private */,
      18,    1,  105,    2, 0x08 /* Private */,
      19,    1,  108,    2, 0x08 /* Private */,
      20,    1,  111,    2, 0x08 /* Private */,
      21,    0,  114,    2, 0x08 /* Private */,
      22,    1,  115,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    6,
    QMetaType::Void, QMetaType::Bool,    6,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void, 0x80000000 | 13,   14,
    QMetaType::Void, 0x80000000 | 16,   17,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void, 0x80000000 | 16,   17,
    QMetaType::Void, 0x80000000 | 13,   14,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    6,

       0        // eod
};

void MidiToolWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MidiToolWindow *_t = static_cast<MidiToolWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_actionSet_TC_In_triggered(); break;
        case 1: _t->on_actionSet_TC_Out_triggered(); break;
        case 2: _t->on_actionPreferences_triggered(); break;
        case 3: _t->on_writeMtcCheckBox_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->on_readMtcCheckBox_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->onGoToTime((*reinterpret_cast< PhTime(*)>(_a[1]))); break;
        case 6: _t->onWriterTimeChanged((*reinterpret_cast< PhTime(*)>(_a[1]))); break;
        case 7: _t->updateTCTypeSetting((*reinterpret_cast< PhTimeCodeType(*)>(_a[1]))); break;
        case 8: _t->updateRateSetting((*reinterpret_cast< PhRate(*)>(_a[1]))); break;
        case 9: _t->onReaderTimeChanged((*reinterpret_cast< PhTime(*)>(_a[1]))); break;
        case 10: _t->onReaderRateChanged((*reinterpret_cast< PhRate(*)>(_a[1]))); break;
        case 11: _t->updateFpsLabel((*reinterpret_cast< PhTimeCodeType(*)>(_a[1]))); break;
        case 12: _t->onTick(); break;
        case 13: _t->on_writerLoopPlaybackCheckBox_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject MidiToolWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MidiToolWindow.data,
      qt_meta_data_MidiToolWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MidiToolWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MidiToolWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MidiToolWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MidiToolWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 14)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 14;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE

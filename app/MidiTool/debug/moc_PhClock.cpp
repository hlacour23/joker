/****************************************************************************
** Meta object code from reading C++ file 'PhClock.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.9)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../libs/PhSync/PhClock.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'PhClock.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.9. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_PhClock_t {
    QByteArrayData data[10];
    char stringdata0[76];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PhClock_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PhClock_t qt_meta_stringdata_PhClock = {
    {
QT_MOC_LITERAL(0, 0, 7), // "PhClock"
QT_MOC_LITERAL(1, 8, 11), // "timeChanged"
QT_MOC_LITERAL(2, 20, 0), // ""
QT_MOC_LITERAL(3, 21, 6), // "PhTime"
QT_MOC_LITERAL(4, 28, 4), // "time"
QT_MOC_LITERAL(5, 33, 11), // "rateChanged"
QT_MOC_LITERAL(6, 45, 6), // "PhRate"
QT_MOC_LITERAL(7, 52, 4), // "rate"
QT_MOC_LITERAL(8, 57, 6), // "elapse"
QT_MOC_LITERAL(9, 64, 11) // "elapsedTime"

    },
    "PhClock\0timeChanged\0\0PhTime\0time\0"
    "rateChanged\0PhRate\0rate\0elapse\0"
    "elapsedTime"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PhClock[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   29,    2, 0x06 /* Public */,
       5,    1,   32,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    1,   35,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 6,    7,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    9,

       0        // eod
};

void PhClock::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        PhClock *_t = static_cast<PhClock *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->timeChanged((*reinterpret_cast< PhTime(*)>(_a[1]))); break;
        case 1: _t->rateChanged((*reinterpret_cast< PhRate(*)>(_a[1]))); break;
        case 2: _t->elapse((*reinterpret_cast< PhTime(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (PhClock::*_t)(PhTime );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PhClock::timeChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (PhClock::*_t)(PhRate );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PhClock::rateChanged)) {
                *result = 1;
                return;
            }
        }
    }
}

const QMetaObject PhClock::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_PhClock.data,
      qt_meta_data_PhClock,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *PhClock::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PhClock::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PhClock.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int PhClock::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void PhClock::timeChanged(PhTime _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void PhClock::rateChanged(PhRate _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE

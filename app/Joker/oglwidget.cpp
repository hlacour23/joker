#include "oglwidget.h"
#include <QCloseEvent>
#include <QMessageBox>
#include <QFileDialog>
#include <QFontDatabase>
#include <thread>
#include <iostream>
#include <string>
#include <d3d11.h>

#include <dwmapi.h>
#include <QLibrary>


#include "PhTools/PhDebug.h"

#include "PhCommonUI/PhTimeCodeDialog.h"
#include "PhCommonUI/PhFeedbackDialog.h"

#include "PhGraphic/PhGraphicText.h"
#include "PhGraphic/PhGraphicSolidRect.h"
#include "PhGraphic/PhGraphicRect.h"
#include "PhGraphic/PhFont.h"

#include "JokerWindow.h"
#include "ui_JokerWindow.h"
#include "player.h"
#include <vlc/vlc.h>
#include <QOpenGLWidget>
#include <QGLWidget>

#include "AboutDialog.h"
#include "PreferencesDialog.h"
#include "PeopleDialog.h"
#include <windows.h>
#include <dwmapi.h>
#include "GL\gl.h"
#include "GL\glext.h"

OGLWidget::OGLWidget(QWidget *parent, PhGraphicText _titleText, PhGraphicText _tcText, PhGraphicSolidRect _outsideLoopRect, PhGraphicText _gCurrentLoop, PhGraphicText _nextTCText, PhGraphicText _errorText, QList<QString> _peoples, int _widthS, int _heightS)
    : QGLWidget(parent),
      titleText(_titleText),
      tcText(_tcText),
      outsideLoopRect(_outsideLoopRect),
      gCurrentLoop(_gCurrentLoop),
      nextTCText(_nextTCText),
      errorText(_errorText),
      peoples(_peoples),
      widthS(_widthS),
      heightS(_heightS)
{
    //_font = font;
    printf("saluuuuuuuuuuut" + _titleText.content().toLatin1());
    //_titleText.draw();
    /*setTcText(_tcText);
    setOutsideLoopRect(_outsideLoopRect);
    setGCurrentLoop(_gCurrentLoop);
    setNextTCText(_nextTCText);
    setErrorText(_errorText);*/
}

OGLWidget::~OGLWidget()
{

}

void OGLWidget::setTitleText(PhGraphicText _titleText)
{
    titleText = _titleText;
    title = 1;
}

void OGLWidget::setTcText(PhGraphicText _tcText)
{
    tcText = _tcText;
    tc = 1;
}

void OGLWidget::setOutsideLoopRect(PhGraphicSolidRect _outsideLoopRect)
{
    outsideLoopRect = _outsideLoopRect;
    rect = 1;
}

void OGLWidget::setGCurrentLoop(PhGraphicText _gCurrentLoop)
{
    gCurrentLoop = _gCurrentLoop;
    g = 1;
}

void OGLWidget::setNextTCText(PhGraphicText _nextTCText)
{
    nextTCText = _nextTCText;
    next = 1;
}

void OGLWidget::setErrorText(PhGraphicText _errorText)
{
    errorText = _errorText;
    error = 1;
}

void OGLWidget::setPeoples(QList<QString> _peoples)
{
    peoples = _peoples;
}

void OGLWidget::clean()
{
    title = 0;
    tc = 0;
    rect = 0;
    g = 0;
    next = 0;
    error = 0;
}


void OGLWidget::initializeGL()
{
    glClearColor(0, 0, 0, 1);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);
}

void OGLWidget::setTest(QString content)
{
    _content = content;
}

void OGLWidget::paintGL()
{
    /*PhGraphicRect::draw();

    _font->select();

    int totalAdvance = 0;
    //Compute the natural width of the content to scale it later
    for(int i = 0; i < _content.length(); i++) {
        totalAdvance += _font->getAdvance(_content.at(i).toLatin1());
        if(_content.at(i).unicode() == 339)
            totalAdvance += _font->getAdvance(153);
    }

    if((totalAdvance == 0) || (_font->height() == 0)) {
        // empty string or bad font initialization: displaying a rect
        glBegin(GL_QUADS);
        {
            glVertex3i(0,      0, 0);
            glVertex3i(0 + 1000,  0, 0);
            glVertex3i(0 + 1000,  0 + 800,  0);
            glVertex3i(0,      0 +800,  0);
        }
        glEnd();

        return;
    }

    glEnable(GL_TEXTURE_2D);

    glEnable(GL_BLEND);

    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

    // Set the letter initial horizontal offset
    int currentAdvance = 0;
    float space = 0.0625f; // all glyph are in a 1/16 x 1/16 box
    // Display a string
    for(int i = 0; i < _content.length(); i++) {
        QChar qChar = _content.at(i);
        unsigned char ch = (unsigned char)qChar.toLatin1();
        int glyphAdvance = _font->getAdvance(ch);
        if (ch == 0)
            PHERR << "Unhandled character:" << qChar << "/" << qChar.unicode();
        else if(glyphAdvance) {
            // computing texture coordinates
            float tu1 = (ch % 16) * space;
            float tv1 = (ch / 16) * space;
            float tu2 = tu1 + space;
            float tv2 = tv1 + space;

            // computing quads coordinate;
            int h = 800;
            int w = 1000 * 128 / totalAdvance;

            //        (tu1, tv1) --- (tu2, tv1)
            //            |              |
            //            |              |
            //        (tu1, tv2) --- (tu2, tv2)


            int offset = (128 - glyphAdvance) / 2 * 1000 / totalAdvance;
            int glyphX = 0 + currentAdvance * 1000 / totalAdvance - offset;
            glBegin(GL_QUADS);  //Begining the cube's drawing
            {
                glTexCoord3f(tu1, tv1, 1);  glVertex3i(glyphX,      0, 0);
                glTexCoord3f(tu2, tv1, 1);  glVertex3i(glyphX + w,  0, 0);
                glTexCoord3f(tu2, tv2, 1);  glVertex3i(glyphX + w,  0 + h,  0);
                glTexCoord3f(tu1, tv2, 1);  glVertex3i(glyphX,      0 + h,  0);
            }
            glEnd();

        }
        // Inc the advance
        currentAdvance += glyphAdvance;
    }

    glDisable(GL_BLEND);

    glDisable(GL_TEXTURE_2D);*/

    /*glBegin(GL_TRIANGLES);
        glColor3f(1.0, 0.0, 0.0);
        glVertex3f(-0.5, -0.5, 0);
        glColor3f(0.0, 1.0, 0.0);
        glVertex3f( 0.5, -0.5, 0);
        glColor3f(0.0, 0.0, 1.0);
        glVertex3f( 0.0,  0.5, 0);
    glEnd();*/
    //emit paint();
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f );
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    QFont *font = new QFont;
    printf("heyyyyyyyyyyyyyyyyyy");
    printf(titleText.content().toLatin1());
    int test = 150;
    QString print;
    renderText(100, 100, titleText.content().toLatin1());
    foreach(QString name, peoples) {
        renderText(100, test, name);
        test += 50;
    }


    //if (tc == 1)
        //tcText.draw();
    //if (rect == 1)
        //outsideLoopRect.draw();
    //if (g == 1)
        //gCurrentLoop.draw();
    //if (next == 1)
        //nextTCText.draw();
    //if (error == 1)
        //errorText.draw();
    clean();
}

void OGLWidget::paint()
{
    paintGL();
}

void OGLWidget::resizeGL(int w, int h)
{
    glViewport(0,0,w,h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    //gluPerspective(45, (float)w/h, 0.01, 100.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    //gluLookAt(0,0,5,0,0,0,0,1,0);
}

/****************************************************************************
** Meta object code from reading C++ file 'PhGraphicView.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.9)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../libs/PhGraphic/PhGraphicView.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'PhGraphicView.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.9. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_PhGraphicView_t {
    QByteArrayData data[9];
    char stringdata0[75];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PhGraphicView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PhGraphicView_t qt_meta_stringdata_PhGraphicView = {
    {
QT_MOC_LITERAL(0, 0, 13), // "PhGraphicView"
QT_MOC_LITERAL(1, 14, 11), // "beforePaint"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 6), // "PhTime"
QT_MOC_LITERAL(4, 34, 11), // "elapsedTime"
QT_MOC_LITERAL(5, 46, 5), // "paint"
QT_MOC_LITERAL(6, 52, 5), // "width"
QT_MOC_LITERAL(7, 58, 6), // "height"
QT_MOC_LITERAL(8, 65, 9) // "onRefresh"

    },
    "PhGraphicView\0beforePaint\0\0PhTime\0"
    "elapsedTime\0paint\0width\0height\0onRefresh"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PhGraphicView[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   29,    2, 0x06 /* Public */,
       5,    2,   32,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    0,   37,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    6,    7,

 // slots: parameters
    QMetaType::Void,

       0        // eod
};

void PhGraphicView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        PhGraphicView *_t = static_cast<PhGraphicView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->beforePaint((*reinterpret_cast< PhTime(*)>(_a[1]))); break;
        case 1: _t->paint((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: _t->onRefresh(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (PhGraphicView::*_t)(PhTime );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PhGraphicView::beforePaint)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (PhGraphicView::*_t)(int , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PhGraphicView::paint)) {
                *result = 1;
                return;
            }
        }
    }
}

const QMetaObject PhGraphicView::staticMetaObject = {
    { &QGLWidget::staticMetaObject, qt_meta_stringdata_PhGraphicView.data,
      qt_meta_data_PhGraphicView,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *PhGraphicView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PhGraphicView::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PhGraphicView.stringdata0))
        return static_cast<void*>(this);
    return QGLWidget::qt_metacast(_clname);
}

int PhGraphicView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGLWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void PhGraphicView::beforePaint(PhTime _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void PhGraphicView::paint(int _t1, int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE

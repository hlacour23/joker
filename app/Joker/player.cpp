/******************************
 * Qt player using libVLC     *
 * By protonux                *
 *                            *
 * Under WTFPL                *
 ******************************/

#include "player.h"
#include <vlc/vlc.h>

#define qtu( i ) ((i).toUtf8().constData())

#include <QtGui>
#include <QMessageBox>
#include <QMenuBar>
#include <QAction>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFileDialog>
#include <QMessageBox>
#include <QPalette>

Mwindow::Mwindow() {
    const char * const args[] =
    {
        "--intf=dummy",        /* No interface     */
    #ifdef Q_OS_WIN
        "--dummy-quiet",       /* No command-line  */
    #elif defined(Q_OS_MAC)
        "--vout=macosx",
    #endif
        "--ignore-config",     /* No configuration */
        "--no-spu",            /* No sub-pictures  */
        "--no-osd",            /* No video overlay */
        "--no-stats",          /* No statistics    */
        "--no-media-library",  /* No Media Library */
    };

    vlcPlayer = NULL;
    p_window = NULL;

    /* Initialize libVLC */
    printf("________________");
    vlcInstance = libvlc_new(sizeof(args) / sizeof(*args), args);
    printf("________________");
    /* Complain in case of broken installation */
    if (vlcInstance == NULL) {
        QMessageBox::critical(this, "Qt libVLC player", "Could not init libVLC");
        exit(1);
    }
    printf("________________");
    initUI();
}

Mwindow::~Mwindow() {
    /* Release libVLC instance on quit */
    if (vlcInstance)
        libvlc_release(vlcInstance);
}

void Mwindow::initUI() {
    videoWidget = new QWidget(nullptr, Qt::Tool | Qt::FramelessWindowHint );
    videoWidget->setAutoFillBackground( true );
    QPalette plt = palette();
    plt.setColor( QPalette::Window, Qt::black );
    videoWidget->setPalette( plt );

    fileOpen = "file:///" + QFileDialog::getOpenFileName(this, tr("Load a file"), "~");

    if (vlcPlayer && libvlc_media_player_is_playing(vlcPlayer))
        libvlc_media_player_stop(vlcPlayer);

    vlcMedia = libvlc_media_new_location(vlcInstance, fileOpen.toLatin1());
    if (!vlcMedia) {
        return;
    }

    vlcPlayer = libvlc_media_player_new_from_media (vlcMedia);
    //resize(300, 200);
    //updateGeometry();
    //setCentralWidget((videoWidget));
    //libvlc_media_player_set_hwnd(vlcPlayer, (HWND)videoWidget->winId());
    //setCentralWidget((videoWidget));
    //libvlc_media_player_play(vlcPlayer);

    //videoWidget->resize(300, 200);
    //ui->centralwidget->setFixedSize(QSize(600, 400));
    //ui->centralwidget->setMaximumSize(QSize(300, 200));
    //adjustSize();
   //setCentralWidget((videoWidget));
   //libvlc_media_player_play(vlcPlayer);

   //QWidget *widget = new QWidget;
   //QLabel *test = new QLabel;
   /*widget->setMaximumHeight(600);
   widget->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
   QVBoxLayout *w_layout = new QVBoxLayout;
   widget->setLayout(w_layout);
   QHBoxLayout *top_layout = new QHBoxLayout;
   //top_layout->addWidget(("Title"));
   QPushButton *toggle_button = new QPushButton("Toggle");
   top_layout->addWidget(toggle_button);
   toggle_button->setCheckable(true);
   //QTextEdit *text_edit = new QTextEdit;
   //connect(toggle_button, SIGNAL(clicked(bool)), text_edit, SLOT(setHidden(bool)));
   w_layout->addLayout(top_layout);
   //w_layout->addWidget(text_edit);
   w_layout->addWidget(videoWidget);

   //centralWidget()->layout()->addWidget(widget);
   setCentralWidget(widget);*/
   //libvlc_media_player_play(vlcPlayer);*/
}

void Mwindow::openFile() {

    /* The basic file-select box */
    QString fileOpen = QFileDialog::getOpenFileName(this, tr("Load a file"), "~");

    /* Stop if something is playing */
    if (vlcPlayer && libvlc_media_player_is_playing(vlcPlayer))
        stop();

    /* Create a new Media */
    libvlc_media_t *vlcMedia = libvlc_media_new_path(vlcInstance, qtu(fileOpen));
    if (!vlcMedia)
        return;

    /* Create a new libvlc player */
    vlcPlayer = libvlc_media_player_new_from_media (vlcMedia);

    /* Release the media */
    libvlc_media_release(vlcMedia);

    /* Integrate the video in the interface */
#if defined(Q_OS_MAC)
    libvlc_media_player_set_nsobject(vlcPlayer, (void *)videoWidget->winId());
#elif defined(Q_OS_UNIX)
    libvlc_media_player_set_xwindow(vlcPlayer, videoWidget->winId());
#elif defined(Q_OS_WIN)
    libvlc_media_player_set_hwnd(vlcPlayer, (HWND)videoWidget->winId());
#endif

    /* And start playback */
    libvlc_media_player_play (vlcPlayer);

    /* Update playback button */
    playBut->setText("Pause");
}

void Mwindow::play() {
    if (!vlcPlayer)
        return;

    if (libvlc_media_player_is_playing(vlcPlayer))
    {
        /* Pause */
        libvlc_media_player_pause(vlcPlayer);
        playBut->setText("Play");
    }
    else
    {
        /* Play again */
        libvlc_media_player_play(vlcPlayer);
        playBut->setText("Pause");
    }
}

int Mwindow::changeVolume(int vol) { /* Called on volume slider change */

    if (vlcPlayer)
        return libvlc_audio_set_volume (vlcPlayer,vol);

    return 0;
}

void Mwindow::changePosition(int pos) { /* Called on position slider change */

    if (pos == 0)
        return;
    //if (vlcPlayer)
        //libvlc_media_player_set_position(vlcPlayer, (float)pos/1000.0, true);
}

void Mwindow::updateInterface() { //Update interface and check if song is finished

    if (!vlcPlayer)
        return;

    /* update the timeline */
    float pos = libvlc_media_player_get_position(vlcPlayer);
    slider->setValue((int)(pos*1000.0));

    /* Stop the media */
    if (libvlc_media_player_get_state(vlcPlayer) == libvlc_Stopped)
        this->stop();
}

void Mwindow::stop() {
    if(vlcPlayer) {
        /* stop the media player */
        libvlc_media_player_stop(vlcPlayer);

        /* release the media player */
        libvlc_media_player_release(vlcPlayer);

        /* Reset application values */
        vlcPlayer = NULL;
        slider->setValue(0);
        playBut->setText("Play");
    }
}

void Mwindow::mute() {
    if(vlcPlayer) {
        if(volumeSlider->value() == 0) { //if already muted...

                this->changeVolume(80);
                volumeSlider->setValue(80);

        } else { //else mute volume

                this->changeVolume(0);
                volumeSlider->setValue(0);

        }
    }
}

void Mwindow::about()
{
    QMessageBox::about(this, "Qt libVLC player demo", QString::fromUtf8(libvlc_get_version()) );
}

void Mwindow::fullscreen()
{
   if (isFullScreen()) {
       showNormal();
       menuWidget()->show();
   }
   else {
       showFullScreen();
       menuWidget()->hide();
   }
}

void Mwindow::closeEvent(QCloseEvent *event) {
    stop();
    event->accept();
}

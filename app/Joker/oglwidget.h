#ifndef OGLWIDGET_H
#define OGLWIDGET_H

#include <QWidget>
#include <QOpenGLWidget>
#include <QCloseEvent>
#include <QMessageBox>
#include <QFileDialog>
#include <QFontDatabase>
#include <thread>
#include <iostream>
#include <string>
#include <d3d11.h>

#include <dwmapi.h>
#include <QLibrary>


#include "PhTools/PhDebug.h"

#include "PhCommonUI/PhTimeCodeDialog.h"
#include "PhCommonUI/PhFeedbackDialog.h"

#include "PhGraphic/PhGraphicText.h"
#include "PhGraphic/PhGraphicSolidRect.h"
#include "PhStrip/PhPeople.h"

#include "JokerWindow.h"
#include "ui_JokerWindow.h"
#include "player.h"
#include <vlc/vlc.h>

#include "AboutDialog.h"
#include "PreferencesDialog.h"
#include "PeopleDialog.h"
#include <windows.h>
#include <dwmapi.h>
#include <QGLWidget>


class OGLWidget : public QGLWidget
{
public:
    OGLWidget(QWidget *parent = 0, PhGraphicText titleText = 0, PhGraphicText tcText = 0, PhGraphicSolidRect outsideLoopRect = 0, PhGraphicText gCurrentLoop = 0, PhGraphicText nextTCText = 0, PhGraphicText errorText = 0, QList<QString> _peoples = QList<QString>(), int _widthS = 0, int _heightS = 0);
    ~OGLWidget();

    void setTitleText(PhGraphicText _titleText);
    void setTcText(PhGraphicText _tcText);
    void setOutsideLoopRect(PhGraphicSolidRect _outsideLoopRect);
    void setGCurrentLoop(PhGraphicText _gCurrentLoop);
    void setNextTCText(PhGraphicText _nextTCText);
    void setErrorText(PhGraphicText _errorText);
    void setTest(QString _content);
    void setPeoples(QList<QString> _peoples);
    //void paint();
    void clean();
signals:
    /**
     * @brief emit a signal just before the paint
     * @param elapsedTime the time elapsed since the last paint event
     */
    void beforePaint(PhTime elapsedTime);

    /**
     * @brief paint event, every class have to re-implement it.
     * @param width Width of the paint area
     * @param height Height of the paint area
     */
    void paint();
protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();
private:
    //PhGraphicStrip _strip;
    PhGraphicText titleText;
    PhGraphicText tcText = 0;
    PhGraphicSolidRect outsideLoopRect;
    PhGraphicText gCurrentLoop;
    PhGraphicText nextTCText;
    PhGraphicText errorText;
    QList<QString> peoples;
    int widthS;
    int heightS;
    int title, tc, rect, g, next, error = 0;
    //PhFont *_font;
    QString _content = PhFont::filter("test");
};

#endif // OGLWIDGET_H

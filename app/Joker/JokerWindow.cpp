/**
 * @file
 * @copyright (C) 2012-2014 Phonations
 * @license http://www.gnu.org/licenses/gpl.html GPL version 2 or higher
 */
#define qtu( i ) ((i).toUtf8().constData())
#include <QCloseEvent>
#include <QMessageBox>
#include <QFileDialog>
#include <QFontDatabase>
#include <thread>
#include <iostream>
#include <string>
#include <d3d11.h>

#include <dwmapi.h>
#include <QLibrary>


#include "PhTools/PhDebug.h"

#include "PhCommonUI/PhTimeCodeDialog.h"
#include "PhCommonUI/PhFeedbackDialog.h"

#include "PhGraphic/PhGraphicText.h"
#include "PhGraphic/PhGraphicSolidRect.h"

#include "JokerWindow.h"
#include "ui_JokerWindow.h"
#include "player.h"
#include <vlc/vlc.h>
#include <QOpenGLWidget>

#include "AboutDialog.h"
#include "PreferencesDialog.h"
#include "PeopleDialog.h"
#include <windows.h>
#include <dwmapi.h>


#include "oglwidget.h"

JokerWindow::JokerWindow(JokerSettings *settings) :
	PhEditableDocumentWindow(settings),
	ui(new Ui::JokerWindow),
    _settings(settings),
    _strip(settings),
	_doc(_strip.doc()),
#ifdef USE_VIDEO
	_videoEngine(settings),
	_secondScreenWindow(NULL),
#endif

	_synchronizer(settings),
#ifdef USE_SONY
	_sonySlave(settings),
#endif
#ifdef USE_LTC
	_ltcReader(settings),
#endif
#ifdef USE_MIDI
	_mtcReader(PhTimeCodeType25),
	_mtcWriter(PhTimeCodeType25),
#endif
	_firstDoc(true),
	_resizingStrip(false),
	_setCurrentTimeToVideoTimeIn(false),
	_syncTimeInToDoc(false),
	_timePlayed(settings->timePlayed())
{
	// Setting up UI
	ui->setupUi(this);
	QFontDatabase::addApplicationFont(QCoreApplication::applicationDirPath() + PATH_TO_RESSOURCES + "/Bookerly-BoldItalic.ttf");
	QFontDatabase::addApplicationFont(QCoreApplication::applicationDirPath() + PATH_TO_RESSOURCES + "/Cappella-Regular.ttf");

	// Due to translation, Qt might not be able to link automatically the menu
	ui->actionPreferences->setMenuRole(QAction::PreferencesRole);
	ui->actionAbout->setMenuRole(QAction::AboutRole);
	connect(ui->actionFullscreen, &QAction::triggered, this, &JokerWindow::toggleFullScreen);
    connect(ui->playButton, &QPushButton::clicked, this, &JokerWindow::onChangePlayButton);
    connect(ui->videoSlider, &QSlider::sliderMoved, this, &JokerWindow::onChangeVideoslider);
    connect(ui->videoSlider, &QSlider::sliderPressed, this, &JokerWindow::onPressVideoSlider);
    connect(ui->videoSlider, &QSlider::sliderReleased, this,  &JokerWindow::onReleaseVideoSlider);

	this->restoreGeometry(_settings->windowGeometry());
	_mediaPanel.move(this->x() + this->width() / 2 - _mediaPanel.width() / 2,
	                 this->y() + this->height() * 2 / 3);

	ui->videoStripView->setGraphicSettings(_settings);

	// Initialize the synchronizer
    _synchronizer.setStripClock(_strip.clock());

	// Initialize the property dialog
    _propertyDialog.setDoc(_doc);

#ifdef USE_VLC
    const char * const args[] =
    {
        "--intf=dummy",        /* No interface     */
    #ifdef Q_OS_WIN
        "--dummy-quiet",       /* No command-line  */
    #elif defined(Q_OS_MAC)
        "--vout=macosx",
    #endif
        "--ignore-config",     /* No configuration */
        "--no-spu",            /* No sub-pictures  */
        "--no-osd",            /* No video overlay */
        "--no-stats",          /* No statistics    */
        "--no-media-library",  /* No Media Library */
    };
    vlcInstance = libvlc_new(sizeof(args) / sizeof(*args), args);
    if (vlcInstance == NULL) {
        QMessageBox::critical(this, "Qt libVLC player", "Could not init libVLC");
        exit(1);
    }


#endif

#ifdef USE_SONY
	connect(&_sonySlave, &PhSonySlaveController::videoSync, this, &JokerWindow::onVideoSync);
#endif

#ifdef USE_LTC
	connect(&_ltcReader, &PhLtcReader::timeCodeTypeChanged, this, &JokerWindow::onTimecodeTypeChanged);
#endif

#ifdef USE_MIDI
	_mtcReader.force24as2398(_settings->mtcForce24as2398());
	connect(&_mtcReader, &PhMidiTimeCodeReader::timeCodeTypeChanged, this, &JokerWindow::onTimecodeTypeChanged);
#endif // USE_MIDI

	setupSyncProtocol();

	// Setting up the media panel
	_mediaPanel.setClock(_strip.clock());
	_mediaPanel.setDropdownEnable(false);
	onTimecodeTypeChanged(this->synchroTimeCodeType());

    ui->actionDisplay_the_control_panel->setChecked(_settings->displayControlPanel());

	showMediaPanel();

	this->setFocus();

	ui->actionDisplay_the_information_panel->setChecked(_settings->displayNextText());

	ui->actionTest_mode->setChecked(_settings->stripTestMode());

	ui->actionLoop->setChecked(_settings->syncLooping());

	ui->actionInvert_colors->setChecked(_settings->invertColor());

	ui->actionHide_the_rythmo->setChecked(_settings->hideStrip());

	ui->actionDisplay_the_cuts->setChecked(_settings->displayCuts());

	ui->actionDisplay_the_vertical_scale->setChecked(_settings->displayVerticalScale());

	on_actionDisplay_feet_triggered(_settings->displayFeet());


	this->connect(ui->videoStripView, &PhGraphicView::beforePaint, this, &JokerWindow::timeCounter);
	this->connect(ui->videoStripView, &PhGraphicView::beforePaint, _strip.clock(), &PhClock::elapse);

    this->connect(ui->videoStripView, &PhGraphicView::paint, this, &JokerWindow::onPaint);

	_videoLogo.setFilename(":/Joker/joker_gray");
	_videoLogo.setTransparent(true);

}

JokerWindow::~JokerWindow()
{
	_mediaPanel.close();

	delete ui;
}

void JokerWindow::MyTimerSlot()
{
    QScreen *screen = QGuiApplication::primaryScreen();
    QRect  screenGeometry = screen->geometry();
    int heightScreen = screenGeometry.height();
    int widthScreen = screenGeometry.width();
    onPaint(widthScreen, heightScreen);
}

void play(QPushButton *playBut,libvlc_media_player_t* vlcPlayer) {
    if (!vlcPlayer)
        return;

    if (libvlc_media_player_is_playing(vlcPlayer))
    {
        /* Pause */
        libvlc_media_player_pause(vlcPlayer);
        playBut->setText("Play");
    }
    else
    {
        /* Play again */
        libvlc_media_player_play(vlcPlayer);
        playBut->setText("Pause");
    }
}

void JokerWindow::closeEvent(QCloseEvent *event)
{
	// The user will be asked if the document has to be saved
	PhEditableDocumentWindow::closeEvent(event);

	// If the close operation is not cancelled by the user
	if (event->isAccepted()) {
		// The media panel has to be closed manually, or the application
		// will stay open forever in the background
		_mediaPanel.close();

        /*if(_secondScreenWindow) {
			_secondScreenWindow->close();
			delete _secondScreenWindow;
			_secondScreenWindow = NULL;
        }*/

		this->disconnect(ui->videoStripView, &PhGraphicView::beforePaint, this, &JokerWindow::timeCounter);
		this->disconnect(ui->videoStripView, &PhGraphicView::beforePaint, _strip.clock(), &PhClock::elapse);

        this->disconnect(ui->videoStripView, &PhGraphicView::paint, this, &JokerWindow::onPaint);


		// Force doc to unmodified to avoid double confirmation
		// since closeEvent is called twice
		// https://bugreports.qt.io/browse/QTBUG-43344
		_doc->setModified(false);

		_settings->setTimePlayed(_timePlayed);
	}
}

void JokerWindow::setupSyncProtocol()
{
	PhClock* clock = NULL;
	QString mtcPortName;

	// Disable old protocol
#ifdef USE_SONY
	_sonySlave.close();
#endif
#ifdef USE_LTC
	_ltcReader.close();
#endif
#ifdef USE_MIDI
	_mtcReader.close();
	_mtcWriter.close();
#endif

	PhSynchronizer::SyncType type = (PhSynchronizer::SyncType)_settings->synchroProtocol();

	switch(type) {
#ifdef USE_SONY
	case PhSynchronizer::Sony:
		// Initialize the sony module
		if(_sonySlave.open()) {
			clock = _sonySlave.clock();
			_lastVideoSyncElapsed.start();
			onTimecodeTypeChanged((PhTimeCodeType)_settings->sonySlaveCommunicationTimeCodeType());
		}
		else {
			type = PhSynchronizer::NoSync;
			QMessageBox::critical(this, tr("Error"), tr("Unable to connect to USB422v module"));
		}
		break;
#endif
	case PhSynchronizer::LTC:
#ifdef USE_LTC
		_ltcReader.setTimeCodeType((PhTimeCodeType)_settings->ltcReaderTimeCodeType());
		if(_ltcReader.init(_settings->ltcInputPort()))
			clock = _ltcReader.clock();
		else {
			QMessageBox::critical(this, tr("Error"), QString(tr("Unable to open %0")).arg(_settings->ltcInputPort()));
			type = PhSynchronizer::NoSync;
		}
		break;
#endif
#ifdef USE_MIDI
	case PhSynchronizer::MTC:
		if (_settings->mtcInputUseExistingPort()) {
			mtcPortName = _settings->mtcInputPort();
		}
		else {
			mtcPortName = _settings->mtcVirtualInputPort();
		}

		if(_mtcReader.open(mtcPortName))
			clock = _mtcReader.clock();
		else {
			QMessageBox::critical(this, tr("Error"), QString(tr("Unable to open %0 midi port")).arg(mtcPortName));
			type = PhSynchronizer::NoSync;
		}
		break;
#endif
	default:
		type = PhSynchronizer::NoSync;
		break;
	}

#ifdef USE_MIDI
	if(_settings->sendMmcMessage()) {
		if(!_mtcWriter.open(_settings->mmcOutputPort())) {
			QMessageBox::critical(this, tr("Error"), QString(tr("Unable to open %0 midi port")).arg(_settings->mmcOutputPort()));
			_settings->setSendMmcMessage(false);
		}
	}
#endif

	_synchronizer.setSyncClock(clock, type);

	// Disable slide if Joker is sync to a protocol
	_mediaPanel.setSliderEnable(clock == NULL);

	_settings->setSynchroProtocol(type);
}

bool JokerWindow::openDocument(const QString &fileName)
{
	QFileInfo info(fileName);
	if(_settings->videoFileType().contains(info.suffix().toLower())) {
#ifdef USE_VIDEO
		return openVideoFile(fileName);
#else
		return false;
#endif
	}

	/// Clear the selected people name list (except for the first document).
	if(!_firstDoc && (fileName != _settings->currentDocument()))
		_settings->setSelectedPeopleNameList(QStringList());
	else
		_firstDoc = false;
	if(!_doc->openStripFile(fileName))
		return false;
	/// If the document is opened successfully :
	/// - Update the current document name (settings, windows title)
    ///
    QString fileOpen = "file:///" + _doc->videoFilePath();

   if (vlcPlayer && libvlc_media_player_is_playing(vlcPlayer))
        libvlc_media_player_pause(vlcPlayer);

    vlcMedia = libvlc_media_new_location(vlcInstance, fileOpen.toLatin1());
    if (!vlcMedia) {
        return false;
    }

    vlcPlayer = libvlc_media_player_new_from_media (vlcMedia);
    libvlc_media_player_set_hwnd(vlcPlayer, (HWND)ui->vlcWidget->winId());

	PhEditableDocumentWindow::openDocument(fileName);
	if(fileName != _doc->filePath()) {
		PHDEBUG << "Adding to watch: " << _doc->filePath();
		_watcher.addPath(_doc->filePath());
	}

    libvlc_media_player_play(vlcPlayer);
	return true;
}

void JokerWindow::saveDocument(const QString &fileName)
{
	// prevent from reloading the document when we are the ones changing it
	bool updateWatcher;
	if (_watcher.files().contains(fileName)) {
		_watcher.removePath(_doc->filePath());
		updateWatcher = true;
	}

	if(_doc->exportDetXFile(fileName, currentTime())) {
		_doc->setModified(false);
		PhEditableDocumentWindow::saveDocument(fileName);

		if (updateWatcher) {
			_watcher.addPath(fileName);
		}
	}
	else {
		if (updateWatcher) {
			_watcher.addPath(fileName);
		}

		QMessageBox::critical(this, "", QString(tr("Unable to save %1")).arg(fileName));
	}
}

void JokerWindow::onExternalChange(const QString &path)
{
    if (path != "")
            return;
    /*PhTime currentTime = _videoEngine.clock()->time();
	PhDocumentWindow::onExternalChange(path);
    _videoEngine.clock()->setTime(currentTime);*/
}

bool JokerWindow::eventFilter(QObject * sender, QEvent *event)
{
	/// The event filter catch the following event:
	switch (event->type()) {
	case QEvent::MouseMove: /// - Mouse move show the media panel
	case QEvent::HoverEnter:
	case QEvent::HoverMove:
		{
			QMouseEvent * mouseEvent = (QMouseEvent*)event;
			// Check if it is near the video/strip border
			float stripHeight = this->height() * _settings->stripHeight();
			if((mouseEvent->pos().y() > (this->height() - stripHeight) - 10)
			   && (mouseEvent->pos().y() < (this->height() - stripHeight) + 10))
				QApplication::setOverrideCursor(Qt::SizeVerCursor);
			else
				QApplication::setOverrideCursor(Qt::ArrowCursor);

			if(_resizingStrip && (mouseEvent->buttons() & Qt::LeftButton)) {
				float newStripHeight = 1.0 - ((float) mouseEvent->pos().y() /(float) this->height());
				if(newStripHeight <= 0) {
					_settings->resetStripHeight();
					ui->actionHide_the_rythmo->setChecked(true);
					_settings->setHideStrip(true);
				}
				else {
					PHDBG(2) << "resizing strip:" << newStripHeight;
					_settings->setStripHeight(newStripHeight);
				}
			}
			break;
		}
	case QEvent::MouseButtonDblClick: /// - Double mouse click toggle fullscreen mode
		if(sender == this)
			toggleFullScreen();
		break;
	case QEvent::MouseButtonRelease:
		PHDEBUG << "end resizing strip";
		_resizingStrip = false;
		break;
	case QEvent::MouseButtonPress:
		{
			QMouseEvent *mouseEvent = (QMouseEvent*)event;
			if((sender == this) && (mouseEvent->buttons() & Qt::RightButton)) {
				/// - Right mouse click on the video open the video file dialog.
				if(mouseEvent->y() < this->height() * (1.0f - _settings->stripHeight()))
					on_actionOpen_Video_triggered();
				else /// - Left mouse click on the strip open the strip file dialog.
					on_actionOpen_triggered();
				return true;
			}
			float stripHeight = this->height() * _settings->stripHeight();
			if((mouseEvent->pos().y() > (this->height() - stripHeight) - 10)
			   && (mouseEvent->pos().y() < (this->height() - stripHeight) + 10)) {
				PHDEBUG << "start resizing strip";
				_resizingStrip = true;
			}
			break;
		}
	case QEvent::KeyPress:
		{
			QKeyEvent *keyEvent = (QKeyEvent*)event;
			if(keyEvent->key() == Qt::Key_Space) {
                onChangePlayButton();
			}
			break;
		}
	default:
		break;
	}

	return PhDocumentWindow::eventFilter(sender, event);
}

QMenu *JokerWindow::recentDocumentMenu()
{
	return ui->menuOpen_recent;
}

QAction *JokerWindow::fullScreenAction()
{
	return ui->actionFullscreen;
}

void JokerWindow::onApplicationActivate()
{
	PhDocumentWindow::onApplicationActivate();
	showMediaPanel();
}

void JokerWindow::onApplicationDeactivate()
{
	PhDocumentWindow::onApplicationDeactivate();
	hideMediaPanel();
}

void JokerWindow::showMediaPanel()
{
	// Don't show the mediaPanel if Joker has not thefocus.
	if(_settings->displayControlPanel()) {
		_mediaPanel.show();
	}
}

void JokerWindow::hideMediaPanel()
{
	_mediaPanel.hide();
}

//print the window
void JokerWindow::onPaint(int width, int height)
{

#ifdef USE_VLC
#ifdef USE_VIDEO
    PhClock *clock = _videoEngine.clock();
#else
    int timeTmp = 0;
    PhClock *clock = _strip.clock();
    if (vlcMedia) {
        timeTmp = libvlc_media_get_duration(vlcMedia);
        // ltc and mtc print who don't work
        if(_synchronizer.time() && 0 == 1) {
            qDebug() << _synchronizer.time();
            ltcTime = _synchronizer.time();
            videoTime2 = _synchronizer.time();
            ms = duration_cast< milliseconds >(
                system_clock::now().time_since_epoch()
            );
        } else {
        qDebug() << _synchronizer.time();
        //update the timer
            videoTime2 = libvlc_media_player_get_time(vlcPlayer);
                if (videoTime2 == videoTime2tmp) {

                    if (videoIsPlaying == true) {
                         ms2 = duration_cast< milliseconds >(
                            system_clock::now().time_since_epoch()
                        );
                        ms3 = ms2.count() - ms.count();
                    }
                    videoTime2 += ms3;
                } else {
                    ms = duration_cast< milliseconds >(
                        system_clock::now().time_since_epoch()
                    );
                    videoTime2tmp = videoTime2;
                }
                _strip.clock()->setTime(videoTime2 + 3600000);
                _synchronizer.setStripClock(_strip.clock());
                _mediaPanel.setClock(_strip.clock());

        }
    }
    //update video time
    if (vlcMedia) {
        if (timeTmp > 0 &&  timeTmp != videoTime) {
            videoTime = timeTmp;
            ui->videoSlider->setMaximum(timeTmp/1000);
            ui->videoSlider->setValue(0);
        }
        if (videoTime2 > videoTime) {
            videoTime2 = 0;
            libvlc_media_player_set_time(vlcPlayer, 0);
            libvlc_media_player_pause(vlcPlayer);
            videoIsPlaying = false;
        }
    }

    if (sliderPress == false)
        ui->videoSlider->setValue(videoTime2 / 1000);
#endif
    //_strip.clock()->setTime(_strip.clock()->time() / 36000 + 3600000);

    PhTime delay = (PhTime)(24 * _settings->screenDelay() * _strip.clock()->rate());
    PhTime clockTime = _strip.clock()->time() + delay;

    float stripHeightRatio = 0.0f;
    if(!_settings->hideStrip())
        stripHeightRatio = _settings->stripHeight();

    int stripHeight = height * stripHeightRatio;
    int videoHeight = height - stripHeight;

    //update video and widgets size
    if (this->width() != windowWidth || videoHeight != windowHeight) {
        windowWidth = this->width();
        windowHeight = this->height();
        ui->vlcWidget->setGeometry(QRect(0, 0, windowWidth * 0.80, videoHeight - 30));
        ui->videoSlider->setGeometry(QRect(0, videoHeight - 25, windowWidth * 0.8 * 0.92, windowHeight * 0.02));
        ui->playButton->setGeometry(QRect(windowWidth * 0.8 * 0.94, videoHeight - 25, windowWidth * 0.8 * 0.05, windowHeight * 0.02) );
    }
#ifdef USE_VIDEO
    if(!_doc->forceRatio169() && (_videoEngine.height() > 0))
        videoWidth = videoHeight * _videoEngine.width() / _videoEngine.height();
#endif
    int videoAvailableWidth = width;
    // Center video if no information panel with next text
    if(_settings->displayNextText())
        videoAvailableWidth = width * 0.8f;
    QColor infoColor = _settings->backgroundColorLight();

    int y = 0; 

    // Get the selected people list
    QList<PhPeople*> selectedPeoples;
    QList<QString> peoples;
    foreach(QString name, _settings->selectedPeopleNameList()) {
        PhPeople *people = _strip.doc()->peopleByName(name);
        if(people) {
            peoples.append(name);
            selectedPeoples.append(people);
        }
    }

    int x = videoAvailableWidth;
    if(_settings->displayNextText()) {
        int infoWidth = width - videoAvailableWidth;
        int spacing = 4;

        // Display the title
        {
            QString title = _strip.doc()->title().toLower();
            if(_strip.doc()->episode().length() > 0)
                title += " #" + _strip.doc()->episode().toLower();

            int titleHeight = infoWidth / 12;
            int titleWidth = _strip.hudFont()->getNominalWidth(title) * titleHeight / 110;
            PhGraphicText titleText(_strip.hudFont());
            titleText.setColor(infoColor);
            titleText.setRect(x + spacing, y, titleWidth, titleHeight);
            y += titleHeight;
            titleText.setContent(title);
            titleText.setZ(5);
            _titleText = titleText;
            titleText.draw();
        }

        // Display the current timecode
        {
            int tcWidth = infoWidth - 2 * spacing;
            int tcHeight = infoWidth / 6;
            PhGraphicText tcText(_strip.hudFont());
            tcText.setColor(infoColor);
            tcText.setRect(x + 4, y, tcWidth, tcHeight);

            tcText.setContent(PhTimeCode::stringFromTime(clockTime, localTimeCodeType()));
            _tcText = tcText;
            tcText.draw();

            y += tcHeight;
        }

        // Display the box around current loop number
        int boxWidth = infoWidth / 4;
        int nextTcWidth = infoWidth - boxWidth - 3 * spacing;
        int nextTcHeight = nextTcWidth / 6;
        int boxHeight = nextTcHeight + spacing;
        {
            PhGraphicSolidRect outsideLoopRect(x + spacing, y, boxWidth, boxHeight);
            outsideLoopRect.setColor(infoColor);
            outsideLoopRect.draw();
            _outsideLoopRect = outsideLoopRect;

            // Display the current loop number
            QString loopLabel = "0";
            PhStripLoop * currentLoop = _strip.doc()->previousLoop(clockTime);
            if(currentLoop)
                loopLabel = currentLoop->label();
            int loopWidth = _strip.hudFont()->getNominalWidth(loopLabel) * nextTcHeight / 110;
            int loopHeight = nextTcHeight;
            int loopX = x + spacing + (boxWidth - loopWidth) / 2;
            int loopY = y + spacing;

            PhGraphicText gCurrentLoop(_strip.hudFont(), loopLabel);

            gCurrentLoop.setRect(loopX, loopY, loopWidth, loopHeight);
            gCurrentLoop.setColor(Qt::black);
            gCurrentLoop.draw();
            _gCurrentLoop = gCurrentLoop;
        }

        // Display the next timecode
        {
            /// The next time code will be the next element of the people from the list.
            PhStripText *nextText = NULL;
            if(selectedPeoples.count()) {
                nextText = _strip.doc()->nextText(selectedPeoples, clockTime);
                if(nextText == NULL)
                    nextText = _strip.doc()->nextText(selectedPeoples, 0);
            }
            else {
                nextText = _strip.doc()->nextText(clockTime);
                if(nextText == NULL)
                    nextText = _strip.doc()->nextText(0);
            }

            PhTime nextTextTime = 0;
            if(nextText != NULL)
                nextTextTime = nextText->timeIn();

            PhGraphicText nextTCText(_strip.hudFont());
            nextTCText.setColor(infoColor);

            int nextTcX = x + 2 * spacing + boxWidth;
            int nextTcY = y + spacing;
            nextTCText.setRect(nextTcX, nextTcY, nextTcWidth, nextTcHeight);

            nextTCText.setContent(PhTimeCode::stringFromTime(nextTextTime, localTimeCodeType()));
            nextTCText.draw();
            _nextTCText = nextTCText;

            y += boxHeight + spacing;
        }

        // Display the control button
//		{
//			_playButton.setColor(infoColor);
//			_playButton.setRect(x, y, 16, 16);
//			_playButton.draw();
//			_forwardButton.setColor(infoColor);
//			_forwardButton.setRect(x + 16, y, 16, 16);
//			_forwardButton.draw();
//		}
    }

    if(stripHeight > 0) {
        if(_synchronizer.stripClock() == NULL) {
            PHDEBUG <<  "Reconnect the strip clock";
            _synchronizer.setStripClock(_strip.clock());
#ifdef USE_VIDEO
            _mediaPanel.setClock(_strip.clock());
#endif
        }

        _strip.draw(0, videoHeight, width, stripHeight, x, y, selectedPeoples);
        _selectedPeoples = selectedPeoples;

    }
    else if(_synchronizer.stripClock()) {
        PHDEBUG << "Disconnect the strip clock";
        _synchronizer.setStripClock(NULL);
#ifdef USE_VIDEO
        _mediaPanel.setClock(_videoEngine.clock());
#endif
    }

    foreach(QString info, _strip.infos()) {
        ui->videoStripView->addInfo(info);
    }

    if((_settings->synchroProtocol() == PhSynchronizer::Sony) && (_lastVideoSyncElapsed.elapsed() > 1000)) {
        PhGraphicText errorText(_strip.hudFont(), tr("No video sync"));
        errorText.setRect(width / 2 - 100, height / 2 - 25, 200, 50);
        int red = (_lastVideoSyncElapsed.elapsed() - 1000) / 4;
        if (red > 255)
            red = 255;
        errorText.setColor(QColor(red, 0, 0));
        errorText.draw();
        _errorText = errorText;
    }

#endif
}

void initUi() {

}

void JokerWindow::onChangePlayButton() {
    if (vlcPlayer && libvlc_media_player_is_playing(vlcPlayer)) {
         libvlc_media_player_pause(vlcPlayer);
         ui->playButton->setText("Play");
         videoIsPlaying = false;
    }
    else if (vlcPlayer && !libvlc_media_player_is_playing(vlcPlayer)) {
        libvlc_media_player_play(vlcPlayer);
        ui->playButton->setText("Pause");
        videoIsPlaying = true;
   }
}

void JokerWindow::onChangeVideoslider() {
    long long timeSlider = ui->videoSlider->value();
    libvlc_media_player_set_time(vlcPlayer, timeSlider * 1000);
}

void JokerWindow::onPressVideoSlider() {
    sliderPress = true;
}

void JokerWindow::onReleaseVideoSlider() {
    sliderPress = false;
}

void JokerWindow::onVideoSync()
{
	_lastVideoSyncElapsed.restart();
}

void JokerWindow::setCurrentTime(PhTime time)
{
    _strip.clock()->setTime(time);
    videoTime2 = time;
    qDebug() << time;
#ifdef USE_MIDI
	if(_settings->sendMmcMessage())
		_mtcWriter.sendMMCGotoFromTime(time);
#endif

}

void JokerWindow::setCurrentRate(PhRate rate)
{
	_strip.clock()->setRate(rate);
#ifdef USE_MIDI
	if(_settings->sendMmcMessage()) {
		_mtcWriter.sendMMCGotoFromTime(currentTime());
		if(rate == 0.0f)
			_mtcWriter.sendMMCStop();
		else if(rate == 1.0f)
			_mtcWriter.sendMMCPlay();
	}
#endif // USE_MIDI
}

void JokerWindow::onTimecodeTypeChanged(PhTimeCodeType)
{
	PHDEBUG << this->synchroTimeCodeType();
	_mediaPanel.setTimeCodeType(this->synchroTimeCodeType());
}

PhTimeCodeType JokerWindow::localTimeCodeType()
{
#ifdef USE_VIDEO
	return _videoEngine.timeCodeType();
#else
	return _doc->videoTimeCodeType();
#endif

}

PhTimeCodeType JokerWindow::synchroTimeCodeType()
{
	switch (_settings->synchroProtocol()) {
#ifdef USE_SONY
	case PhSynchronizer::Sony:
		return _sonySlave.timeCodeType();
#endif
#ifdef USE_LTC
	case PhSynchronizer::LTC:
		return _ltcReader.timeCodeType();
#endif
#ifdef USE_MIDI
	case PhSynchronizer::MTC:
		return _mtcReader.timeCodeType();
#endif
	default:
		return localTimeCodeType();
	}
}

void JokerWindow::on_actionOpen_triggered()
{
	hideMediaPanel();

	if(checkDocumentModification()) {
		QString filter = tr("Rythmo files") + " (";
		foreach(QString type, _settings->stripFileType())
			filter += "*." + type + " ";
		filter += ")";
		QFileDialog dlg(this, tr("Open..."), _settings->lastDocumentFolder(), filter);

		dlg.setFileMode(QFileDialog::ExistingFile);
		if(dlg.exec()) {
			QString fileName = dlg.selectedFiles()[0];
			if(!openDocument(fileName))
				QMessageBox::critical(this, "", QString(tr("Unable to open %1")).arg(fileName));
		}
	}
	showMediaPanel();
}

void JokerWindow::on_actionPlay_pause_triggered()
{
	if(currentRate() == 0.0) {
		setCurrentRate(1.0);
	}
	else {
		setCurrentRate(0.0);
	}
}

void JokerWindow::on_actionPlay_backward_triggered()
{
	setCurrentRate(-1.0);
}

void JokerWindow::on_actionStep_forward_triggered()
{
    int videoTimeTmp = videoTime2tmp + plus + (1000 / 25);
    int timeTmp = libvlc_media_get_duration(vlcMedia);
    if (videoTimeTmp > timeTmp)
        videoTimeTmp = timeTmp;
    libvlc_media_player_set_time(vlcPlayer, videoTimeTmp);
}

void JokerWindow::on_actionStep_backward_triggered()
{
     int videoTimeTmp = videoTime2tmp + plus - (1000 / 25);
     if (videoTimeTmp < 0)
         videoTimeTmp = 0;
     libvlc_media_player_set_time(vlcPlayer, videoTimeTmp);
}

void JokerWindow::on_actionStep_time_forward_triggered()
{
	setCurrentRate(0.0);
	setCurrentTime(currentTime() + 1);
}

void JokerWindow::on_actionStep_time_backward_triggered()
{
	setCurrentRate(0.0);
	setCurrentTime(currentTime() - 1);
}

void JokerWindow::on_action_3_triggered()
{
	setCurrentRate(-3.0);
}

void JokerWindow::on_action_1_triggered()
{
	setCurrentRate(-1.0);
}

void JokerWindow::on_action_0_5_triggered()
{
	setCurrentRate(-0.5);
}

void JokerWindow::on_action0_triggered()
{
	setCurrentRate(0.0);
}

void JokerWindow::on_action0_5_triggered()
{
	setCurrentRate(0.5);
}

void JokerWindow::on_action1_triggered()
{
	setCurrentRate(1.0);
}

void JokerWindow::on_action3_triggered()
{
	setCurrentRate(3.0);
}

void JokerWindow::on_actionOpen_Video_triggered()
{
#ifdef USE_VIDEO
	hideMediaPanel();

	QString lastFolder = _settings->lastVideoFolder();
	QString filter = tr("Movie files") + " (";
	foreach(QString type, _settings->videoFileType())
		filter += "*." + type + " ";
	filter += ");";

	QFileDialog dlg(this, tr("Open a video..."), lastFolder, filter);
	if(dlg.exec()) {
		QString videoFile = dlg.selectedFiles()[0];

		_setCurrentTimeToVideoTimeIn = true;
		openVideoFile(videoFile);
	}

	showMediaPanel();
#endif
}

#ifdef USE_VIDEO
void JokerWindow::videoFileOpened(bool success)
{
	PHDEBUG << "video file opened";

	if (success) {
		PhTime videoTimeIn = _videoEngine.timeIn();
		QFileInfo lastFileInfo(_doc->videoFilePath());
		QFileInfo fileInfo(_videoEngine.fileName());

		if(videoTimeIn == 0) {
			/* the video itself has no timestamp, and until now we have not
			 * propagated the doc videoTimeIn to the videoEngine */
			videoTimeIn = _doc->videoTimeIn();
			_videoEngine.setTimeIn(videoTimeIn);
			_videoEngine.clock()->setTime(videoTimeIn);

			/* ask the user if he wants to change the video time in */
			if(fileInfo.fileName() != lastFileInfo.fileName()) {
				on_actionChange_timestamp_triggered();
				videoTimeIn = _videoEngine.timeIn();
			}
		}

		if(_videoEngine.fileName() != _doc->videoFilePath()) {
			_doc->setVideoFilePath(_videoEngine.fileName());
			_doc->setVideoTimeIn(videoTimeIn, _videoEngine.timeCodeType());
			_doc->setModified(true);
		}

		_videoEngine.clock()->setTime(videoTimeIn);
		_mediaPanel.setTimeIn(videoTimeIn);
		_mediaPanel.setLength(_videoEngine.length());

		_settings->setLastVideoFolder(fileInfo.absolutePath());

		this->addFilePermission(_doc->videoFilePath());

		if (_setCurrentTimeToVideoTimeIn) {
			setCurrentTime(_doc->videoTimeIn());
			_setCurrentTimeToVideoTimeIn = false;
		}

		if (_syncTimeInToDoc) {
			_videoEngine.setTimeIn(_doc->videoTimeIn());
			_mediaPanel.setTimeIn(_doc->videoTimeIn());
		}
	}

	if (_syncTimeInToDoc) {
		/// - Set the video aspect ratio.
		ui->actionForce_16_9_ratio->setChecked(_doc->forceRatio169());

		/// - Goto to the document last position.
		setCurrentTime(_doc->lastTime());

		_syncTimeInToDoc = false;
	}
}

bool JokerWindow::openVideoFile(QString videoFile)
{
	QFileInfo fileInfo(videoFile);
	if (fileInfo.exists() && _videoEngine.open(videoFile)) {
		return true;
	}
	return false;
}
#endif

void JokerWindow::timeCounter(PhTime elapsedTime)
{
	if(currentRate() == 1 && (PhSynchronizer::SyncType)_settings->synchroProtocol() != PhSynchronizer::NoSync) {
		_timePlayed += elapsedTime;
	}
}

void JokerWindow::on_actionChange_timestamp_triggered()
{
	hideMediaPanel();
	setCurrentRate(0);
    //PhTime time = _synchronizer.videoClock()->time();

#ifdef USE_VIDEO
	if(_synchronizer.videoClock()->time() < _videoEngine.timeIn())
		time = _videoEngine.timeIn();
	else if(_synchronizer.videoClock()->time() > _videoEngine.timeIn() + _videoEngine.length())
		time = _videoEngine.timeOut();

	PhTimeCodeDialog dlg(localTimeCodeType(), time);
	if(dlg.exec() == QDialog::Accepted) {
		PhTime timeStamp = 0;
		if(_synchronizer.videoClock()->time() > _videoEngine.timeIn() + _videoEngine.length())
			timeStamp = dlg.time() - (_videoEngine.length() - PhTimeCode::timePerFrame(localTimeCodeType()));
		else if (_synchronizer.videoClock()->time() < _videoEngine.timeIn())
			timeStamp =  dlg.time();
		else
			timeStamp = _videoEngine.timeIn() + dlg.time() - _synchronizer.videoClock()->time();

		_videoEngine.setTimeIn(timeStamp);
		setCurrentTime(dlg.time());
		_doc->setVideoTimeIn(timeStamp, localTimeCodeType());
		_mediaPanel.setTimeIn(timeStamp);
		_doc->setModified(true);
	}
#endif

	showMediaPanel();
}



void JokerWindow::on_actionAbout_triggered()
{
	hideMediaPanel();

	AboutDialog dlg;
	dlg.setTimePlayed(_timePlayed);
	dlg.exec();

	showMediaPanel();
}


void JokerWindow::on_actionPreferences_triggered()
{
	hideMediaPanel();
	int oldSynchroProtocol = _settings->synchroProtocol();
#ifdef USE_LTC
	QString oldLtcInputPort = _settings->ltcInputPort();
	PhTimeCodeType oldLtcTimeCodeType = (PhTimeCodeType)_settings->ltcReaderTimeCodeType();
#endif // USE_LTC
#ifdef USE_MIDI
	QString oldMtcInputPort = _settings->mtcInputPort();
	QString oldMtcVirtualInputPort = _settings->mtcVirtualInputPort();
	bool oldMtcInputUseExistingPort = _settings->mtcInputUseExistingPort();
	bool oldSendMmcMessage = _settings->sendMmcMessage();
	QString oldMmcOutputPort = _settings->mmcOutputPort();
#endif // USE_MIDI

	PreferencesDialog dlg(_settings);
	if(dlg.exec() == QDialog::Accepted) {
		if((oldSynchroProtocol != _settings->synchroProtocol())
#ifdef USE_LTC
		   || (oldLtcInputPort != _settings->ltcInputPort())
		   || (oldLtcTimeCodeType != (PhTimeCodeType)_settings->ltcReaderTimeCodeType())
#endif // USE_LTC
#ifdef USE_MIDI
		   || (oldMtcInputPort != _settings->mtcInputPort())
		   || (oldMtcVirtualInputPort != _settings->mtcVirtualInputPort())
		   || (oldMtcInputUseExistingPort != _settings->mtcInputUseExistingPort())
		   || (oldSendMmcMessage != _settings->sendMmcMessage())
		   || (oldMmcOutputPort != _settings->mmcOutputPort())
#endif // USE_MIDI
		   ) {
			PHDEBUG << "Set protocol:" << _settings->synchroProtocol();
			setupSyncProtocol();
		}
#ifdef USE_MIDI
		_mtcReader.force24as2398(_settings->mtcForce24as2398());
#endif // USE_MIDI
	}

	showMediaPanel();
}

void JokerWindow::on_actionProperties_triggered()
{
	_propertyDialog.show();
}

void JokerWindow::on_actionTest_mode_triggered()
{
	_settings->setStripTestMode(!_settings->stripTestMode());
}

void JokerWindow::on_actionTimecode_triggered()
{
	hideMediaPanel();

	PhTimeCodeDialog dlg(localTimeCodeType(), currentTime());
	if(dlg.exec() == QDialog::Accepted)
		setCurrentTime(dlg.time());

	showMediaPanel();
}

void JokerWindow::on_actionNext_element_triggered()
{
	PhTime time = _doc->nextElementTime(currentTime());
	if(time < PHTIMEMAX)
		setCurrentTime(time);
}

void JokerWindow::on_actionPrevious_element_triggered()
{
	PhTime time = _doc->previousElementTime(currentTime());
	if(time > PHTIMEMIN)
		setCurrentTime(time);
}

void JokerWindow::on_actionClear_list_triggered()
{
	//Open the recent group
	//	_settings->beginGroup("openRecent");
	//	//List all keys
	//	QStringList indexes = _settings->allKeys();
	//	//Remove them from
	//	foreach(QString index, indexes)
	//	_settings->remove(index);

	//	//Close the group
	//	_settings->endGroup();

	//Remove the buttons of the UI, keep the separator and the Clear button
	foreach(QAction * action, ui->menuOpen_recent->actions()) {
		// Break if the separator is reached
        if(action->isSeparator())
            break;
        // Remove it
        ui->menuOpen_recent->removeAction(action);
		delete action;
	}

	// Remove all the buttons
	//	_recentFileButtons.clear();
	ui->menuOpen_recent->setEnabled(false);
}

void JokerWindow::on_actionSave_triggered()
{
	QString fileName = _settings->currentDocument();
	QFileInfo info(fileName);
	if(!info.exists() || (info.suffix() != "detx"))
		on_actionSave_as_triggered();
	else
		saveDocument(fileName);
}

void JokerWindow::on_actionSave_as_triggered()
{
	hideMediaPanel();

	QString fileName = _settings->currentDocument();
	QString lastFolder = _settings->lastDocumentFolder();
	// If there is no current strip file, ask for a name
	if(fileName == "")
		fileName = lastFolder;
	else {
		QFileInfo info(fileName);
		if(info.suffix() != "detx")
			fileName = lastFolder + "/" + info.completeBaseName() + ".detx";
	}

	fileName = QFileDialog::getSaveFileName(this, tr("Save..."), fileName, "*.detx");
	if(fileName != "")
		saveDocument(fileName);
}

bool JokerWindow::isDocumentModified()
{
	return _doc->modified();
}

void JokerWindow::on_actionSelect_character_triggered()
{
	hideMediaPanel();

	PeopleDialog dlg(this, _doc, _settings);

	dlg.restoreGeometry(_settings->peopleDialogGeometry());
	dlg.exec();
	_settings->setPeopleDialogGeometry(dlg.saveGeometry());
}

void JokerWindow::on_actionForce_16_9_ratio_triggered(bool checked)
{
	_doc->setForceRatio169(checked);
	_doc->setModified(true);
}

void JokerWindow::on_actionInvert_colors_toggled(bool checked)
{
	_settings->setInvertColor(checked);
}

void JokerWindow::on_actionDisplay_feet_triggered(bool checked)
{
	_settings->setDisplayFeet(checked);
}

void JokerWindow::on_actionSet_first_foot_timecode_triggered()
{
	PhTimeCodeDialog dlg(localTimeCodeType(), _settings->firstFootTime(), this);
	if(dlg.exec())
		_settings->setFirstFootTime(dlg.time());
}

void JokerWindow::on_actionNew_triggered()
{
	_doc->reset();
    //on_actionClose_video_triggered();
    if (vlcPlayer && libvlc_media_player_is_playing(vlcPlayer)) {
        libvlc_media_player_pause(vlcPlayer);
    }
	this->resetDocument();
}

void JokerWindow::on_actionClose_video_triggered()
{
#ifdef USE_VIDEO
	_videoEngine.close();

#endif
}

void JokerWindow::on_actionSend_feedback_triggered()
{
	hideMediaPanel();
	PhFeedbackDialog dlg(this);
	dlg.exec();
	showMediaPanel();
}

void JokerWindow::on_actionDeinterlace_video_triggered(bool checked)
{
#ifdef USE_VIDEO
	_videoEngine.setDeinterlace(checked);
	if(checked != _doc->videoDeinterlace()) {
		_doc->setVideoDeinterlace(checked);
		_doc->setModified(true);
	}
#endif
    if (checked)
        return;
}

void JokerWindow::on_actionHide_the_rythmo_triggered(bool checked)
{
	_settings->setHideStrip(checked);
}

void JokerWindow::on_actionPrevious_loop_triggered()
{
	PhTime time = _doc->previousLoopTime(currentTime());
	if(time > PHTIMEMIN)
		setCurrentTime(time);
}

void JokerWindow::on_actionNext_loop_triggered()
{
	PhTime time = _doc->nextLoopTime(currentTime());
	if(time < PHTIMEMAX)
		setCurrentTime(time);
}

void JokerWindow::on_actionDisplay_the_cuts_toggled(bool checked)
{
	_settings->setDisplayCuts(checked);
}

void JokerWindow::on_actionSet_distance_between_two_feet_triggered()
{
	TimeBetweenTwoFeetDialog dlg(_settings);
	dlg.exec();
}

void JokerWindow::on_actionDisplay_the_vertical_scale_triggered(bool checked)
{
	_settings->setDisplayVerticalScale(checked);
}

PhTime JokerWindow::currentTime()
{
	return _strip.clock()->time();
}

PhRate JokerWindow::currentRate()
{
	return _strip.clock()->rate();
}

void JokerWindow::on_actionDisplay_the_control_panel_triggered(bool checked)
{
	_settings->setDisplayControlPanel(checked);
	if(checked)
		showMediaPanel();
	else
		hideMediaPanel();
}

void JokerWindow::on_actionDisplay_the_information_panel_triggered(bool checked)
{
	_settings->setDisplayNextText(checked);
}

void JokerWindow::on_actionHide_selected_peoples_triggered(bool checked)
{
	_settings->setHideSelectedPeoples(checked);
}

void JokerWindow::on_actionUse_native_video_size_triggered(bool checked)
{
#ifdef USE_VIDEO
	_settings->setUseNativeVideoSize(checked);
#endif
    if (checked)
        return;
}

void JokerWindow::on_actionPicture_in_picture_triggered(bool checked)
{
#ifdef USE_VIDEO
	_settings->setVideoPictureInPicture(checked);
#endif
    if (checked)
        return;
}

void JokerWindow::on_actionSet_TC_in_triggered()
{
	_settings->setSyncLoopTimeIn(_synchronizer.time());
}

void JokerWindow::on_actionSet_TC_out_triggered()
{
	_settings->setSyncLoopTimeOut(_synchronizer.time());
}

void JokerWindow::on_actionLoop_triggered(bool checked)
{
	_settings->setSyncLooping(checked);
}

void JokerWindow::on_actionSecond_screen_triggered(bool checked)
{
#ifdef USE_VIDEO
	_settings->setVideoSecondScreen(checked);
	if(checked) {
		_secondScreenWindow = new SecondScreenWindow(&_videoEngine, ui->videoStripView, _settings);
		_secondScreenWindow->show();
		connect(_secondScreenWindow, &SecondScreenWindow::closing, this, &JokerWindow::onSecondScreenClosed);
	}
	else {
		_secondScreenWindow->close();
		delete _secondScreenWindow;
		_secondScreenWindow = NULL;
	}
#endif
    if (checked)
        return;
}

void JokerWindow::onSecondScreenClosed(bool closedFromUser)
{
    /*ui->actionSecond_screen->setChecked(false);
	if(_secondScreenWindow) {
		delete _secondScreenWindow;
		_secondScreenWindow = NULL;
	}
	if(closedFromUser)
        _settings->setVideoSecondScreen(false);*/
    if (closedFromUser)
        return;
}

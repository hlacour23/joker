/******************************
 * Qt player using libVLC     *
 * By protonux                *
 *                            *
 * Under WTFPL                *
 ******************************/

#ifndef PLAYER
#define PLAYER

#include <QtGui>
#include <QMainWindow>
#include <QPushButton>
#include <QSlider>
#include <QWidget>
#include <QCloseEvent>
#include <QMessageBox>
#include <QFileDialog>
#include <QFontDatabase>
#include <QTimer>
#include <QVBoxLayout>
#include <QPropertyAnimation>
#include <thread>
#include <iostream>
#include <string>
#include <vlc/vlc.h>

struct vout_window_t;

class Mwindow : public QMainWindow {

    Q_OBJECT

        public:
               Mwindow();
               virtual ~Mwindow();
               bool request( struct vout_window_t * );

        private slots:
               void openFile();
               void play();
               void stop();
               void mute();
               void about();
               void fullscreen();

               int changeVolume(int);
               void changePosition(int);
               void updateInterface();

        protected:
               virtual void closeEvent(QCloseEvent*);

        public:
               QPushButton *playBut;
               QSlider *volumeSlider;
               QSlider *slider;
               QWidget *videoWidget;
               QVBoxLayout *layout;
               QVBoxLayout *layout2;
               QString fileOpen;

               libvlc_media_t *vlcMedia;
               libvlc_instance_t *vlcInstance;
               libvlc_media_player_t *vlcPlayer;
               vout_window_t *p_window;

               void initUI();
};


#endif

/********************************************************************************
** Form generated from reading UI file 'PreferencesDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PREFERENCESDIALOG_H
#define UI_PREFERENCESDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFontComboBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "PhCommonUI/PhDialogButtonBox.h"
#include "PhCommonUI/PhLockableSpinBox.h"

QT_BEGIN_NAMESPACE

class Ui_PreferencesDialog
{
public:
    QVBoxLayout *verticalLayout_0;
    QTabWidget *tabWidget;
    QWidget *tabGeneral;
    QVBoxLayout *verticalLayout;
    QGroupBox *interfaceGroupBox;
    QFormLayout *formLayout_8;
    QLabel *label_5;
    QComboBox *cboBoxLang;
    QGroupBox *delayGroupBox;
    QFormLayout *formLayout_7;
    QLabel *mainDelayLabel;
    PhLockableSpinBox *mainScreenDelayspinBox;
    QLabel *label_2;
    PhLockableSpinBox *secondScreenDelaySpinBox;
    QGroupBox *pipGroupBox;
    QFormLayout *formLayout_6;
    QLabel *pipRatioLabel;
    QLabel *pipOffsetLabel;
    QSpinBox *pipOffsetSpinBox;
    QSlider *pipRatioSlider;
    QRadioButton *pipLeftPositionRadioButton;
    QLabel *pipPositionLabel;
    QRadioButton *pipRightPositionRadioButton;
    QWidget *tabStrip;
    QFormLayout *formLayout;
    QLabel *lblSpeed;
    QSpinBox *spinBoxSpeed;
    QLabel *fontLabel;
    QFontComboBox *fontComboBox;
    QLabel *lblStripHeight;
    QSlider *sliderStripHeight;
    QWidget *tabSynchro;
    QVBoxLayout *verticalLayout_2;
    QRadioButton *noSyncRadioButton;
    QRadioButton *sonyRadioButton;
    QFrame *sonyFrame;
    QFormLayout *formLayout_4;
    QLabel *sonyCommunicationTimeCodeTypeLabel;
    QComboBox *sonyCommunicationTimeCodeTypeComboBox;
    QLabel *sonyVideoSyncTimeCodeTypeLabel;
    QComboBox *sonyVideoSyncTimeCodeTypeComboBox;
    QRadioButton *ltcRadioButton;
    QFrame *ltcFrame;
    QFormLayout *formLayout_2;
    QLabel *ltcInputPortLabel;
    QComboBox *ltcInputPortComboBox;
    QComboBox *ltcTimeCodeTypeComboBox;
    QLabel *ltcTimeCodeTypeLabel;
    QRadioButton *mtcRadioButton;
    QFrame *mtcFrame;
    QFormLayout *formLayout_3;
    QComboBox *mtcExistingInputPortComboBox;
    QRadioButton *mtcExistingInputPortRadioButton;
    QRadioButton *mtcVirtualInputPortRadioButton;
    QLineEdit *mtcVirtualInputPortLineEdit;
    QCheckBox *mtcForce24as2398CheckBox;
    QCheckBox *mmcCheckBox;
    QFrame *mmcFrame;
    QFormLayout *formLayout_5;
    QComboBox *mmcOutputPortComboBox;
    QLabel *mmcOutputPortLabel;
    PhDialogButtonBox *buttonBox;

    void setupUi(QDialog *PreferencesDialog)
    {
        if (PreferencesDialog->objectName().isEmpty())
            PreferencesDialog->setObjectName(QStringLiteral("PreferencesDialog"));
        PreferencesDialog->resize(503, 569);
        PreferencesDialog->setModal(true);
        verticalLayout_0 = new QVBoxLayout(PreferencesDialog);
        verticalLayout_0->setSpacing(6);
        verticalLayout_0->setContentsMargins(11, 11, 11, 11);
        verticalLayout_0->setObjectName(QStringLiteral("verticalLayout_0"));
        tabWidget = new QTabWidget(PreferencesDialog);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabGeneral = new QWidget();
        tabGeneral->setObjectName(QStringLiteral("tabGeneral"));
        verticalLayout = new QVBoxLayout(tabGeneral);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        interfaceGroupBox = new QGroupBox(tabGeneral);
        interfaceGroupBox->setObjectName(QStringLiteral("interfaceGroupBox"));
        formLayout_8 = new QFormLayout(interfaceGroupBox);
        formLayout_8->setSpacing(6);
        formLayout_8->setContentsMargins(11, 11, 11, 11);
        formLayout_8->setObjectName(QStringLiteral("formLayout_8"));
        label_5 = new QLabel(interfaceGroupBox);
        label_5->setObjectName(QStringLiteral("label_5"));

        formLayout_8->setWidget(0, QFormLayout::LabelRole, label_5);

        cboBoxLang = new QComboBox(interfaceGroupBox);
        cboBoxLang->setObjectName(QStringLiteral("cboBoxLang"));

        formLayout_8->setWidget(0, QFormLayout::FieldRole, cboBoxLang);


        verticalLayout->addWidget(interfaceGroupBox);

        delayGroupBox = new QGroupBox(tabGeneral);
        delayGroupBox->setObjectName(QStringLiteral("delayGroupBox"));
        formLayout_7 = new QFormLayout(delayGroupBox);
        formLayout_7->setSpacing(6);
        formLayout_7->setContentsMargins(11, 11, 11, 11);
        formLayout_7->setObjectName(QStringLiteral("formLayout_7"));
        mainDelayLabel = new QLabel(delayGroupBox);
        mainDelayLabel->setObjectName(QStringLiteral("mainDelayLabel"));

        formLayout_7->setWidget(0, QFormLayout::LabelRole, mainDelayLabel);

        mainScreenDelayspinBox = new PhLockableSpinBox(delayGroupBox);
        mainScreenDelayspinBox->setObjectName(QStringLiteral("mainScreenDelayspinBox"));
        mainScreenDelayspinBox->setMaximum(1000);
        mainScreenDelayspinBox->setSingleStep(10);

        formLayout_7->setWidget(0, QFormLayout::FieldRole, mainScreenDelayspinBox);

        label_2 = new QLabel(delayGroupBox);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout_7->setWidget(1, QFormLayout::LabelRole, label_2);

        secondScreenDelaySpinBox = new PhLockableSpinBox(delayGroupBox);
        secondScreenDelaySpinBox->setObjectName(QStringLiteral("secondScreenDelaySpinBox"));
        secondScreenDelaySpinBox->setMaximum(1000);
        secondScreenDelaySpinBox->setSingleStep(10);

        formLayout_7->setWidget(1, QFormLayout::FieldRole, secondScreenDelaySpinBox);


        verticalLayout->addWidget(delayGroupBox);

        pipGroupBox = new QGroupBox(tabGeneral);
        pipGroupBox->setObjectName(QStringLiteral("pipGroupBox"));
        formLayout_6 = new QFormLayout(pipGroupBox);
        formLayout_6->setSpacing(6);
        formLayout_6->setContentsMargins(11, 11, 11, 11);
        formLayout_6->setObjectName(QStringLiteral("formLayout_6"));
        pipRatioLabel = new QLabel(pipGroupBox);
        pipRatioLabel->setObjectName(QStringLiteral("pipRatioLabel"));

        formLayout_6->setWidget(1, QFormLayout::LabelRole, pipRatioLabel);

        pipOffsetLabel = new QLabel(pipGroupBox);
        pipOffsetLabel->setObjectName(QStringLiteral("pipOffsetLabel"));

        formLayout_6->setWidget(0, QFormLayout::LabelRole, pipOffsetLabel);

        pipOffsetSpinBox = new QSpinBox(pipGroupBox);
        pipOffsetSpinBox->setObjectName(QStringLiteral("pipOffsetSpinBox"));
        pipOffsetSpinBox->setMaximum(1000);
        pipOffsetSpinBox->setSingleStep(100);
        pipOffsetSpinBox->setValue(1000);

        formLayout_6->setWidget(0, QFormLayout::FieldRole, pipOffsetSpinBox);

        pipRatioSlider = new QSlider(pipGroupBox);
        pipRatioSlider->setObjectName(QStringLiteral("pipRatioSlider"));
        pipRatioSlider->setMaximum(50);
        pipRatioSlider->setSingleStep(10);
        pipRatioSlider->setSliderPosition(30);
        pipRatioSlider->setOrientation(Qt::Horizontal);

        formLayout_6->setWidget(1, QFormLayout::FieldRole, pipRatioSlider);

        pipLeftPositionRadioButton = new QRadioButton(pipGroupBox);
        pipLeftPositionRadioButton->setObjectName(QStringLiteral("pipLeftPositionRadioButton"));

        formLayout_6->setWidget(3, QFormLayout::FieldRole, pipLeftPositionRadioButton);

        pipPositionLabel = new QLabel(pipGroupBox);
        pipPositionLabel->setObjectName(QStringLiteral("pipPositionLabel"));

        formLayout_6->setWidget(3, QFormLayout::LabelRole, pipPositionLabel);

        pipRightPositionRadioButton = new QRadioButton(pipGroupBox);
        pipRightPositionRadioButton->setObjectName(QStringLiteral("pipRightPositionRadioButton"));

        formLayout_6->setWidget(4, QFormLayout::FieldRole, pipRightPositionRadioButton);


        verticalLayout->addWidget(pipGroupBox);

        tabWidget->addTab(tabGeneral, QString());
        tabStrip = new QWidget();
        tabStrip->setObjectName(QStringLiteral("tabStrip"));
        formLayout = new QFormLayout(tabStrip);
        formLayout->setSpacing(6);
        formLayout->setContentsMargins(11, 11, 11, 11);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        lblSpeed = new QLabel(tabStrip);
        lblSpeed->setObjectName(QStringLiteral("lblSpeed"));

        formLayout->setWidget(0, QFormLayout::LabelRole, lblSpeed);

        spinBoxSpeed = new QSpinBox(tabStrip);
        spinBoxSpeed->setObjectName(QStringLiteral("spinBoxSpeed"));
        spinBoxSpeed->setMinimum(1);
        spinBoxSpeed->setMaximum(100);
        spinBoxSpeed->setValue(12);

        formLayout->setWidget(0, QFormLayout::FieldRole, spinBoxSpeed);

        fontLabel = new QLabel(tabStrip);
        fontLabel->setObjectName(QStringLiteral("fontLabel"));

        formLayout->setWidget(3, QFormLayout::LabelRole, fontLabel);

        fontComboBox = new QFontComboBox(tabStrip);
        fontComboBox->setObjectName(QStringLiteral("fontComboBox"));

        formLayout->setWidget(3, QFormLayout::FieldRole, fontComboBox);

        lblStripHeight = new QLabel(tabStrip);
        lblStripHeight->setObjectName(QStringLiteral("lblStripHeight"));

        formLayout->setWidget(4, QFormLayout::LabelRole, lblStripHeight);

        sliderStripHeight = new QSlider(tabStrip);
        sliderStripHeight->setObjectName(QStringLiteral("sliderStripHeight"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(sliderStripHeight->sizePolicy().hasHeightForWidth());
        sliderStripHeight->setSizePolicy(sizePolicy);
        sliderStripHeight->setMaximumSize(QSize(22, 16777215));
        sliderStripHeight->setLayoutDirection(Qt::LeftToRight);
        sliderStripHeight->setMaximum(100);
        sliderStripHeight->setOrientation(Qt::Vertical);

        formLayout->setWidget(4, QFormLayout::FieldRole, sliderStripHeight);

        tabWidget->addTab(tabStrip, QString());
        tabSynchro = new QWidget();
        tabSynchro->setObjectName(QStringLiteral("tabSynchro"));
        verticalLayout_2 = new QVBoxLayout(tabSynchro);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        noSyncRadioButton = new QRadioButton(tabSynchro);
        noSyncRadioButton->setObjectName(QStringLiteral("noSyncRadioButton"));

        verticalLayout_2->addWidget(noSyncRadioButton);

        sonyRadioButton = new QRadioButton(tabSynchro);
        sonyRadioButton->setObjectName(QStringLiteral("sonyRadioButton"));

        verticalLayout_2->addWidget(sonyRadioButton);

        sonyFrame = new QFrame(tabSynchro);
        sonyFrame->setObjectName(QStringLiteral("sonyFrame"));
        sonyFrame->setFrameShape(QFrame::StyledPanel);
        sonyFrame->setFrameShadow(QFrame::Raised);
        formLayout_4 = new QFormLayout(sonyFrame);
        formLayout_4->setSpacing(6);
        formLayout_4->setContentsMargins(11, 11, 11, 11);
        formLayout_4->setObjectName(QStringLiteral("formLayout_4"));
        sonyCommunicationTimeCodeTypeLabel = new QLabel(sonyFrame);
        sonyCommunicationTimeCodeTypeLabel->setObjectName(QStringLiteral("sonyCommunicationTimeCodeTypeLabel"));

        formLayout_4->setWidget(0, QFormLayout::LabelRole, sonyCommunicationTimeCodeTypeLabel);

        sonyCommunicationTimeCodeTypeComboBox = new QComboBox(sonyFrame);
        sonyCommunicationTimeCodeTypeComboBox->setObjectName(QStringLiteral("sonyCommunicationTimeCodeTypeComboBox"));

        formLayout_4->setWidget(0, QFormLayout::FieldRole, sonyCommunicationTimeCodeTypeComboBox);

        sonyVideoSyncTimeCodeTypeLabel = new QLabel(sonyFrame);
        sonyVideoSyncTimeCodeTypeLabel->setObjectName(QStringLiteral("sonyVideoSyncTimeCodeTypeLabel"));

        formLayout_4->setWidget(1, QFormLayout::LabelRole, sonyVideoSyncTimeCodeTypeLabel);

        sonyVideoSyncTimeCodeTypeComboBox = new QComboBox(sonyFrame);
        sonyVideoSyncTimeCodeTypeComboBox->setObjectName(QStringLiteral("sonyVideoSyncTimeCodeTypeComboBox"));

        formLayout_4->setWidget(1, QFormLayout::FieldRole, sonyVideoSyncTimeCodeTypeComboBox);


        verticalLayout_2->addWidget(sonyFrame);

        ltcRadioButton = new QRadioButton(tabSynchro);
        ltcRadioButton->setObjectName(QStringLiteral("ltcRadioButton"));

        verticalLayout_2->addWidget(ltcRadioButton);

        ltcFrame = new QFrame(tabSynchro);
        ltcFrame->setObjectName(QStringLiteral("ltcFrame"));
        ltcFrame->setFrameShape(QFrame::StyledPanel);
        ltcFrame->setFrameShadow(QFrame::Raised);
        formLayout_2 = new QFormLayout(ltcFrame);
        formLayout_2->setSpacing(6);
        formLayout_2->setContentsMargins(11, 11, 11, 11);
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        ltcInputPortLabel = new QLabel(ltcFrame);
        ltcInputPortLabel->setObjectName(QStringLiteral("ltcInputPortLabel"));

        formLayout_2->setWidget(3, QFormLayout::LabelRole, ltcInputPortLabel);

        ltcInputPortComboBox = new QComboBox(ltcFrame);
        ltcInputPortComboBox->setObjectName(QStringLiteral("ltcInputPortComboBox"));

        formLayout_2->setWidget(3, QFormLayout::FieldRole, ltcInputPortComboBox);

        ltcTimeCodeTypeComboBox = new QComboBox(ltcFrame);
        ltcTimeCodeTypeComboBox->setObjectName(QStringLiteral("ltcTimeCodeTypeComboBox"));

        formLayout_2->setWidget(4, QFormLayout::FieldRole, ltcTimeCodeTypeComboBox);

        ltcTimeCodeTypeLabel = new QLabel(ltcFrame);
        ltcTimeCodeTypeLabel->setObjectName(QStringLiteral("ltcTimeCodeTypeLabel"));

        formLayout_2->setWidget(4, QFormLayout::LabelRole, ltcTimeCodeTypeLabel);


        verticalLayout_2->addWidget(ltcFrame);

        mtcRadioButton = new QRadioButton(tabSynchro);
        mtcRadioButton->setObjectName(QStringLiteral("mtcRadioButton"));

        verticalLayout_2->addWidget(mtcRadioButton);

        mtcFrame = new QFrame(tabSynchro);
        mtcFrame->setObjectName(QStringLiteral("mtcFrame"));
        mtcFrame->setFrameShape(QFrame::StyledPanel);
        mtcFrame->setFrameShadow(QFrame::Raised);
        formLayout_3 = new QFormLayout(mtcFrame);
        formLayout_3->setSpacing(6);
        formLayout_3->setContentsMargins(11, 11, 11, 11);
        formLayout_3->setObjectName(QStringLiteral("formLayout_3"));
        mtcExistingInputPortComboBox = new QComboBox(mtcFrame);
        mtcExistingInputPortComboBox->setObjectName(QStringLiteral("mtcExistingInputPortComboBox"));

        formLayout_3->setWidget(1, QFormLayout::FieldRole, mtcExistingInputPortComboBox);

        mtcExistingInputPortRadioButton = new QRadioButton(mtcFrame);
        mtcExistingInputPortRadioButton->setObjectName(QStringLiteral("mtcExistingInputPortRadioButton"));

        formLayout_3->setWidget(1, QFormLayout::LabelRole, mtcExistingInputPortRadioButton);

        mtcVirtualInputPortRadioButton = new QRadioButton(mtcFrame);
        mtcVirtualInputPortRadioButton->setObjectName(QStringLiteral("mtcVirtualInputPortRadioButton"));

        formLayout_3->setWidget(0, QFormLayout::LabelRole, mtcVirtualInputPortRadioButton);

        mtcVirtualInputPortLineEdit = new QLineEdit(mtcFrame);
        mtcVirtualInputPortLineEdit->setObjectName(QStringLiteral("mtcVirtualInputPortLineEdit"));

        formLayout_3->setWidget(0, QFormLayout::FieldRole, mtcVirtualInputPortLineEdit);

        mtcForce24as2398CheckBox = new QCheckBox(mtcFrame);
        mtcForce24as2398CheckBox->setObjectName(QStringLiteral("mtcForce24as2398CheckBox"));

        formLayout_3->setWidget(2, QFormLayout::SpanningRole, mtcForce24as2398CheckBox);


        verticalLayout_2->addWidget(mtcFrame);

        mmcCheckBox = new QCheckBox(tabSynchro);
        mmcCheckBox->setObjectName(QStringLiteral("mmcCheckBox"));

        verticalLayout_2->addWidget(mmcCheckBox);

        mmcFrame = new QFrame(tabSynchro);
        mmcFrame->setObjectName(QStringLiteral("mmcFrame"));
        mmcFrame->setFrameShape(QFrame::StyledPanel);
        mmcFrame->setFrameShadow(QFrame::Raised);
        formLayout_5 = new QFormLayout(mmcFrame);
        formLayout_5->setSpacing(6);
        formLayout_5->setContentsMargins(11, 11, 11, 11);
        formLayout_5->setObjectName(QStringLiteral("formLayout_5"));
        mmcOutputPortComboBox = new QComboBox(mmcFrame);
        mmcOutputPortComboBox->setObjectName(QStringLiteral("mmcOutputPortComboBox"));

        formLayout_5->setWidget(0, QFormLayout::FieldRole, mmcOutputPortComboBox);

        mmcOutputPortLabel = new QLabel(mmcFrame);
        mmcOutputPortLabel->setObjectName(QStringLiteral("mmcOutputPortLabel"));

        formLayout_5->setWidget(0, QFormLayout::LabelRole, mmcOutputPortLabel);


        verticalLayout_2->addWidget(mmcFrame);

        tabWidget->addTab(tabSynchro, QString());

        verticalLayout_0->addWidget(tabWidget);

        buttonBox = new PhDialogButtonBox(PreferencesDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout_0->addWidget(buttonBox);


        retranslateUi(PreferencesDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), PreferencesDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), PreferencesDialog, SLOT(reject()));

        tabWidget->setCurrentIndex(2);


        QMetaObject::connectSlotsByName(PreferencesDialog);
    } // setupUi

    void retranslateUi(QDialog *PreferencesDialog)
    {
        PreferencesDialog->setWindowTitle(QApplication::translate("PreferencesDialog", "Preferences", Q_NULLPTR));
        interfaceGroupBox->setTitle(QApplication::translate("PreferencesDialog", "Interface:", Q_NULLPTR));
        label_5->setText(QApplication::translate("PreferencesDialog", "Language:", Q_NULLPTR));
        delayGroupBox->setTitle(QApplication::translate("PreferencesDialog", "Delay:", Q_NULLPTR));
        mainDelayLabel->setText(QApplication::translate("PreferencesDialog", "Main screen delay (ms):", Q_NULLPTR));
        label_2->setText(QApplication::translate("PreferencesDialog", "Second screen delay (ms):", Q_NULLPTR));
        pipGroupBox->setTitle(QApplication::translate("PreferencesDialog", "Picture in picture:", Q_NULLPTR));
        pipRatioLabel->setText(QApplication::translate("PreferencesDialog", "Ratio:", Q_NULLPTR));
        pipOffsetLabel->setText(QApplication::translate("PreferencesDialog", "Offset (ms):", Q_NULLPTR));
        pipLeftPositionRadioButton->setText(QApplication::translate("PreferencesDialog", "Left", Q_NULLPTR));
        pipPositionLabel->setText(QApplication::translate("PreferencesDialog", "Position:", Q_NULLPTR));
        pipRightPositionRadioButton->setText(QApplication::translate("PreferencesDialog", "Right", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tabGeneral), QApplication::translate("PreferencesDialog", "General", Q_NULLPTR));
        lblSpeed->setText(QApplication::translate("PreferencesDialog", "Speed:", Q_NULLPTR));
        fontLabel->setText(QApplication::translate("PreferencesDialog", "Font:", Q_NULLPTR));
        lblStripHeight->setText(QApplication::translate("PreferencesDialog", "Strip Height", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tabStrip), QApplication::translate("PreferencesDialog", "Strip", Q_NULLPTR));
        noSyncRadioButton->setText(QApplication::translate("PreferencesDialog", "No sync", Q_NULLPTR));
        sonyRadioButton->setText(QApplication::translate("PreferencesDialog", "Sony 9 pin", Q_NULLPTR));
        sonyCommunicationTimeCodeTypeLabel->setText(QApplication::translate("PreferencesDialog", "Communication framerate:", Q_NULLPTR));
        sonyCommunicationTimeCodeTypeComboBox->clear();
        sonyCommunicationTimeCodeTypeComboBox->insertItems(0, QStringList()
         << QApplication::translate("PreferencesDialog", "23.98 fps", Q_NULLPTR)
         << QApplication::translate("PreferencesDialog", "24 fps", Q_NULLPTR)
         << QApplication::translate("PreferencesDialog", "25 fps", Q_NULLPTR)
         << QApplication::translate("PreferencesDialog", "29.97 fps", Q_NULLPTR)
         << QApplication::translate("PreferencesDialog", "30 fps", Q_NULLPTR)
        );
        sonyVideoSyncTimeCodeTypeLabel->setText(QApplication::translate("PreferencesDialog", "Video sync framerate:", Q_NULLPTR));
        sonyVideoSyncTimeCodeTypeComboBox->clear();
        sonyVideoSyncTimeCodeTypeComboBox->insertItems(0, QStringList()
         << QApplication::translate("PreferencesDialog", "23.98 fps", Q_NULLPTR)
         << QApplication::translate("PreferencesDialog", "24 fps", Q_NULLPTR)
         << QApplication::translate("PreferencesDialog", "25 fps", Q_NULLPTR)
         << QApplication::translate("PreferencesDialog", "29.97 fps", Q_NULLPTR)
         << QApplication::translate("PreferencesDialog", "30 fps", Q_NULLPTR)
        );
        ltcRadioButton->setText(QApplication::translate("PreferencesDialog", "LTC", Q_NULLPTR));
        ltcInputPortLabel->setText(QApplication::translate("PreferencesDialog", "Audio input port:", Q_NULLPTR));
        ltcTimeCodeTypeComboBox->clear();
        ltcTimeCodeTypeComboBox->insertItems(0, QStringList()
         << QApplication::translate("PreferencesDialog", "23.98 fps", Q_NULLPTR)
         << QApplication::translate("PreferencesDialog", "24 fps", Q_NULLPTR)
         << QApplication::translate("PreferencesDialog", "25 fps", Q_NULLPTR)
         << QApplication::translate("PreferencesDialog", "29.97 fps", Q_NULLPTR)
         << QApplication::translate("PreferencesDialog", "30 fps", Q_NULLPTR)
        );
        ltcTimeCodeTypeLabel->setText(QApplication::translate("PreferencesDialog", "Timecode type:", Q_NULLPTR));
        mtcRadioButton->setText(QApplication::translate("PreferencesDialog", "Midi timecode", Q_NULLPTR));
        mtcExistingInputPortRadioButton->setText(QApplication::translate("PreferencesDialog", "Read from existing port:", Q_NULLPTR));
        mtcVirtualInputPortRadioButton->setText(QApplication::translate("PreferencesDialog", "Read from virtual port:", Q_NULLPTR));
        mtcForce24as2398CheckBox->setText(QApplication::translate("PreferencesDialog", "Force 24 fps as 23.98 when reading MTC", Q_NULLPTR));
        mmcCheckBox->setText(QApplication::translate("PreferencesDialog", "Midi machine control:", Q_NULLPTR));
        mmcOutputPortLabel->setText(QApplication::translate("PreferencesDialog", "Send MMC message to port:", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tabSynchro), QApplication::translate("PreferencesDialog", "Synchronization", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class PreferencesDialog: public Ui_PreferencesDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PREFERENCESDIALOG_H

/****************************************************************************
** Meta object code from reading C++ file 'LTCToolWindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.9)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../LTCToolWindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'LTCToolWindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.9. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_LTCToolWindow_t {
    QByteArrayData data[22];
    char stringdata0[362];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_LTCToolWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_LTCToolWindow_t qt_meta_stringdata_LTCToolWindow = {
    {
QT_MOC_LITERAL(0, 0, 13), // "LTCToolWindow"
QT_MOC_LITERAL(1, 14, 28), // "on_actionSet_TC_In_triggered"
QT_MOC_LITERAL(2, 43, 0), // ""
QT_MOC_LITERAL(3, 44, 29), // "on_actionSet_TC_Out_triggered"
QT_MOC_LITERAL(4, 74, 30), // "on_actionPreferences_triggered"
QT_MOC_LITERAL(5, 105, 19), // "onWriterTimeChanged"
QT_MOC_LITERAL(6, 125, 6), // "PhTime"
QT_MOC_LITERAL(7, 132, 4), // "time"
QT_MOC_LITERAL(8, 137, 27), // "onWriterTimeCodeTypeChanged"
QT_MOC_LITERAL(9, 165, 14), // "PhTimeCodeType"
QT_MOC_LITERAL(10, 180, 6), // "tcType"
QT_MOC_LITERAL(11, 187, 19), // "onReaderTimeChanged"
QT_MOC_LITERAL(12, 207, 19), // "onReaderRateChanged"
QT_MOC_LITERAL(13, 227, 6), // "PhRate"
QT_MOC_LITERAL(14, 234, 16), // "updateReaderInfo"
QT_MOC_LITERAL(15, 251, 27), // "on_generateCheckBox_clicked"
QT_MOC_LITERAL(16, 279, 7), // "checked"
QT_MOC_LITERAL(17, 287, 23), // "on_readCheckBox_clicked"
QT_MOC_LITERAL(18, 311, 16), // "onAudioProcessed"
QT_MOC_LITERAL(19, 328, 8), // "minLevel"
QT_MOC_LITERAL(20, 337, 8), // "maxLevel"
QT_MOC_LITERAL(21, 346, 15) // "onTCTypeChanged"

    },
    "LTCToolWindow\0on_actionSet_TC_In_triggered\0"
    "\0on_actionSet_TC_Out_triggered\0"
    "on_actionPreferences_triggered\0"
    "onWriterTimeChanged\0PhTime\0time\0"
    "onWriterTimeCodeTypeChanged\0PhTimeCodeType\0"
    "tcType\0onReaderTimeChanged\0"
    "onReaderRateChanged\0PhRate\0updateReaderInfo\0"
    "on_generateCheckBox_clicked\0checked\0"
    "on_readCheckBox_clicked\0onAudioProcessed\0"
    "minLevel\0maxLevel\0onTCTypeChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_LTCToolWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   74,    2, 0x08 /* Private */,
       3,    0,   75,    2, 0x08 /* Private */,
       4,    0,   76,    2, 0x08 /* Private */,
       5,    1,   77,    2, 0x08 /* Private */,
       8,    1,   80,    2, 0x08 /* Private */,
      11,    1,   83,    2, 0x08 /* Private */,
      12,    1,   86,    2, 0x08 /* Private */,
      14,    0,   89,    2, 0x08 /* Private */,
      15,    1,   90,    2, 0x08 /* Private */,
      17,    1,   93,    2, 0x08 /* Private */,
      18,    2,   96,    2, 0x08 /* Private */,
      21,    1,  101,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void, 0x80000000 | 6,    2,
    QMetaType::Void, 0x80000000 | 13,    2,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   16,
    QMetaType::Void, QMetaType::Bool,   16,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   19,   20,
    QMetaType::Void, 0x80000000 | 9,   10,

       0        // eod
};

void LTCToolWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        LTCToolWindow *_t = static_cast<LTCToolWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_actionSet_TC_In_triggered(); break;
        case 1: _t->on_actionSet_TC_Out_triggered(); break;
        case 2: _t->on_actionPreferences_triggered(); break;
        case 3: _t->onWriterTimeChanged((*reinterpret_cast< PhTime(*)>(_a[1]))); break;
        case 4: _t->onWriterTimeCodeTypeChanged((*reinterpret_cast< PhTimeCodeType(*)>(_a[1]))); break;
        case 5: _t->onReaderTimeChanged((*reinterpret_cast< PhTime(*)>(_a[1]))); break;
        case 6: _t->onReaderRateChanged((*reinterpret_cast< PhRate(*)>(_a[1]))); break;
        case 7: _t->updateReaderInfo(); break;
        case 8: _t->on_generateCheckBox_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 9: _t->on_readCheckBox_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: _t->onAudioProcessed((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 11: _t->onTCTypeChanged((*reinterpret_cast< PhTimeCodeType(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject LTCToolWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_LTCToolWindow.data,
      qt_meta_data_LTCToolWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *LTCToolWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *LTCToolWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_LTCToolWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int LTCToolWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 12)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 12;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE

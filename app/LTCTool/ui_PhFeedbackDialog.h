/********************************************************************************
** Form generated from reading UI file 'PhFeedbackDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PHFEEDBACKDIALOG_H
#define UI_PHFEEDBACKDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_PhFeedbackDialog
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *problemLabel;
    QLabel *labelMsg;

    void setupUi(QDialog *PhFeedbackDialog)
    {
        if (PhFeedbackDialog->objectName().isEmpty())
            PhFeedbackDialog->setObjectName(QStringLiteral("PhFeedbackDialog"));
        PhFeedbackDialog->resize(364, 194);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(PhFeedbackDialog->sizePolicy().hasHeightForWidth());
        PhFeedbackDialog->setSizePolicy(sizePolicy);
        PhFeedbackDialog->setMinimumSize(QSize(0, 0));
        verticalLayout = new QVBoxLayout(PhFeedbackDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        problemLabel = new QLabel(PhFeedbackDialog);
        problemLabel->setObjectName(QStringLiteral("problemLabel"));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        problemLabel->setFont(font);

        verticalLayout->addWidget(problemLabel);

        labelMsg = new QLabel(PhFeedbackDialog);
        labelMsg->setObjectName(QStringLiteral("labelMsg"));
        sizePolicy.setHeightForWidth(labelMsg->sizePolicy().hasHeightForWidth());
        labelMsg->setSizePolicy(sizePolicy);
        labelMsg->setWordWrap(true);
        labelMsg->setOpenExternalLinks(true);
        labelMsg->setTextInteractionFlags(Qt::TextBrowserInteraction);

        verticalLayout->addWidget(labelMsg);


        retranslateUi(PhFeedbackDialog);

        QMetaObject::connectSlotsByName(PhFeedbackDialog);
    } // setupUi

    void retranslateUi(QDialog *PhFeedbackDialog)
    {
        PhFeedbackDialog->setWindowTitle(QApplication::translate("PhFeedbackDialog", "Feedback report", Q_NULLPTR));
        problemLabel->setText(QApplication::translate("PhFeedbackDialog", "Having problem with %1 ?", Q_NULLPTR));
        labelMsg->setText(QApplication::translate("PhFeedbackDialog", "<p>To help us improve this software, please send your feedback on <a href=\"https://github.com/JokerSync/Joker/issues\">GitHub Issues</a>.</p>\n"
"\n"
"<p>Thank you!</p>", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class PhFeedbackDialog: public Ui_PhFeedbackDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PHFEEDBACKDIALOG_H

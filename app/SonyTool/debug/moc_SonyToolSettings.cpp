/****************************************************************************
** Meta object code from reading C++ file 'SonyToolSettings.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.9)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../SonyToolSettings.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'SonyToolSettings.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.9. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SonyToolSettings_t {
    QByteArrayData data[30];
    char stringdata0[704];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SonyToolSettings_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SonyToolSettings_t qt_meta_stringdata_SonyToolSettings = {
    {
QT_MOC_LITERAL(0, 0, 16), // "SonyToolSettings"
QT_MOC_LITERAL(1, 17, 18), // "videoSyncUpChanged"
QT_MOC_LITERAL(2, 36, 0), // ""
QT_MOC_LITERAL(3, 37, 18), // "sonyDevice1Changed"
QT_MOC_LITERAL(4, 56, 18), // "sonyDevice2Changed"
QT_MOC_LITERAL(5, 75, 19), // "sonyFastRateChanged"
QT_MOC_LITERAL(6, 95, 26), // "sonySlavePortSuffixChanged"
QT_MOC_LITERAL(7, 122, 27), // "sonyMasterPortSuffixChanged"
QT_MOC_LITERAL(8, 150, 42), // "sonyMasterCommunicationTimeCo..."
QT_MOC_LITERAL(9, 193, 41), // "sonySlaveCommunicationTimeCod..."
QT_MOC_LITERAL(10, 235, 38), // "sonyMasterVideoSyncTimeCodeTy..."
QT_MOC_LITERAL(11, 274, 37), // "sonySlaveVideoSyncTimeCodeTyp..."
QT_MOC_LITERAL(12, 312, 22), // "sonySlaveActiveChanged"
QT_MOC_LITERAL(13, 335, 23), // "sonyMasterActiveChanged"
QT_MOC_LITERAL(14, 359, 24), // "useVideoSlaveSyncChanged"
QT_MOC_LITERAL(15, 384, 25), // "useVideoMasterSyncChanged"
QT_MOC_LITERAL(16, 410, 11), // "videoSyncUp"
QT_MOC_LITERAL(17, 422, 11), // "sonyDevice1"
QT_MOC_LITERAL(18, 434, 11), // "sonyDevice2"
QT_MOC_LITERAL(19, 446, 12), // "sonyFastRate"
QT_MOC_LITERAL(20, 459, 19), // "sonySlavePortSuffix"
QT_MOC_LITERAL(21, 479, 20), // "sonyMasterPortSuffix"
QT_MOC_LITERAL(22, 500, 35), // "sonyMasterCommunicationTimeCo..."
QT_MOC_LITERAL(23, 536, 34), // "sonySlaveCommunicationTimeCod..."
QT_MOC_LITERAL(24, 571, 31), // "sonyMasterVideoSyncTimeCodeType"
QT_MOC_LITERAL(25, 603, 30), // "sonySlaveVideoSyncTimeCodeType"
QT_MOC_LITERAL(26, 634, 15), // "sonySlaveActive"
QT_MOC_LITERAL(27, 650, 16), // "sonyMasterActive"
QT_MOC_LITERAL(28, 667, 17), // "useVideoSlaveSync"
QT_MOC_LITERAL(29, 685, 18) // "useVideoMasterSync"

    },
    "SonyToolSettings\0videoSyncUpChanged\0"
    "\0sonyDevice1Changed\0sonyDevice2Changed\0"
    "sonyFastRateChanged\0sonySlavePortSuffixChanged\0"
    "sonyMasterPortSuffixChanged\0"
    "sonyMasterCommunicationTimeCodeTypeChanged\0"
    "sonySlaveCommunicationTimeCodeTypeChanged\0"
    "sonyMasterVideoSyncTimeCodeTypeChanged\0"
    "sonySlaveVideoSyncTimeCodeTypeChanged\0"
    "sonySlaveActiveChanged\0sonyMasterActiveChanged\0"
    "useVideoSlaveSyncChanged\0"
    "useVideoMasterSyncChanged\0videoSyncUp\0"
    "sonyDevice1\0sonyDevice2\0sonyFastRate\0"
    "sonySlavePortSuffix\0sonyMasterPortSuffix\0"
    "sonyMasterCommunicationTimeCodeType\0"
    "sonySlaveCommunicationTimeCodeType\0"
    "sonyMasterVideoSyncTimeCodeType\0"
    "sonySlaveVideoSyncTimeCodeType\0"
    "sonySlaveActive\0sonyMasterActive\0"
    "useVideoSlaveSync\0useVideoMasterSync"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SonyToolSettings[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
      14,   98, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      14,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   84,    2, 0x06 /* Public */,
       3,    0,   85,    2, 0x06 /* Public */,
       4,    0,   86,    2, 0x06 /* Public */,
       5,    0,   87,    2, 0x06 /* Public */,
       6,    0,   88,    2, 0x06 /* Public */,
       7,    0,   89,    2, 0x06 /* Public */,
       8,    0,   90,    2, 0x06 /* Public */,
       9,    0,   91,    2, 0x06 /* Public */,
      10,    0,   92,    2, 0x06 /* Public */,
      11,    0,   93,    2, 0x06 /* Public */,
      12,    0,   94,    2, 0x06 /* Public */,
      13,    0,   95,    2, 0x06 /* Public */,
      14,    0,   96,    2, 0x06 /* Public */,
      15,    0,   97,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      16, QMetaType::Bool, 0x00495103,
      17, QMetaType::UChar, 0x00495103,
      18, QMetaType::UChar, 0x00495103,
      19, QMetaType::Float, 0x00495103,
      20, QMetaType::QString, 0x00495103,
      21, QMetaType::QString, 0x00495103,
      22, QMetaType::Int, 0x00495103,
      23, QMetaType::Int, 0x00495103,
      24, QMetaType::Int, 0x00495103,
      25, QMetaType::Int, 0x00495103,
      26, QMetaType::Bool, 0x00495103,
      27, QMetaType::Bool, 0x00495103,
      28, QMetaType::Bool, 0x00495103,
      29, QMetaType::Bool, 0x00495103,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       6,
       7,
       8,
       9,
      10,
      11,
      12,
      13,

       0        // eod
};

void SonyToolSettings::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SonyToolSettings *_t = static_cast<SonyToolSettings *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->videoSyncUpChanged(); break;
        case 1: _t->sonyDevice1Changed(); break;
        case 2: _t->sonyDevice2Changed(); break;
        case 3: _t->sonyFastRateChanged(); break;
        case 4: _t->sonySlavePortSuffixChanged(); break;
        case 5: _t->sonyMasterPortSuffixChanged(); break;
        case 6: _t->sonyMasterCommunicationTimeCodeTypeChanged(); break;
        case 7: _t->sonySlaveCommunicationTimeCodeTypeChanged(); break;
        case 8: _t->sonyMasterVideoSyncTimeCodeTypeChanged(); break;
        case 9: _t->sonySlaveVideoSyncTimeCodeTypeChanged(); break;
        case 10: _t->sonySlaveActiveChanged(); break;
        case 11: _t->sonyMasterActiveChanged(); break;
        case 12: _t->useVideoSlaveSyncChanged(); break;
        case 13: _t->useVideoMasterSyncChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (SonyToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SonyToolSettings::videoSyncUpChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (SonyToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SonyToolSettings::sonyDevice1Changed)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (SonyToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SonyToolSettings::sonyDevice2Changed)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (SonyToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SonyToolSettings::sonyFastRateChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (SonyToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SonyToolSettings::sonySlavePortSuffixChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (SonyToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SonyToolSettings::sonyMasterPortSuffixChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (SonyToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SonyToolSettings::sonyMasterCommunicationTimeCodeTypeChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (SonyToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SonyToolSettings::sonySlaveCommunicationTimeCodeTypeChanged)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (SonyToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SonyToolSettings::sonyMasterVideoSyncTimeCodeTypeChanged)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (SonyToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SonyToolSettings::sonySlaveVideoSyncTimeCodeTypeChanged)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (SonyToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SonyToolSettings::sonySlaveActiveChanged)) {
                *result = 10;
                return;
            }
        }
        {
            typedef void (SonyToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SonyToolSettings::sonyMasterActiveChanged)) {
                *result = 11;
                return;
            }
        }
        {
            typedef void (SonyToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SonyToolSettings::useVideoSlaveSyncChanged)) {
                *result = 12;
                return;
            }
        }
        {
            typedef void (SonyToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&SonyToolSettings::useVideoMasterSyncChanged)) {
                *result = 13;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        SonyToolSettings *_t = static_cast<SonyToolSettings *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->videoSyncUp(); break;
        case 1: *reinterpret_cast< unsigned char*>(_v) = _t->sonyDevice1(); break;
        case 2: *reinterpret_cast< unsigned char*>(_v) = _t->sonyDevice2(); break;
        case 3: *reinterpret_cast< float*>(_v) = _t->sonyFastRate(); break;
        case 4: *reinterpret_cast< QString*>(_v) = _t->sonySlavePortSuffix(); break;
        case 5: *reinterpret_cast< QString*>(_v) = _t->sonyMasterPortSuffix(); break;
        case 6: *reinterpret_cast< int*>(_v) = _t->sonyMasterCommunicationTimeCodeType(); break;
        case 7: *reinterpret_cast< int*>(_v) = _t->sonySlaveCommunicationTimeCodeType(); break;
        case 8: *reinterpret_cast< int*>(_v) = _t->sonyMasterVideoSyncTimeCodeType(); break;
        case 9: *reinterpret_cast< int*>(_v) = _t->sonySlaveVideoSyncTimeCodeType(); break;
        case 10: *reinterpret_cast< bool*>(_v) = _t->sonySlaveActive(); break;
        case 11: *reinterpret_cast< bool*>(_v) = _t->sonyMasterActive(); break;
        case 12: *reinterpret_cast< bool*>(_v) = _t->useVideoSlaveSync(); break;
        case 13: *reinterpret_cast< bool*>(_v) = _t->useVideoMasterSync(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        SonyToolSettings *_t = static_cast<SonyToolSettings *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setVideoSyncUp(*reinterpret_cast< bool*>(_v)); break;
        case 1: _t->setSonyDevice1(*reinterpret_cast< unsigned char*>(_v)); break;
        case 2: _t->setSonyDevice2(*reinterpret_cast< unsigned char*>(_v)); break;
        case 3: _t->setSonyFastRate(*reinterpret_cast< float*>(_v)); break;
        case 4: _t->setSonySlavePortSuffix(*reinterpret_cast< QString*>(_v)); break;
        case 5: _t->setSonyMasterPortSuffix(*reinterpret_cast< QString*>(_v)); break;
        case 6: _t->setSonyMasterCommunicationTimeCodeType(*reinterpret_cast< int*>(_v)); break;
        case 7: _t->setSonySlaveCommunicationTimeCodeType(*reinterpret_cast< int*>(_v)); break;
        case 8: _t->setSonyMasterVideoSyncTimeCodeType(*reinterpret_cast< int*>(_v)); break;
        case 9: _t->setSonySlaveVideoSyncTimeCodeType(*reinterpret_cast< int*>(_v)); break;
        case 10: _t->setSonySlaveActive(*reinterpret_cast< bool*>(_v)); break;
        case 11: _t->setSonyMasterActive(*reinterpret_cast< bool*>(_v)); break;
        case 12: _t->setUseVideoSlaveSync(*reinterpret_cast< bool*>(_v)); break;
        case 13: _t->setUseVideoMasterSync(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_a);
}

const QMetaObject SonyToolSettings::staticMetaObject = {
    { &PhGenericSettings::staticMetaObject, qt_meta_stringdata_SonyToolSettings.data,
      qt_meta_data_SonyToolSettings,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *SonyToolSettings::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SonyToolSettings::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SonyToolSettings.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "PhSonySettings"))
        return static_cast< PhSonySettings*>(this);
    return PhGenericSettings::qt_metacast(_clname);
}

int SonyToolSettings::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PhGenericSettings::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 14)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 14;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 14;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 14;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 14;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 14;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 14;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void SonyToolSettings::videoSyncUpChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void SonyToolSettings::sonyDevice1Changed()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void SonyToolSettings::sonyDevice2Changed()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void SonyToolSettings::sonyFastRateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void SonyToolSettings::sonySlavePortSuffixChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void SonyToolSettings::sonyMasterPortSuffixChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void SonyToolSettings::sonyMasterCommunicationTimeCodeTypeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void SonyToolSettings::sonySlaveCommunicationTimeCodeTypeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void SonyToolSettings::sonyMasterVideoSyncTimeCodeTypeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void SonyToolSettings::sonySlaveVideoSyncTimeCodeTypeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, nullptr);
}

// SIGNAL 10
void SonyToolSettings::sonySlaveActiveChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, nullptr);
}

// SIGNAL 11
void SonyToolSettings::sonyMasterActiveChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 11, nullptr);
}

// SIGNAL 12
void SonyToolSettings::useVideoSlaveSyncChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 12, nullptr);
}

// SIGNAL 13
void SonyToolSettings::useVideoMasterSyncChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 13, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE

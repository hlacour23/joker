/****************************************************************************
** Meta object code from reading C++ file 'PhSonyMasterController.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.9)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../libs/PhSony/PhSonyMasterController.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'PhSonyMasterController.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.9. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_PhSonyMasterController_t {
    QByteArrayData data[26];
    char stringdata0[227];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PhSonyMasterController_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PhSonyMasterController_t qt_meta_stringdata_PhSonyMasterController = {
    {
QT_MOC_LITERAL(0, 0, 22), // "PhSonyMasterController"
QT_MOC_LITERAL(1, 23, 12), // "deviceIdData"
QT_MOC_LITERAL(2, 36, 0), // ""
QT_MOC_LITERAL(3, 37, 3), // "id1"
QT_MOC_LITERAL(4, 41, 3), // "id2"
QT_MOC_LITERAL(5, 45, 10), // "statusData"
QT_MOC_LITERAL(6, 56, 14), // "unsigned char*"
QT_MOC_LITERAL(7, 71, 6), // "offset"
QT_MOC_LITERAL(8, 78, 6), // "length"
QT_MOC_LITERAL(9, 85, 11), // "onVideoSync"
QT_MOC_LITERAL(10, 97, 17), // "deviceTypeRequest"
QT_MOC_LITERAL(11, 115, 4), // "play"
QT_MOC_LITERAL(12, 120, 4), // "stop"
QT_MOC_LITERAL(13, 125, 3), // "cue"
QT_MOC_LITERAL(14, 129, 6), // "PhTime"
QT_MOC_LITERAL(15, 136, 4), // "time"
QT_MOC_LITERAL(16, 141, 11), // "fastForward"
QT_MOC_LITERAL(17, 153, 6), // "rewind"
QT_MOC_LITERAL(18, 160, 3), // "jog"
QT_MOC_LITERAL(19, 164, 6), // "PhRate"
QT_MOC_LITERAL(20, 171, 4), // "rate"
QT_MOC_LITERAL(21, 176, 9), // "varispeed"
QT_MOC_LITERAL(22, 186, 7), // "shuttle"
QT_MOC_LITERAL(23, 194, 9), // "timeSense"
QT_MOC_LITERAL(24, 204, 11), // "statusSense"
QT_MOC_LITERAL(25, 216, 10) // "speedSense"

    },
    "PhSonyMasterController\0deviceIdData\0"
    "\0id1\0id2\0statusData\0unsigned char*\0"
    "offset\0length\0onVideoSync\0deviceTypeRequest\0"
    "play\0stop\0cue\0PhTime\0time\0fastForward\0"
    "rewind\0jog\0PhRate\0rate\0varispeed\0"
    "shuttle\0timeSense\0statusSense\0speedSense"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PhSonyMasterController[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,   89,    2, 0x06 /* Public */,
       5,    3,   94,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       9,    0,  101,    2, 0x0a /* Public */,
      10,    0,  102,    2, 0x0a /* Public */,
      11,    0,  103,    2, 0x0a /* Public */,
      12,    0,  104,    2, 0x0a /* Public */,
      13,    1,  105,    2, 0x0a /* Public */,
      16,    0,  108,    2, 0x0a /* Public */,
      17,    0,  109,    2, 0x0a /* Public */,
      18,    1,  110,    2, 0x0a /* Public */,
      21,    1,  113,    2, 0x0a /* Public */,
      22,    1,  116,    2, 0x0a /* Public */,
      23,    0,  119,    2, 0x0a /* Public */,
      24,    0,  120,    2, 0x0a /* Public */,
      25,    0,  121,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::UChar, QMetaType::UChar,    3,    4,
    QMetaType::Void, 0x80000000 | 6, QMetaType::Int, QMetaType::Int,    5,    7,    8,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 14,   15,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 19,   20,
    QMetaType::Void, 0x80000000 | 19,   20,
    QMetaType::Void, 0x80000000 | 19,   20,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void PhSonyMasterController::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        PhSonyMasterController *_t = static_cast<PhSonyMasterController *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->deviceIdData((*reinterpret_cast< unsigned char(*)>(_a[1])),(*reinterpret_cast< unsigned char(*)>(_a[2]))); break;
        case 1: _t->statusData((*reinterpret_cast< unsigned char*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 2: _t->onVideoSync(); break;
        case 3: _t->deviceTypeRequest(); break;
        case 4: _t->play(); break;
        case 5: _t->stop(); break;
        case 6: _t->cue((*reinterpret_cast< PhTime(*)>(_a[1]))); break;
        case 7: _t->fastForward(); break;
        case 8: _t->rewind(); break;
        case 9: _t->jog((*reinterpret_cast< PhRate(*)>(_a[1]))); break;
        case 10: _t->varispeed((*reinterpret_cast< PhRate(*)>(_a[1]))); break;
        case 11: _t->shuttle((*reinterpret_cast< PhRate(*)>(_a[1]))); break;
        case 12: _t->timeSense(); break;
        case 13: _t->statusSense(); break;
        case 14: _t->speedSense(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (PhSonyMasterController::*_t)(unsigned char , unsigned char );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PhSonyMasterController::deviceIdData)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (PhSonyMasterController::*_t)(unsigned char * , int , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PhSonyMasterController::statusData)) {
                *result = 1;
                return;
            }
        }
    }
}

const QMetaObject PhSonyMasterController::staticMetaObject = {
    { &PhSonyController::staticMetaObject, qt_meta_stringdata_PhSonyMasterController.data,
      qt_meta_data_PhSonyMasterController,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *PhSonyMasterController::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PhSonyMasterController::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PhSonyMasterController.stringdata0))
        return static_cast<void*>(this);
    return PhSonyController::qt_metacast(_clname);
}

int PhSonyMasterController::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PhSonyController::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 15)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 15;
    }
    return _id;
}

// SIGNAL 0
void PhSonyMasterController::deviceIdData(unsigned char _t1, unsigned char _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void PhSonyMasterController::statusData(unsigned char * _t1, int _t2, int _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE

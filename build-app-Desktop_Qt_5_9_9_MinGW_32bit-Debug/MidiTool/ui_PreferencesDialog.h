/********************************************************************************
** Form generated from reading UI file 'PreferencesDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PREFERENCESDIALOG_H
#define UI_PREFERENCESDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_PreferencesDialog
{
public:
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QFormLayout *formLayout;
    QLabel *lblDeviceOut;
    QComboBox *comboBoxOutput;
    QRadioButton *radioButtonVirtualPort;
    QRadioButton *radioButtonExistingPort;
    QLineEdit *lineEditInput;
    QComboBox *comboBoxInput;
    QCheckBox *checkBoxForce24as2398;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *PreferencesDialog)
    {
        if (PreferencesDialog->objectName().isEmpty())
            PreferencesDialog->setObjectName(QStringLiteral("PreferencesDialog"));
        PreferencesDialog->resize(401, 208);
        verticalLayout_2 = new QVBoxLayout(PreferencesDialog);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        formLayout = new QFormLayout();
        formLayout->setObjectName(QStringLiteral("formLayout"));
        lblDeviceOut = new QLabel(PreferencesDialog);
        lblDeviceOut->setObjectName(QStringLiteral("lblDeviceOut"));

        formLayout->setWidget(0, QFormLayout::LabelRole, lblDeviceOut);

        comboBoxOutput = new QComboBox(PreferencesDialog);
        comboBoxOutput->setObjectName(QStringLiteral("comboBoxOutput"));

        formLayout->setWidget(0, QFormLayout::FieldRole, comboBoxOutput);

        radioButtonVirtualPort = new QRadioButton(PreferencesDialog);
        radioButtonVirtualPort->setObjectName(QStringLiteral("radioButtonVirtualPort"));

        formLayout->setWidget(1, QFormLayout::LabelRole, radioButtonVirtualPort);

        radioButtonExistingPort = new QRadioButton(PreferencesDialog);
        radioButtonExistingPort->setObjectName(QStringLiteral("radioButtonExistingPort"));

        formLayout->setWidget(2, QFormLayout::LabelRole, radioButtonExistingPort);

        lineEditInput = new QLineEdit(PreferencesDialog);
        lineEditInput->setObjectName(QStringLiteral("lineEditInput"));

        formLayout->setWidget(1, QFormLayout::FieldRole, lineEditInput);

        comboBoxInput = new QComboBox(PreferencesDialog);
        comboBoxInput->setObjectName(QStringLiteral("comboBoxInput"));

        formLayout->setWidget(2, QFormLayout::FieldRole, comboBoxInput);

        checkBoxForce24as2398 = new QCheckBox(PreferencesDialog);
        checkBoxForce24as2398->setObjectName(QStringLiteral("checkBoxForce24as2398"));

        formLayout->setWidget(3, QFormLayout::LabelRole, checkBoxForce24as2398);


        verticalLayout->addLayout(formLayout);

        buttonBox = new QDialogButtonBox(PreferencesDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        verticalLayout_2->addLayout(verticalLayout);


        retranslateUi(PreferencesDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), PreferencesDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), PreferencesDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(PreferencesDialog);
    } // setupUi

    void retranslateUi(QDialog *PreferencesDialog)
    {
        PreferencesDialog->setWindowTitle(QApplication::translate("PreferencesDialog", "Preferences", Q_NULLPTR));
        lblDeviceOut->setText(QApplication::translate("PreferencesDialog", "Send midi through:", Q_NULLPTR));
        radioButtonVirtualPort->setText(QApplication::translate("PreferencesDialog", "Read midi from virtual port:", Q_NULLPTR));
        radioButtonExistingPort->setText(QApplication::translate("PreferencesDialog", "Read midi from existing port:", Q_NULLPTR));
        checkBoxForce24as2398->setText(QApplication::translate("PreferencesDialog", "Force 24 fps as 23.98 fps", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class PreferencesDialog: public Ui_PreferencesDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PREFERENCESDIALOG_H

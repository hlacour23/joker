/****************************************************************************
** Meta object code from reading C++ file 'MidiToolSettings.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.9)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../app/MidiTool/MidiToolSettings.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MidiToolSettings.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.9. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MidiToolSettings_t {
    QByteArrayData data[28];
    char stringdata0[505];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MidiToolSettings_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MidiToolSettings_t qt_meta_stringdata_MidiToolSettings = {
    {
QT_MOC_LITERAL(0, 0, 16), // "MidiToolSettings"
QT_MOC_LITERAL(1, 17, 15), // "writeMTCChanged"
QT_MOC_LITERAL(2, 33, 0), // ""
QT_MOC_LITERAL(3, 34, 14), // "readMTCChanged"
QT_MOC_LITERAL(4, 49, 25), // "writerTimeCodeTypeChanged"
QT_MOC_LITERAL(5, 75, 17), // "writerRateChanged"
QT_MOC_LITERAL(6, 93, 19), // "writerTimeInChanged"
QT_MOC_LITERAL(7, 113, 23), // "writerLoopLengthChanged"
QT_MOC_LITERAL(8, 137, 14), // "loopingChanged"
QT_MOC_LITERAL(9, 152, 25), // "midiOutputPortNameChanged"
QT_MOC_LITERAL(10, 178, 24), // "midiInputPortNameChanged"
QT_MOC_LITERAL(11, 203, 31), // "midiVirtualInputPortNameChanged"
QT_MOC_LITERAL(12, 235, 31), // "midiInputUseExistingPortChanged"
QT_MOC_LITERAL(13, 267, 24), // "midiForce24as2398Changed"
QT_MOC_LITERAL(14, 292, 14), // "logMaskChanged"
QT_MOC_LITERAL(15, 307, 8), // "writeMTC"
QT_MOC_LITERAL(16, 316, 7), // "readMTC"
QT_MOC_LITERAL(17, 324, 18), // "writerTimeCodeType"
QT_MOC_LITERAL(18, 343, 10), // "writerRate"
QT_MOC_LITERAL(19, 354, 12), // "writerTimeIn"
QT_MOC_LITERAL(20, 367, 16), // "writerLoopLength"
QT_MOC_LITERAL(21, 384, 7), // "looping"
QT_MOC_LITERAL(22, 392, 18), // "midiOutputPortName"
QT_MOC_LITERAL(23, 411, 17), // "midiInputPortName"
QT_MOC_LITERAL(24, 429, 24), // "midiVirtualInputPortName"
QT_MOC_LITERAL(25, 454, 24), // "midiInputUseExistingPort"
QT_MOC_LITERAL(26, 479, 17), // "midiForce24as2398"
QT_MOC_LITERAL(27, 497, 7) // "logMask"

    },
    "MidiToolSettings\0writeMTCChanged\0\0"
    "readMTCChanged\0writerTimeCodeTypeChanged\0"
    "writerRateChanged\0writerTimeInChanged\0"
    "writerLoopLengthChanged\0loopingChanged\0"
    "midiOutputPortNameChanged\0"
    "midiInputPortNameChanged\0"
    "midiVirtualInputPortNameChanged\0"
    "midiInputUseExistingPortChanged\0"
    "midiForce24as2398Changed\0logMaskChanged\0"
    "writeMTC\0readMTC\0writerTimeCodeType\0"
    "writerRate\0writerTimeIn\0writerLoopLength\0"
    "looping\0midiOutputPortName\0midiInputPortName\0"
    "midiVirtualInputPortName\0"
    "midiInputUseExistingPort\0midiForce24as2398\0"
    "logMask"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MidiToolSettings[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
      13,   92, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      13,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   79,    2, 0x06 /* Public */,
       3,    0,   80,    2, 0x06 /* Public */,
       4,    0,   81,    2, 0x06 /* Public */,
       5,    0,   82,    2, 0x06 /* Public */,
       6,    0,   83,    2, 0x06 /* Public */,
       7,    0,   84,    2, 0x06 /* Public */,
       8,    0,   85,    2, 0x06 /* Public */,
       9,    0,   86,    2, 0x06 /* Public */,
      10,    0,   87,    2, 0x06 /* Public */,
      11,    0,   88,    2, 0x06 /* Public */,
      12,    0,   89,    2, 0x06 /* Public */,
      13,    0,   90,    2, 0x06 /* Public */,
      14,    0,   91,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      15, QMetaType::Bool, 0x00495103,
      16, QMetaType::Bool, 0x00495103,
      17, QMetaType::Int, 0x00495103,
      18, QMetaType::Float, 0x00495103,
      19, QMetaType::Int, 0x00495103,
      20, QMetaType::Int, 0x00495103,
      21, QMetaType::Bool, 0x00495103,
      22, QMetaType::QString, 0x00495103,
      23, QMetaType::QString, 0x00495103,
      24, QMetaType::QString, 0x00495103,
      25, QMetaType::Bool, 0x00495103,
      26, QMetaType::Bool, 0x00495103,
      27, QMetaType::Int, 0x00495103,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       6,
       7,
       8,
       9,
      10,
      11,
      12,

       0        // eod
};

void MidiToolSettings::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MidiToolSettings *_t = static_cast<MidiToolSettings *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->writeMTCChanged(); break;
        case 1: _t->readMTCChanged(); break;
        case 2: _t->writerTimeCodeTypeChanged(); break;
        case 3: _t->writerRateChanged(); break;
        case 4: _t->writerTimeInChanged(); break;
        case 5: _t->writerLoopLengthChanged(); break;
        case 6: _t->loopingChanged(); break;
        case 7: _t->midiOutputPortNameChanged(); break;
        case 8: _t->midiInputPortNameChanged(); break;
        case 9: _t->midiVirtualInputPortNameChanged(); break;
        case 10: _t->midiInputUseExistingPortChanged(); break;
        case 11: _t->midiForce24as2398Changed(); break;
        case 12: _t->logMaskChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (MidiToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MidiToolSettings::writeMTCChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (MidiToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MidiToolSettings::readMTCChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (MidiToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MidiToolSettings::writerTimeCodeTypeChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (MidiToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MidiToolSettings::writerRateChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (MidiToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MidiToolSettings::writerTimeInChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (MidiToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MidiToolSettings::writerLoopLengthChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (MidiToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MidiToolSettings::loopingChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (MidiToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MidiToolSettings::midiOutputPortNameChanged)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (MidiToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MidiToolSettings::midiInputPortNameChanged)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (MidiToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MidiToolSettings::midiVirtualInputPortNameChanged)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (MidiToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MidiToolSettings::midiInputUseExistingPortChanged)) {
                *result = 10;
                return;
            }
        }
        {
            typedef void (MidiToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MidiToolSettings::midiForce24as2398Changed)) {
                *result = 11;
                return;
            }
        }
        {
            typedef void (MidiToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MidiToolSettings::logMaskChanged)) {
                *result = 12;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        MidiToolSettings *_t = static_cast<MidiToolSettings *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->writeMTC(); break;
        case 1: *reinterpret_cast< bool*>(_v) = _t->readMTC(); break;
        case 2: *reinterpret_cast< int*>(_v) = _t->writerTimeCodeType(); break;
        case 3: *reinterpret_cast< float*>(_v) = _t->writerRate(); break;
        case 4: *reinterpret_cast< int*>(_v) = _t->writerTimeIn(); break;
        case 5: *reinterpret_cast< int*>(_v) = _t->writerLoopLength(); break;
        case 6: *reinterpret_cast< bool*>(_v) = _t->looping(); break;
        case 7: *reinterpret_cast< QString*>(_v) = _t->midiOutputPortName(); break;
        case 8: *reinterpret_cast< QString*>(_v) = _t->midiInputPortName(); break;
        case 9: *reinterpret_cast< QString*>(_v) = _t->midiVirtualInputPortName(); break;
        case 10: *reinterpret_cast< bool*>(_v) = _t->midiInputUseExistingPort(); break;
        case 11: *reinterpret_cast< bool*>(_v) = _t->midiForce24as2398(); break;
        case 12: *reinterpret_cast< int*>(_v) = _t->logMask(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        MidiToolSettings *_t = static_cast<MidiToolSettings *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setWriteMTC(*reinterpret_cast< bool*>(_v)); break;
        case 1: _t->setReadMTC(*reinterpret_cast< bool*>(_v)); break;
        case 2: _t->setWriterTimeCodeType(*reinterpret_cast< int*>(_v)); break;
        case 3: _t->setWriterRate(*reinterpret_cast< float*>(_v)); break;
        case 4: _t->setWriterTimeIn(*reinterpret_cast< int*>(_v)); break;
        case 5: _t->setWriterLoopLength(*reinterpret_cast< int*>(_v)); break;
        case 6: _t->setLooping(*reinterpret_cast< bool*>(_v)); break;
        case 7: _t->setMidiOutputPortName(*reinterpret_cast< QString*>(_v)); break;
        case 8: _t->setMidiInputPortName(*reinterpret_cast< QString*>(_v)); break;
        case 9: _t->setMidiVirtualInputPortName(*reinterpret_cast< QString*>(_v)); break;
        case 10: _t->setMidiInputUseExistingPort(*reinterpret_cast< bool*>(_v)); break;
        case 11: _t->setMidiForce24as2398(*reinterpret_cast< bool*>(_v)); break;
        case 12: _t->setLogMask(*reinterpret_cast< int*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_a);
}

const QMetaObject MidiToolSettings::staticMetaObject = {
    { &PhGenericSettings::staticMetaObject, qt_meta_stringdata_MidiToolSettings.data,
      qt_meta_data_MidiToolSettings,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MidiToolSettings::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MidiToolSettings::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MidiToolSettings.stringdata0))
        return static_cast<void*>(this);
    return PhGenericSettings::qt_metacast(_clname);
}

int MidiToolSettings::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PhGenericSettings::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 13;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 13;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 13;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 13;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 13;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 13;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void MidiToolSettings::writeMTCChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void MidiToolSettings::readMTCChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void MidiToolSettings::writerTimeCodeTypeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void MidiToolSettings::writerRateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void MidiToolSettings::writerTimeInChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void MidiToolSettings::writerLoopLengthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void MidiToolSettings::loopingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void MidiToolSettings::midiOutputPortNameChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void MidiToolSettings::midiInputPortNameChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void MidiToolSettings::midiVirtualInputPortNameChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, nullptr);
}

// SIGNAL 10
void MidiToolSettings::midiInputUseExistingPortChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, nullptr);
}

// SIGNAL 11
void MidiToolSettings::midiForce24as2398Changed()
{
    QMetaObject::activate(this, &staticMetaObject, 11, nullptr);
}

// SIGNAL 12
void MidiToolSettings::logMaskChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 12, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE

/********************************************************************************
** Form generated from reading UI file 'PropertyDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PROPERTYDIALOG_H
#define UI_PROPERTYDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_PropertyDialog
{
public:
    QVBoxLayout *verticalLayout;
    QFormLayout *formLayout;
    QLabel *label;
    QLabel *titleLabel;
    QLabel *label_2;
    QLabel *authorLabel;
    QLabel *label_4;
    QLabel *tcInLabel;
    QLabel *label_5;
    QLabel *tcOutLabel;
    QLabel *label_8;
    QLabel *peopleNumberLabel;
    QLabel *label_10;
    QLabel *charNumberLabel;
    QFrame *line_2;
    QLabel *label_7;
    QLabel *label_9;
    QLabel *label_3;
    QLabel *videoFileLabel;
    QLabel *label_6;
    QLabel *videoTCInLabel;
    QLabel *label_11;
    QLabel *resolutionLabel;
    QLabel *label_13;
    QLabel *codecNameLabel;
    QLabel *label_15;
    QLabel *fpsLabel;
    QLabel *label_12;
    QLabel *videoTCOutLabel;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *PropertyDialog)
    {
        if (PropertyDialog->objectName().isEmpty())
            PropertyDialog->setObjectName(QStringLiteral("PropertyDialog"));
        PropertyDialog->resize(301, 409);
        verticalLayout = new QVBoxLayout(PropertyDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        formLayout = new QFormLayout();
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::FieldsStayAtSizeHint);
        label = new QLabel(PropertyDialog);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label);

        titleLabel = new QLabel(PropertyDialog);
        titleLabel->setObjectName(QStringLiteral("titleLabel"));
        QFont font;
        font.setItalic(true);
        titleLabel->setFont(font);

        formLayout->setWidget(1, QFormLayout::FieldRole, titleLabel);

        label_2 = new QLabel(PropertyDialog);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_2);

        authorLabel = new QLabel(PropertyDialog);
        authorLabel->setObjectName(QStringLiteral("authorLabel"));
        authorLabel->setFont(font);

        formLayout->setWidget(2, QFormLayout::FieldRole, authorLabel);

        label_4 = new QLabel(PropertyDialog);
        label_4->setObjectName(QStringLiteral("label_4"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_4);

        tcInLabel = new QLabel(PropertyDialog);
        tcInLabel->setObjectName(QStringLiteral("tcInLabel"));
        tcInLabel->setFont(font);

        formLayout->setWidget(3, QFormLayout::FieldRole, tcInLabel);

        label_5 = new QLabel(PropertyDialog);
        label_5->setObjectName(QStringLiteral("label_5"));

        formLayout->setWidget(4, QFormLayout::LabelRole, label_5);

        tcOutLabel = new QLabel(PropertyDialog);
        tcOutLabel->setObjectName(QStringLiteral("tcOutLabel"));
        tcOutLabel->setFont(font);

        formLayout->setWidget(4, QFormLayout::FieldRole, tcOutLabel);

        label_8 = new QLabel(PropertyDialog);
        label_8->setObjectName(QStringLiteral("label_8"));

        formLayout->setWidget(5, QFormLayout::LabelRole, label_8);

        peopleNumberLabel = new QLabel(PropertyDialog);
        peopleNumberLabel->setObjectName(QStringLiteral("peopleNumberLabel"));
        peopleNumberLabel->setFont(font);

        formLayout->setWidget(5, QFormLayout::FieldRole, peopleNumberLabel);

        label_10 = new QLabel(PropertyDialog);
        label_10->setObjectName(QStringLiteral("label_10"));

        formLayout->setWidget(6, QFormLayout::LabelRole, label_10);

        charNumberLabel = new QLabel(PropertyDialog);
        charNumberLabel->setObjectName(QStringLiteral("charNumberLabel"));
        charNumberLabel->setFont(font);

        formLayout->setWidget(6, QFormLayout::FieldRole, charNumberLabel);

        line_2 = new QFrame(PropertyDialog);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        formLayout->setWidget(14, QFormLayout::LabelRole, line_2);

        label_7 = new QLabel(PropertyDialog);
        label_7->setObjectName(QStringLiteral("label_7"));
        QFont font1;
        font1.setBold(true);
        font1.setWeight(75);
        label_7->setFont(font1);

        formLayout->setWidget(0, QFormLayout::LabelRole, label_7);

        label_9 = new QLabel(PropertyDialog);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setFont(font1);

        formLayout->setWidget(7, QFormLayout::LabelRole, label_9);

        label_3 = new QLabel(PropertyDialog);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout->setWidget(8, QFormLayout::LabelRole, label_3);

        videoFileLabel = new QLabel(PropertyDialog);
        videoFileLabel->setObjectName(QStringLiteral("videoFileLabel"));
        videoFileLabel->setFont(font);

        formLayout->setWidget(8, QFormLayout::FieldRole, videoFileLabel);

        label_6 = new QLabel(PropertyDialog);
        label_6->setObjectName(QStringLiteral("label_6"));

        formLayout->setWidget(9, QFormLayout::LabelRole, label_6);

        videoTCInLabel = new QLabel(PropertyDialog);
        videoTCInLabel->setObjectName(QStringLiteral("videoTCInLabel"));
        videoTCInLabel->setFont(font);

        formLayout->setWidget(9, QFormLayout::FieldRole, videoTCInLabel);

        label_11 = new QLabel(PropertyDialog);
        label_11->setObjectName(QStringLiteral("label_11"));

        formLayout->setWidget(11, QFormLayout::LabelRole, label_11);

        resolutionLabel = new QLabel(PropertyDialog);
        resolutionLabel->setObjectName(QStringLiteral("resolutionLabel"));
        resolutionLabel->setFont(font);

        formLayout->setWidget(11, QFormLayout::FieldRole, resolutionLabel);

        label_13 = new QLabel(PropertyDialog);
        label_13->setObjectName(QStringLiteral("label_13"));

        formLayout->setWidget(13, QFormLayout::LabelRole, label_13);

        codecNameLabel = new QLabel(PropertyDialog);
        codecNameLabel->setObjectName(QStringLiteral("codecNameLabel"));
        codecNameLabel->setFont(font);

        formLayout->setWidget(13, QFormLayout::FieldRole, codecNameLabel);

        label_15 = new QLabel(PropertyDialog);
        label_15->setObjectName(QStringLiteral("label_15"));

        formLayout->setWidget(12, QFormLayout::LabelRole, label_15);

        fpsLabel = new QLabel(PropertyDialog);
        fpsLabel->setObjectName(QStringLiteral("fpsLabel"));
        fpsLabel->setFont(font);

        formLayout->setWidget(12, QFormLayout::FieldRole, fpsLabel);

        label_12 = new QLabel(PropertyDialog);
        label_12->setObjectName(QStringLiteral("label_12"));

        formLayout->setWidget(10, QFormLayout::LabelRole, label_12);

        videoTCOutLabel = new QLabel(PropertyDialog);
        videoTCOutLabel->setObjectName(QStringLiteral("videoTCOutLabel"));
        videoTCOutLabel->setFont(font);

        formLayout->setWidget(10, QFormLayout::FieldRole, videoTCOutLabel);


        verticalLayout->addLayout(formLayout);

        buttonBox = new QDialogButtonBox(PropertyDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Close);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(PropertyDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), PropertyDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), PropertyDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(PropertyDialog);
    } // setupUi

    void retranslateUi(QDialog *PropertyDialog)
    {
        PropertyDialog->setWindowTitle(QApplication::translate("PropertyDialog", "Properties", Q_NULLPTR));
        label->setText(QApplication::translate("PropertyDialog", "Title:", Q_NULLPTR));
        titleLabel->setText(QApplication::translate("PropertyDialog", "-", Q_NULLPTR));
        label_2->setText(QApplication::translate("PropertyDialog", "Author:", Q_NULLPTR));
        authorLabel->setText(QApplication::translate("PropertyDialog", "-", Q_NULLPTR));
        label_4->setText(QApplication::translate("PropertyDialog", "TC In:", Q_NULLPTR));
        tcInLabel->setText(QApplication::translate("PropertyDialog", "-", Q_NULLPTR));
        label_5->setText(QApplication::translate("PropertyDialog", "TC Out:", Q_NULLPTR));
        tcOutLabel->setText(QApplication::translate("PropertyDialog", "-", Q_NULLPTR));
        label_8->setText(QApplication::translate("PropertyDialog", "People number:", Q_NULLPTR));
        peopleNumberLabel->setText(QApplication::translate("PropertyDialog", "-", Q_NULLPTR));
        label_10->setText(QApplication::translate("PropertyDialog", "Text character number:", Q_NULLPTR));
        charNumberLabel->setText(QApplication::translate("PropertyDialog", "-", Q_NULLPTR));
        label_7->setText(QApplication::translate("PropertyDialog", "Strip:", Q_NULLPTR));
        label_9->setText(QApplication::translate("PropertyDialog", "Video:", Q_NULLPTR));
        label_3->setText(QApplication::translate("PropertyDialog", "Video file:", Q_NULLPTR));
        videoFileLabel->setText(QApplication::translate("PropertyDialog", "-", Q_NULLPTR));
        label_6->setText(QApplication::translate("PropertyDialog", "TC In:", Q_NULLPTR));
        videoTCInLabel->setText(QApplication::translate("PropertyDialog", "-", Q_NULLPTR));
        label_11->setText(QApplication::translate("PropertyDialog", "Resolution:", Q_NULLPTR));
        resolutionLabel->setText(QApplication::translate("PropertyDialog", "-", Q_NULLPTR));
        label_13->setText(QApplication::translate("PropertyDialog", "Codec name:", Q_NULLPTR));
        codecNameLabel->setText(QApplication::translate("PropertyDialog", "-", Q_NULLPTR));
        label_15->setText(QApplication::translate("PropertyDialog", "FPS:", Q_NULLPTR));
        fpsLabel->setText(QApplication::translate("PropertyDialog", "-", Q_NULLPTR));
        label_12->setText(QApplication::translate("PropertyDialog", "TC Out:", Q_NULLPTR));
        videoTCOutLabel->setText(QApplication::translate("PropertyDialog", "-", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class PropertyDialog: public Ui_PropertyDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PROPERTYDIALOG_H

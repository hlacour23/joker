/****************************************************************************
** Meta object code from reading C++ file 'JokerSettings.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.9)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../app/Joker/JokerSettings.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'JokerSettings.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.9. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_JokerSettings_t {
    QByteArrayData data[111];
    char stringdata0[2163];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_JokerSettings_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_JokerSettings_t qt_meta_stringdata_JokerSettings = {
    {
QT_MOC_LITERAL(0, 0, 13), // "JokerSettings"
QT_MOC_LITERAL(1, 14, 17), // "fullScreenChanged"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 20), // "exitedNormalyChanged"
QT_MOC_LITERAL(4, 54, 21), // "windowGeometryChanged"
QT_MOC_LITERAL(5, 76, 22), // "currentDocumentChanged"
QT_MOC_LITERAL(6, 99, 25), // "lastDocumentFolderChanged"
QT_MOC_LITERAL(7, 125, 25), // "recentDocumentListChanged"
QT_MOC_LITERAL(8, 151, 24), // "maxRecentDocumentChanged"
QT_MOC_LITERAL(9, 176, 17), // "autoReloadChanged"
QT_MOC_LITERAL(10, 194, 15), // "bookmarkChanged"
QT_MOC_LITERAL(11, 210, 18), // "screenDelayChanged"
QT_MOC_LITERAL(12, 229, 18), // "syncLoopingChanged"
QT_MOC_LITERAL(13, 248, 18), // "displayInfoChanged"
QT_MOC_LITERAL(14, 267, 16), // "resetInfoChanged"
QT_MOC_LITERAL(15, 284, 18), // "stripHeightChanged"
QT_MOC_LITERAL(16, 303, 29), // "horizontalTimePerPixelChanged"
QT_MOC_LITERAL(17, 333, 27), // "verticalTimePerPixelChanged"
QT_MOC_LITERAL(18, 361, 27), // "backgroundImageLightChanged"
QT_MOC_LITERAL(19, 389, 26), // "backgroundImageDarkChanged"
QT_MOC_LITERAL(20, 416, 20), // "hudFontFamilyChanged"
QT_MOC_LITERAL(21, 437, 21), // "textFontFamilyChanged"
QT_MOC_LITERAL(22, 459, 20), // "stripTestModeChanged"
QT_MOC_LITERAL(23, 480, 22), // "displayNextTextChanged"
QT_MOC_LITERAL(24, 503, 26), // "hideSelectedPeoplesChanged"
QT_MOC_LITERAL(25, 530, 29), // "selectedPeopleNameListChanged"
QT_MOC_LITERAL(26, 560, 18), // "invertColorChanged"
QT_MOC_LITERAL(27, 579, 18), // "displayCutsChanged"
QT_MOC_LITERAL(28, 598, 18), // "displayFeetChanged"
QT_MOC_LITERAL(29, 617, 20), // "firstFootTimeChanged"
QT_MOC_LITERAL(30, 638, 25), // "timeBetweenTwoFeetChanged"
QT_MOC_LITERAL(31, 664, 27), // "displayVerticalScaleChanged"
QT_MOC_LITERAL(32, 692, 34), // "verticalScaleSpaceInSecondsCh..."
QT_MOC_LITERAL(33, 727, 15), // "cutWidthChanged"
QT_MOC_LITERAL(34, 743, 24), // "displayBackgroundChanged"
QT_MOC_LITERAL(35, 768, 27), // "backgroundColorLightChanged"
QT_MOC_LITERAL(36, 796, 26), // "backgroundColorDarkChanged"
QT_MOC_LITERAL(37, 823, 22), // "synchroProtocolChanged"
QT_MOC_LITERAL(38, 846, 19), // "ltcInputPortChanged"
QT_MOC_LITERAL(39, 866, 32), // "ltcAutoDetectTimeCodeTypeChanged"
QT_MOC_LITERAL(40, 899, 28), // "ltcReaderTimeCodeTypeChanged"
QT_MOC_LITERAL(41, 928, 19), // "mtcInputPortChanged"
QT_MOC_LITERAL(42, 948, 26), // "mtcVirtualInputPortChanged"
QT_MOC_LITERAL(43, 975, 30), // "mtcInputUseExistingPortChanged"
QT_MOC_LITERAL(44, 1006, 23), // "mtcForce24as2398Changed"
QT_MOC_LITERAL(45, 1030, 21), // "sendMmcMessageChanged"
QT_MOC_LITERAL(46, 1052, 20), // "mmcOutputPortChanged"
QT_MOC_LITERAL(47, 1073, 27), // "peopleDialogGeometryChanged"
QT_MOC_LITERAL(48, 1101, 22), // "lastVideoFolderChanged"
QT_MOC_LITERAL(49, 1124, 20), // "stripFileTypeChanged"
QT_MOC_LITERAL(50, 1145, 20), // "videoFileTypeChanged"
QT_MOC_LITERAL(51, 1166, 26), // "displayControlPanelChanged"
QT_MOC_LITERAL(52, 1193, 14), // "logMaskChanged"
QT_MOC_LITERAL(53, 1208, 18), // "displayLogoChanged"
QT_MOC_LITERAL(54, 1227, 15), // "languageChanged"
QT_MOC_LITERAL(55, 1243, 16), // "hideStripChanged"
QT_MOC_LITERAL(56, 1260, 25), // "lastPreferencesTabChanged"
QT_MOC_LITERAL(57, 1286, 10), // "fullScreen"
QT_MOC_LITERAL(58, 1297, 13), // "exitedNormaly"
QT_MOC_LITERAL(59, 1311, 14), // "windowGeometry"
QT_MOC_LITERAL(60, 1326, 15), // "currentDocument"
QT_MOC_LITERAL(61, 1342, 18), // "lastDocumentFolder"
QT_MOC_LITERAL(62, 1361, 18), // "recentDocumentList"
QT_MOC_LITERAL(63, 1380, 17), // "maxRecentDocument"
QT_MOC_LITERAL(64, 1398, 10), // "autoReload"
QT_MOC_LITERAL(65, 1409, 11), // "screenDelay"
QT_MOC_LITERAL(66, 1421, 11), // "syncLooping"
QT_MOC_LITERAL(67, 1433, 11), // "displayInfo"
QT_MOC_LITERAL(68, 1445, 9), // "resetInfo"
QT_MOC_LITERAL(69, 1455, 11), // "stripHeight"
QT_MOC_LITERAL(70, 1467, 22), // "horizontalTimePerPixel"
QT_MOC_LITERAL(71, 1490, 20), // "verticalTimePerPixel"
QT_MOC_LITERAL(72, 1511, 20), // "backgroundImageLight"
QT_MOC_LITERAL(73, 1532, 19), // "backgroundImageDark"
QT_MOC_LITERAL(74, 1552, 13), // "hudFontFamily"
QT_MOC_LITERAL(75, 1566, 14), // "textFontFamily"
QT_MOC_LITERAL(76, 1581, 13), // "stripTestMode"
QT_MOC_LITERAL(77, 1595, 15), // "displayNextText"
QT_MOC_LITERAL(78, 1611, 19), // "hideSelectedPeoples"
QT_MOC_LITERAL(79, 1631, 22), // "selectedPeopleNameList"
QT_MOC_LITERAL(80, 1654, 11), // "invertColor"
QT_MOC_LITERAL(81, 1666, 11), // "displayCuts"
QT_MOC_LITERAL(82, 1678, 11), // "displayFeet"
QT_MOC_LITERAL(83, 1690, 13), // "firstFootTime"
QT_MOC_LITERAL(84, 1704, 18), // "timeBetweenTwoFeet"
QT_MOC_LITERAL(85, 1723, 20), // "displayVerticalScale"
QT_MOC_LITERAL(86, 1744, 27), // "verticalScaleSpaceInSeconds"
QT_MOC_LITERAL(87, 1772, 8), // "cutWidth"
QT_MOC_LITERAL(88, 1781, 17), // "displayBackground"
QT_MOC_LITERAL(89, 1799, 20), // "backgroundColorLight"
QT_MOC_LITERAL(90, 1820, 19), // "backgroundColorDark"
QT_MOC_LITERAL(91, 1840, 15), // "synchroProtocol"
QT_MOC_LITERAL(92, 1856, 12), // "ltcInputPort"
QT_MOC_LITERAL(93, 1869, 25), // "ltcAutoDetectTimeCodeType"
QT_MOC_LITERAL(94, 1895, 21), // "ltcReaderTimeCodeType"
QT_MOC_LITERAL(95, 1917, 12), // "mtcInputPort"
QT_MOC_LITERAL(96, 1930, 19), // "mtcVirtualInputPort"
QT_MOC_LITERAL(97, 1950, 23), // "mtcInputUseExistingPort"
QT_MOC_LITERAL(98, 1974, 16), // "mtcForce24as2398"
QT_MOC_LITERAL(99, 1991, 14), // "sendMmcMessage"
QT_MOC_LITERAL(100, 2006, 13), // "mmcOutputPort"
QT_MOC_LITERAL(101, 2020, 20), // "peopleDialogGeometry"
QT_MOC_LITERAL(102, 2041, 15), // "lastVideoFolder"
QT_MOC_LITERAL(103, 2057, 13), // "stripFileType"
QT_MOC_LITERAL(104, 2071, 13), // "videoFileType"
QT_MOC_LITERAL(105, 2085, 19), // "displayControlPanel"
QT_MOC_LITERAL(106, 2105, 7), // "logMask"
QT_MOC_LITERAL(107, 2113, 11), // "displayLogo"
QT_MOC_LITERAL(108, 2125, 8), // "language"
QT_MOC_LITERAL(109, 2134, 9), // "hideStrip"
QT_MOC_LITERAL(110, 2144, 18) // "lastPreferencesTab"

    },
    "JokerSettings\0fullScreenChanged\0\0"
    "exitedNormalyChanged\0windowGeometryChanged\0"
    "currentDocumentChanged\0lastDocumentFolderChanged\0"
    "recentDocumentListChanged\0"
    "maxRecentDocumentChanged\0autoReloadChanged\0"
    "bookmarkChanged\0screenDelayChanged\0"
    "syncLoopingChanged\0displayInfoChanged\0"
    "resetInfoChanged\0stripHeightChanged\0"
    "horizontalTimePerPixelChanged\0"
    "verticalTimePerPixelChanged\0"
    "backgroundImageLightChanged\0"
    "backgroundImageDarkChanged\0"
    "hudFontFamilyChanged\0textFontFamilyChanged\0"
    "stripTestModeChanged\0displayNextTextChanged\0"
    "hideSelectedPeoplesChanged\0"
    "selectedPeopleNameListChanged\0"
    "invertColorChanged\0displayCutsChanged\0"
    "displayFeetChanged\0firstFootTimeChanged\0"
    "timeBetweenTwoFeetChanged\0"
    "displayVerticalScaleChanged\0"
    "verticalScaleSpaceInSecondsChanged\0"
    "cutWidthChanged\0displayBackgroundChanged\0"
    "backgroundColorLightChanged\0"
    "backgroundColorDarkChanged\0"
    "synchroProtocolChanged\0ltcInputPortChanged\0"
    "ltcAutoDetectTimeCodeTypeChanged\0"
    "ltcReaderTimeCodeTypeChanged\0"
    "mtcInputPortChanged\0mtcVirtualInputPortChanged\0"
    "mtcInputUseExistingPortChanged\0"
    "mtcForce24as2398Changed\0sendMmcMessageChanged\0"
    "mmcOutputPortChanged\0peopleDialogGeometryChanged\0"
    "lastVideoFolderChanged\0stripFileTypeChanged\0"
    "videoFileTypeChanged\0displayControlPanelChanged\0"
    "logMaskChanged\0displayLogoChanged\0"
    "languageChanged\0hideStripChanged\0"
    "lastPreferencesTabChanged\0fullScreen\0"
    "exitedNormaly\0windowGeometry\0"
    "currentDocument\0lastDocumentFolder\0"
    "recentDocumentList\0maxRecentDocument\0"
    "autoReload\0screenDelay\0syncLooping\0"
    "displayInfo\0resetInfo\0stripHeight\0"
    "horizontalTimePerPixel\0verticalTimePerPixel\0"
    "backgroundImageLight\0backgroundImageDark\0"
    "hudFontFamily\0textFontFamily\0stripTestMode\0"
    "displayNextText\0hideSelectedPeoples\0"
    "selectedPeopleNameList\0invertColor\0"
    "displayCuts\0displayFeet\0firstFootTime\0"
    "timeBetweenTwoFeet\0displayVerticalScale\0"
    "verticalScaleSpaceInSeconds\0cutWidth\0"
    "displayBackground\0backgroundColorLight\0"
    "backgroundColorDark\0synchroProtocol\0"
    "ltcInputPort\0ltcAutoDetectTimeCodeType\0"
    "ltcReaderTimeCodeType\0mtcInputPort\0"
    "mtcVirtualInputPort\0mtcInputUseExistingPort\0"
    "mtcForce24as2398\0sendMmcMessage\0"
    "mmcOutputPort\0peopleDialogGeometry\0"
    "lastVideoFolder\0stripFileType\0"
    "videoFileType\0displayControlPanel\0"
    "logMask\0displayLogo\0language\0hideStrip\0"
    "lastPreferencesTab"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_JokerSettings[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      55,   14, // methods
      54,  344, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      55,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  289,    2, 0x06 /* Public */,
       3,    0,  290,    2, 0x06 /* Public */,
       4,    0,  291,    2, 0x06 /* Public */,
       5,    0,  292,    2, 0x06 /* Public */,
       6,    0,  293,    2, 0x06 /* Public */,
       7,    0,  294,    2, 0x06 /* Public */,
       8,    0,  295,    2, 0x06 /* Public */,
       9,    0,  296,    2, 0x06 /* Public */,
      10,    0,  297,    2, 0x06 /* Public */,
      11,    0,  298,    2, 0x06 /* Public */,
      12,    0,  299,    2, 0x06 /* Public */,
      13,    0,  300,    2, 0x06 /* Public */,
      14,    0,  301,    2, 0x06 /* Public */,
      15,    0,  302,    2, 0x06 /* Public */,
      16,    0,  303,    2, 0x06 /* Public */,
      17,    0,  304,    2, 0x06 /* Public */,
      18,    0,  305,    2, 0x06 /* Public */,
      19,    0,  306,    2, 0x06 /* Public */,
      20,    0,  307,    2, 0x06 /* Public */,
      21,    0,  308,    2, 0x06 /* Public */,
      22,    0,  309,    2, 0x06 /* Public */,
      23,    0,  310,    2, 0x06 /* Public */,
      24,    0,  311,    2, 0x06 /* Public */,
      25,    0,  312,    2, 0x06 /* Public */,
      26,    0,  313,    2, 0x06 /* Public */,
      27,    0,  314,    2, 0x06 /* Public */,
      28,    0,  315,    2, 0x06 /* Public */,
      29,    0,  316,    2, 0x06 /* Public */,
      30,    0,  317,    2, 0x06 /* Public */,
      31,    0,  318,    2, 0x06 /* Public */,
      32,    0,  319,    2, 0x06 /* Public */,
      33,    0,  320,    2, 0x06 /* Public */,
      34,    0,  321,    2, 0x06 /* Public */,
      35,    0,  322,    2, 0x06 /* Public */,
      36,    0,  323,    2, 0x06 /* Public */,
      37,    0,  324,    2, 0x06 /* Public */,
      38,    0,  325,    2, 0x06 /* Public */,
      39,    0,  326,    2, 0x06 /* Public */,
      40,    0,  327,    2, 0x06 /* Public */,
      41,    0,  328,    2, 0x06 /* Public */,
      42,    0,  329,    2, 0x06 /* Public */,
      43,    0,  330,    2, 0x06 /* Public */,
      44,    0,  331,    2, 0x06 /* Public */,
      45,    0,  332,    2, 0x06 /* Public */,
      46,    0,  333,    2, 0x06 /* Public */,
      47,    0,  334,    2, 0x06 /* Public */,
      48,    0,  335,    2, 0x06 /* Public */,
      49,    0,  336,    2, 0x06 /* Public */,
      50,    0,  337,    2, 0x06 /* Public */,
      51,    0,  338,    2, 0x06 /* Public */,
      52,    0,  339,    2, 0x06 /* Public */,
      53,    0,  340,    2, 0x06 /* Public */,
      54,    0,  341,    2, 0x06 /* Public */,
      55,    0,  342,    2, 0x06 /* Public */,
      56,    0,  343,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      57, QMetaType::Bool, 0x00495103,
      58, QMetaType::Bool, 0x00495103,
      59, QMetaType::QByteArray, 0x00495103,
      60, QMetaType::QString, 0x00495103,
      61, QMetaType::QString, 0x00495103,
      62, QMetaType::QStringList, 0x00495103,
      63, QMetaType::Int, 0x00495103,
      64, QMetaType::Bool, 0x00495103,
      65, QMetaType::Int, 0x00495103,
      66, QMetaType::Bool, 0x00495103,
      67, QMetaType::Bool, 0x00495103,
      68, QMetaType::Bool, 0x00495103,
      69, QMetaType::Float, 0x00495103,
      70, QMetaType::Int, 0x00495103,
      71, QMetaType::Int, 0x00495103,
      72, QMetaType::QString, 0x00495103,
      73, QMetaType::QString, 0x00495103,
      74, QMetaType::QString, 0x00495103,
      75, QMetaType::QString, 0x00495103,
      76, QMetaType::Bool, 0x00495103,
      77, QMetaType::Bool, 0x00495103,
      78, QMetaType::Bool, 0x00495103,
      79, QMetaType::QStringList, 0x00495103,
      80, QMetaType::Bool, 0x00495103,
      81, QMetaType::Bool, 0x00495103,
      82, QMetaType::Bool, 0x00495103,
      83, QMetaType::Int, 0x00495103,
      84, QMetaType::Int, 0x00495103,
      85, QMetaType::Bool, 0x00495103,
      86, QMetaType::Int, 0x00495103,
      87, QMetaType::Int, 0x00495103,
      88, QMetaType::Bool, 0x00495103,
      89, QMetaType::Int, 0x00495103,
      90, QMetaType::Int, 0x00495103,
      91, QMetaType::Int, 0x00495103,
      92, QMetaType::QString, 0x00495103,
      93, QMetaType::Bool, 0x00495103,
      94, QMetaType::Int, 0x00495103,
      95, QMetaType::QString, 0x00495103,
      96, QMetaType::QString, 0x00495103,
      97, QMetaType::Bool, 0x00495103,
      98, QMetaType::Bool, 0x00495103,
      99, QMetaType::Bool, 0x00495103,
     100, QMetaType::QString, 0x00495103,
     101, QMetaType::QByteArray, 0x00495103,
     102, QMetaType::QString, 0x00495103,
     103, QMetaType::QStringList, 0x00495103,
     104, QMetaType::QStringList, 0x00495103,
     105, QMetaType::Bool, 0x00495103,
     106, QMetaType::Int, 0x00495103,
     107, QMetaType::Bool, 0x00495103,
     108, QMetaType::QString, 0x00495103,
     109, QMetaType::Bool, 0x00495103,
     110, QMetaType::Int, 0x00495103,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       6,
       7,
       9,
      10,
      11,
      12,
      13,
      14,
      15,
      16,
      17,
      18,
      19,
      20,
      21,
      22,
      23,
      24,
      25,
      26,
      27,
      28,
      29,
      30,
      31,
      32,
      33,
      34,
      35,
      36,
      37,
      38,
      39,
      40,
      41,
      42,
      43,
      44,
      45,
      46,
      47,
      48,
      49,
      50,
      51,
      52,
      53,
      54,

       0        // eod
};

void JokerSettings::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        JokerSettings *_t = static_cast<JokerSettings *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->fullScreenChanged(); break;
        case 1: _t->exitedNormalyChanged(); break;
        case 2: _t->windowGeometryChanged(); break;
        case 3: _t->currentDocumentChanged(); break;
        case 4: _t->lastDocumentFolderChanged(); break;
        case 5: _t->recentDocumentListChanged(); break;
        case 6: _t->maxRecentDocumentChanged(); break;
        case 7: _t->autoReloadChanged(); break;
        case 8: _t->bookmarkChanged(); break;
        case 9: _t->screenDelayChanged(); break;
        case 10: _t->syncLoopingChanged(); break;
        case 11: _t->displayInfoChanged(); break;
        case 12: _t->resetInfoChanged(); break;
        case 13: _t->stripHeightChanged(); break;
        case 14: _t->horizontalTimePerPixelChanged(); break;
        case 15: _t->verticalTimePerPixelChanged(); break;
        case 16: _t->backgroundImageLightChanged(); break;
        case 17: _t->backgroundImageDarkChanged(); break;
        case 18: _t->hudFontFamilyChanged(); break;
        case 19: _t->textFontFamilyChanged(); break;
        case 20: _t->stripTestModeChanged(); break;
        case 21: _t->displayNextTextChanged(); break;
        case 22: _t->hideSelectedPeoplesChanged(); break;
        case 23: _t->selectedPeopleNameListChanged(); break;
        case 24: _t->invertColorChanged(); break;
        case 25: _t->displayCutsChanged(); break;
        case 26: _t->displayFeetChanged(); break;
        case 27: _t->firstFootTimeChanged(); break;
        case 28: _t->timeBetweenTwoFeetChanged(); break;
        case 29: _t->displayVerticalScaleChanged(); break;
        case 30: _t->verticalScaleSpaceInSecondsChanged(); break;
        case 31: _t->cutWidthChanged(); break;
        case 32: _t->displayBackgroundChanged(); break;
        case 33: _t->backgroundColorLightChanged(); break;
        case 34: _t->backgroundColorDarkChanged(); break;
        case 35: _t->synchroProtocolChanged(); break;
        case 36: _t->ltcInputPortChanged(); break;
        case 37: _t->ltcAutoDetectTimeCodeTypeChanged(); break;
        case 38: _t->ltcReaderTimeCodeTypeChanged(); break;
        case 39: _t->mtcInputPortChanged(); break;
        case 40: _t->mtcVirtualInputPortChanged(); break;
        case 41: _t->mtcInputUseExistingPortChanged(); break;
        case 42: _t->mtcForce24as2398Changed(); break;
        case 43: _t->sendMmcMessageChanged(); break;
        case 44: _t->mmcOutputPortChanged(); break;
        case 45: _t->peopleDialogGeometryChanged(); break;
        case 46: _t->lastVideoFolderChanged(); break;
        case 47: _t->stripFileTypeChanged(); break;
        case 48: _t->videoFileTypeChanged(); break;
        case 49: _t->displayControlPanelChanged(); break;
        case 50: _t->logMaskChanged(); break;
        case 51: _t->displayLogoChanged(); break;
        case 52: _t->languageChanged(); break;
        case 53: _t->hideStripChanged(); break;
        case 54: _t->lastPreferencesTabChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::fullScreenChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::exitedNormalyChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::windowGeometryChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::currentDocumentChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::lastDocumentFolderChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::recentDocumentListChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::maxRecentDocumentChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::autoReloadChanged)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::bookmarkChanged)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::screenDelayChanged)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::syncLoopingChanged)) {
                *result = 10;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::displayInfoChanged)) {
                *result = 11;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::resetInfoChanged)) {
                *result = 12;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::stripHeightChanged)) {
                *result = 13;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::horizontalTimePerPixelChanged)) {
                *result = 14;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::verticalTimePerPixelChanged)) {
                *result = 15;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::backgroundImageLightChanged)) {
                *result = 16;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::backgroundImageDarkChanged)) {
                *result = 17;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::hudFontFamilyChanged)) {
                *result = 18;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::textFontFamilyChanged)) {
                *result = 19;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::stripTestModeChanged)) {
                *result = 20;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::displayNextTextChanged)) {
                *result = 21;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::hideSelectedPeoplesChanged)) {
                *result = 22;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::selectedPeopleNameListChanged)) {
                *result = 23;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::invertColorChanged)) {
                *result = 24;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::displayCutsChanged)) {
                *result = 25;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::displayFeetChanged)) {
                *result = 26;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::firstFootTimeChanged)) {
                *result = 27;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::timeBetweenTwoFeetChanged)) {
                *result = 28;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::displayVerticalScaleChanged)) {
                *result = 29;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::verticalScaleSpaceInSecondsChanged)) {
                *result = 30;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::cutWidthChanged)) {
                *result = 31;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::displayBackgroundChanged)) {
                *result = 32;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::backgroundColorLightChanged)) {
                *result = 33;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::backgroundColorDarkChanged)) {
                *result = 34;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::synchroProtocolChanged)) {
                *result = 35;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::ltcInputPortChanged)) {
                *result = 36;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::ltcAutoDetectTimeCodeTypeChanged)) {
                *result = 37;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::ltcReaderTimeCodeTypeChanged)) {
                *result = 38;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::mtcInputPortChanged)) {
                *result = 39;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::mtcVirtualInputPortChanged)) {
                *result = 40;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::mtcInputUseExistingPortChanged)) {
                *result = 41;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::mtcForce24as2398Changed)) {
                *result = 42;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::sendMmcMessageChanged)) {
                *result = 43;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::mmcOutputPortChanged)) {
                *result = 44;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::peopleDialogGeometryChanged)) {
                *result = 45;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::lastVideoFolderChanged)) {
                *result = 46;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::stripFileTypeChanged)) {
                *result = 47;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::videoFileTypeChanged)) {
                *result = 48;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::displayControlPanelChanged)) {
                *result = 49;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::logMaskChanged)) {
                *result = 50;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::displayLogoChanged)) {
                *result = 51;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::languageChanged)) {
                *result = 52;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::hideStripChanged)) {
                *result = 53;
                return;
            }
        }
        {
            typedef void (JokerSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&JokerSettings::lastPreferencesTabChanged)) {
                *result = 54;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        JokerSettings *_t = static_cast<JokerSettings *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->fullScreen(); break;
        case 1: *reinterpret_cast< bool*>(_v) = _t->exitedNormaly(); break;
        case 2: *reinterpret_cast< QByteArray*>(_v) = _t->windowGeometry(); break;
        case 3: *reinterpret_cast< QString*>(_v) = _t->currentDocument(); break;
        case 4: *reinterpret_cast< QString*>(_v) = _t->lastDocumentFolder(); break;
        case 5: *reinterpret_cast< QStringList*>(_v) = _t->recentDocumentList(); break;
        case 6: *reinterpret_cast< int*>(_v) = _t->maxRecentDocument(); break;
        case 7: *reinterpret_cast< bool*>(_v) = _t->autoReload(); break;
        case 8: *reinterpret_cast< int*>(_v) = _t->screenDelay(); break;
        case 9: *reinterpret_cast< bool*>(_v) = _t->syncLooping(); break;
        case 10: *reinterpret_cast< bool*>(_v) = _t->displayInfo(); break;
        case 11: *reinterpret_cast< bool*>(_v) = _t->resetInfo(); break;
        case 12: *reinterpret_cast< float*>(_v) = _t->stripHeight(); break;
        case 13: *reinterpret_cast< int*>(_v) = _t->horizontalTimePerPixel(); break;
        case 14: *reinterpret_cast< int*>(_v) = _t->verticalTimePerPixel(); break;
        case 15: *reinterpret_cast< QString*>(_v) = _t->backgroundImageLight(); break;
        case 16: *reinterpret_cast< QString*>(_v) = _t->backgroundImageDark(); break;
        case 17: *reinterpret_cast< QString*>(_v) = _t->hudFontFamily(); break;
        case 18: *reinterpret_cast< QString*>(_v) = _t->textFontFamily(); break;
        case 19: *reinterpret_cast< bool*>(_v) = _t->stripTestMode(); break;
        case 20: *reinterpret_cast< bool*>(_v) = _t->displayNextText(); break;
        case 21: *reinterpret_cast< bool*>(_v) = _t->hideSelectedPeoples(); break;
        case 22: *reinterpret_cast< QStringList*>(_v) = _t->selectedPeopleNameList(); break;
        case 23: *reinterpret_cast< bool*>(_v) = _t->invertColor(); break;
        case 24: *reinterpret_cast< bool*>(_v) = _t->displayCuts(); break;
        case 25: *reinterpret_cast< bool*>(_v) = _t->displayFeet(); break;
        case 26: *reinterpret_cast< int*>(_v) = _t->firstFootTime(); break;
        case 27: *reinterpret_cast< int*>(_v) = _t->timeBetweenTwoFeet(); break;
        case 28: *reinterpret_cast< bool*>(_v) = _t->displayVerticalScale(); break;
        case 29: *reinterpret_cast< int*>(_v) = _t->verticalScaleSpaceInSeconds(); break;
        case 30: *reinterpret_cast< int*>(_v) = _t->cutWidth(); break;
        case 31: *reinterpret_cast< bool*>(_v) = _t->displayBackground(); break;
        case 32: *reinterpret_cast< int*>(_v) = _t->backgroundColorLight(); break;
        case 33: *reinterpret_cast< int*>(_v) = _t->backgroundColorDark(); break;
        case 34: *reinterpret_cast< int*>(_v) = _t->synchroProtocol(); break;
        case 35: *reinterpret_cast< QString*>(_v) = _t->ltcInputPort(); break;
        case 36: *reinterpret_cast< bool*>(_v) = _t->ltcAutoDetectTimeCodeType(); break;
        case 37: *reinterpret_cast< int*>(_v) = _t->ltcReaderTimeCodeType(); break;
        case 38: *reinterpret_cast< QString*>(_v) = _t->mtcInputPort(); break;
        case 39: *reinterpret_cast< QString*>(_v) = _t->mtcVirtualInputPort(); break;
        case 40: *reinterpret_cast< bool*>(_v) = _t->mtcInputUseExistingPort(); break;
        case 41: *reinterpret_cast< bool*>(_v) = _t->mtcForce24as2398(); break;
        case 42: *reinterpret_cast< bool*>(_v) = _t->sendMmcMessage(); break;
        case 43: *reinterpret_cast< QString*>(_v) = _t->mmcOutputPort(); break;
        case 44: *reinterpret_cast< QByteArray*>(_v) = _t->peopleDialogGeometry(); break;
        case 45: *reinterpret_cast< QString*>(_v) = _t->lastVideoFolder(); break;
        case 46: *reinterpret_cast< QStringList*>(_v) = _t->stripFileType(); break;
        case 47: *reinterpret_cast< QStringList*>(_v) = _t->videoFileType(); break;
        case 48: *reinterpret_cast< bool*>(_v) = _t->displayControlPanel(); break;
        case 49: *reinterpret_cast< int*>(_v) = _t->logMask(); break;
        case 50: *reinterpret_cast< bool*>(_v) = _t->displayLogo(); break;
        case 51: *reinterpret_cast< QString*>(_v) = _t->language(); break;
        case 52: *reinterpret_cast< bool*>(_v) = _t->hideStrip(); break;
        case 53: *reinterpret_cast< int*>(_v) = _t->lastPreferencesTab(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        JokerSettings *_t = static_cast<JokerSettings *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setFullScreen(*reinterpret_cast< bool*>(_v)); break;
        case 1: _t->setExitedNormaly(*reinterpret_cast< bool*>(_v)); break;
        case 2: _t->setWindowGeometry(*reinterpret_cast< QByteArray*>(_v)); break;
        case 3: _t->setCurrentDocument(*reinterpret_cast< QString*>(_v)); break;
        case 4: _t->setLastDocumentFolder(*reinterpret_cast< QString*>(_v)); break;
        case 5: _t->setRecentDocumentList(*reinterpret_cast< QStringList*>(_v)); break;
        case 6: _t->setMaxRecentDocument(*reinterpret_cast< int*>(_v)); break;
        case 7: _t->setAutoReload(*reinterpret_cast< bool*>(_v)); break;
        case 8: _t->setScreenDelay(*reinterpret_cast< int*>(_v)); break;
        case 9: _t->setSyncLooping(*reinterpret_cast< bool*>(_v)); break;
        case 10: _t->setDisplayInfo(*reinterpret_cast< bool*>(_v)); break;
        case 11: _t->setResetInfo(*reinterpret_cast< bool*>(_v)); break;
        case 12: _t->setStripHeight(*reinterpret_cast< float*>(_v)); break;
        case 13: _t->setHorizontalTimePerPixel(*reinterpret_cast< int*>(_v)); break;
        case 14: _t->setVerticalTimePerPixel(*reinterpret_cast< int*>(_v)); break;
        case 15: _t->setBackgroundImageLight(*reinterpret_cast< QString*>(_v)); break;
        case 16: _t->setBackgroundImageDark(*reinterpret_cast< QString*>(_v)); break;
        case 17: _t->setHudFontFamily(*reinterpret_cast< QString*>(_v)); break;
        case 18: _t->setTextFontFamily(*reinterpret_cast< QString*>(_v)); break;
        case 19: _t->setStripTestMode(*reinterpret_cast< bool*>(_v)); break;
        case 20: _t->setDisplayNextText(*reinterpret_cast< bool*>(_v)); break;
        case 21: _t->setHideSelectedPeoples(*reinterpret_cast< bool*>(_v)); break;
        case 22: _t->setSelectedPeopleNameList(*reinterpret_cast< QStringList*>(_v)); break;
        case 23: _t->setInvertColor(*reinterpret_cast< bool*>(_v)); break;
        case 24: _t->setDisplayCuts(*reinterpret_cast< bool*>(_v)); break;
        case 25: _t->setDisplayFeet(*reinterpret_cast< bool*>(_v)); break;
        case 26: _t->setFirstFootTime(*reinterpret_cast< int*>(_v)); break;
        case 27: _t->setTimeBetweenTwoFeet(*reinterpret_cast< int*>(_v)); break;
        case 28: _t->setDisplayVerticalScale(*reinterpret_cast< bool*>(_v)); break;
        case 29: _t->setVerticalScaleSpaceInSeconds(*reinterpret_cast< int*>(_v)); break;
        case 30: _t->setCutWidth(*reinterpret_cast< int*>(_v)); break;
        case 31: _t->setDisplayBackground(*reinterpret_cast< bool*>(_v)); break;
        case 32: _t->setBackgroundColorLight(*reinterpret_cast< int*>(_v)); break;
        case 33: _t->setBackgroundColorDark(*reinterpret_cast< int*>(_v)); break;
        case 34: _t->setSynchroProtocol(*reinterpret_cast< int*>(_v)); break;
        case 35: _t->setLtcInputPort(*reinterpret_cast< QString*>(_v)); break;
        case 36: _t->setLtcAutoDetectTimeCodeType(*reinterpret_cast< bool*>(_v)); break;
        case 37: _t->setLtcReaderTimeCodeType(*reinterpret_cast< int*>(_v)); break;
        case 38: _t->setMtcInputPort(*reinterpret_cast< QString*>(_v)); break;
        case 39: _t->setMtcVirtualInputPort(*reinterpret_cast< QString*>(_v)); break;
        case 40: _t->setMtcInputUseExistingPort(*reinterpret_cast< bool*>(_v)); break;
        case 41: _t->setMtcForce24as2398(*reinterpret_cast< bool*>(_v)); break;
        case 42: _t->setSendMmcMessage(*reinterpret_cast< bool*>(_v)); break;
        case 43: _t->setMmcOutputPort(*reinterpret_cast< QString*>(_v)); break;
        case 44: _t->setPeopleDialogGeometry(*reinterpret_cast< QByteArray*>(_v)); break;
        case 45: _t->setLastVideoFolder(*reinterpret_cast< QString*>(_v)); break;
        case 46: _t->setStripFileType(*reinterpret_cast< QStringList*>(_v)); break;
        case 47: _t->setVideoFileType(*reinterpret_cast< QStringList*>(_v)); break;
        case 48: _t->setDisplayControlPanel(*reinterpret_cast< bool*>(_v)); break;
        case 49: _t->setLogMask(*reinterpret_cast< int*>(_v)); break;
        case 50: _t->setDisplayLogo(*reinterpret_cast< bool*>(_v)); break;
        case 51: _t->setLanguage(*reinterpret_cast< QString*>(_v)); break;
        case 52: _t->setHideStrip(*reinterpret_cast< bool*>(_v)); break;
        case 53: _t->setLastPreferencesTab(*reinterpret_cast< int*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_a);
}

const QMetaObject JokerSettings::staticMetaObject = {
    { &PhGenericSettings::staticMetaObject, qt_meta_stringdata_JokerSettings.data,
      qt_meta_data_JokerSettings,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *JokerSettings::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *JokerSettings::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_JokerSettings.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "PhGraphicStripSettings"))
        return static_cast< PhGraphicStripSettings*>(this);
    if (!strcmp(_clname, "PhDocumentWindowSettings"))
        return static_cast< PhDocumentWindowSettings*>(this);
    if (!strcmp(_clname, "PhLtcReaderSettings"))
        return static_cast< PhLtcReaderSettings*>(this);
    if (!strcmp(_clname, "PhSyncSettings"))
        return static_cast< PhSyncSettings*>(this);
    return PhGenericSettings::qt_metacast(_clname);
}

int JokerSettings::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PhGenericSettings::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 55)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 55;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 55)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 55;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 54;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 54;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 54;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 54;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 54;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 54;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void JokerSettings::fullScreenChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void JokerSettings::exitedNormalyChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void JokerSettings::windowGeometryChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void JokerSettings::currentDocumentChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void JokerSettings::lastDocumentFolderChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void JokerSettings::recentDocumentListChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void JokerSettings::maxRecentDocumentChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void JokerSettings::autoReloadChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void JokerSettings::bookmarkChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void JokerSettings::screenDelayChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, nullptr);
}

// SIGNAL 10
void JokerSettings::syncLoopingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, nullptr);
}

// SIGNAL 11
void JokerSettings::displayInfoChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 11, nullptr);
}

// SIGNAL 12
void JokerSettings::resetInfoChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 12, nullptr);
}

// SIGNAL 13
void JokerSettings::stripHeightChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 13, nullptr);
}

// SIGNAL 14
void JokerSettings::horizontalTimePerPixelChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 14, nullptr);
}

// SIGNAL 15
void JokerSettings::verticalTimePerPixelChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 15, nullptr);
}

// SIGNAL 16
void JokerSettings::backgroundImageLightChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 16, nullptr);
}

// SIGNAL 17
void JokerSettings::backgroundImageDarkChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 17, nullptr);
}

// SIGNAL 18
void JokerSettings::hudFontFamilyChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 18, nullptr);
}

// SIGNAL 19
void JokerSettings::textFontFamilyChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 19, nullptr);
}

// SIGNAL 20
void JokerSettings::stripTestModeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 20, nullptr);
}

// SIGNAL 21
void JokerSettings::displayNextTextChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 21, nullptr);
}

// SIGNAL 22
void JokerSettings::hideSelectedPeoplesChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 22, nullptr);
}

// SIGNAL 23
void JokerSettings::selectedPeopleNameListChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 23, nullptr);
}

// SIGNAL 24
void JokerSettings::invertColorChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 24, nullptr);
}

// SIGNAL 25
void JokerSettings::displayCutsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 25, nullptr);
}

// SIGNAL 26
void JokerSettings::displayFeetChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 26, nullptr);
}

// SIGNAL 27
void JokerSettings::firstFootTimeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 27, nullptr);
}

// SIGNAL 28
void JokerSettings::timeBetweenTwoFeetChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 28, nullptr);
}

// SIGNAL 29
void JokerSettings::displayVerticalScaleChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 29, nullptr);
}

// SIGNAL 30
void JokerSettings::verticalScaleSpaceInSecondsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 30, nullptr);
}

// SIGNAL 31
void JokerSettings::cutWidthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 31, nullptr);
}

// SIGNAL 32
void JokerSettings::displayBackgroundChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 32, nullptr);
}

// SIGNAL 33
void JokerSettings::backgroundColorLightChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 33, nullptr);
}

// SIGNAL 34
void JokerSettings::backgroundColorDarkChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 34, nullptr);
}

// SIGNAL 35
void JokerSettings::synchroProtocolChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 35, nullptr);
}

// SIGNAL 36
void JokerSettings::ltcInputPortChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 36, nullptr);
}

// SIGNAL 37
void JokerSettings::ltcAutoDetectTimeCodeTypeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 37, nullptr);
}

// SIGNAL 38
void JokerSettings::ltcReaderTimeCodeTypeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 38, nullptr);
}

// SIGNAL 39
void JokerSettings::mtcInputPortChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 39, nullptr);
}

// SIGNAL 40
void JokerSettings::mtcVirtualInputPortChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 40, nullptr);
}

// SIGNAL 41
void JokerSettings::mtcInputUseExistingPortChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 41, nullptr);
}

// SIGNAL 42
void JokerSettings::mtcForce24as2398Changed()
{
    QMetaObject::activate(this, &staticMetaObject, 42, nullptr);
}

// SIGNAL 43
void JokerSettings::sendMmcMessageChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 43, nullptr);
}

// SIGNAL 44
void JokerSettings::mmcOutputPortChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 44, nullptr);
}

// SIGNAL 45
void JokerSettings::peopleDialogGeometryChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 45, nullptr);
}

// SIGNAL 46
void JokerSettings::lastVideoFolderChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 46, nullptr);
}

// SIGNAL 47
void JokerSettings::stripFileTypeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 47, nullptr);
}

// SIGNAL 48
void JokerSettings::videoFileTypeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 48, nullptr);
}

// SIGNAL 49
void JokerSettings::displayControlPanelChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 49, nullptr);
}

// SIGNAL 50
void JokerSettings::logMaskChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 50, nullptr);
}

// SIGNAL 51
void JokerSettings::displayLogoChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 51, nullptr);
}

// SIGNAL 52
void JokerSettings::languageChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 52, nullptr);
}

// SIGNAL 53
void JokerSettings::hideStripChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 53, nullptr);
}

// SIGNAL 54
void JokerSettings::lastPreferencesTabChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 54, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE

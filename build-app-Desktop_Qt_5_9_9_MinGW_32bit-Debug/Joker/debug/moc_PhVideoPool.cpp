/****************************************************************************
** Meta object code from reading C++ file 'PhVideoPool.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.9)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../libs/PhVideo/PhVideoPool.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'PhVideoPool.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.9. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_PhVideoPool_t {
    QByteArrayData data[13];
    char stringdata0[149];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PhVideoPool_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PhVideoPool_t qt_meta_stringdata_PhVideoPool = {
    {
QT_MOC_LITERAL(0, 0, 11), // "PhVideoPool"
QT_MOC_LITERAL(1, 12, 13), // "recycledFrame"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 14), // "PhVideoBuffer*"
QT_MOC_LITERAL(4, 42, 6), // "buffer"
QT_MOC_LITERAL(5, 49, 4), // "stop"
QT_MOC_LITERAL(6, 54, 15), // "poolTimeChanged"
QT_MOC_LITERAL(7, 70, 7), // "PhFrame"
QT_MOC_LITERAL(8, 78, 10), // "stripFrame"
QT_MOC_LITERAL(9, 89, 8), // "backward"
QT_MOC_LITERAL(10, 98, 18), // "stripFrameIsInPool"
QT_MOC_LITERAL(11, 117, 16), // "stripTimeChanged"
QT_MOC_LITERAL(12, 134, 14) // "frameAvailable"

    },
    "PhVideoPool\0recycledFrame\0\0PhVideoBuffer*\0"
    "buffer\0stop\0poolTimeChanged\0PhFrame\0"
    "stripFrame\0backward\0stripFrameIsInPool\0"
    "stripTimeChanged\0frameAvailable"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PhVideoPool[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x06 /* Public */,
       5,    0,   42,    2, 0x06 /* Public */,
       6,    3,   43,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      11,    2,   50,    2, 0x0a /* Public */,
      12,    1,   55,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 7, QMetaType::Bool, QMetaType::Bool,    8,    9,   10,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 7, QMetaType::Bool,    8,    9,
    QMetaType::Void, 0x80000000 | 3,    4,

       0        // eod
};

void PhVideoPool::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        PhVideoPool *_t = static_cast<PhVideoPool *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->recycledFrame((*reinterpret_cast< PhVideoBuffer*(*)>(_a[1]))); break;
        case 1: _t->stop(); break;
        case 2: _t->poolTimeChanged((*reinterpret_cast< PhFrame(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3]))); break;
        case 3: _t->stripTimeChanged((*reinterpret_cast< PhFrame(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 4: _t->frameAvailable((*reinterpret_cast< PhVideoBuffer*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (PhVideoPool::*_t)(PhVideoBuffer * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PhVideoPool::recycledFrame)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (PhVideoPool::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PhVideoPool::stop)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (PhVideoPool::*_t)(PhFrame , bool , bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PhVideoPool::poolTimeChanged)) {
                *result = 2;
                return;
            }
        }
    }
}

const QMetaObject PhVideoPool::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_PhVideoPool.data,
      qt_meta_data_PhVideoPool,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *PhVideoPool::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PhVideoPool::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PhVideoPool.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int PhVideoPool::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void PhVideoPool::recycledFrame(PhVideoBuffer * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void PhVideoPool::stop()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void PhVideoPool::poolTimeChanged(PhFrame _t1, bool _t2, bool _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE

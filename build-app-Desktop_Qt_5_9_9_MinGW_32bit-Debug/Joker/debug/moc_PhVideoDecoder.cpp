/****************************************************************************
** Meta object code from reading C++ file 'PhVideoDecoder.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.9)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../libs/PhVideo/PhVideoDecoder.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'PhVideoDecoder.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.9. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_PhVideoDecoder_t {
    QByteArrayData data[27];
    char stringdata0[271];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PhVideoDecoder_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PhVideoDecoder_t qt_meta_stringdata_PhVideoDecoder = {
    {
QT_MOC_LITERAL(0, 0, 14), // "PhVideoDecoder"
QT_MOC_LITERAL(1, 15, 14), // "frameAvailable"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 14), // "PhVideoBuffer*"
QT_MOC_LITERAL(4, 46, 6), // "buffer"
QT_MOC_LITERAL(5, 53, 6), // "opened"
QT_MOC_LITERAL(6, 60, 14), // "PhTimeCodeType"
QT_MOC_LITERAL(7, 75, 6), // "tcType"
QT_MOC_LITERAL(8, 82, 7), // "PhFrame"
QT_MOC_LITERAL(9, 90, 7), // "frameIn"
QT_MOC_LITERAL(10, 98, 11), // "frameLength"
QT_MOC_LITERAL(11, 110, 5), // "width"
QT_MOC_LITERAL(12, 116, 6), // "height"
QT_MOC_LITERAL(13, 123, 9), // "codecName"
QT_MOC_LITERAL(14, 133, 10), // "openFailed"
QT_MOC_LITERAL(15, 144, 4), // "open"
QT_MOC_LITERAL(16, 149, 8), // "fileName"
QT_MOC_LITERAL(17, 158, 5), // "close"
QT_MOC_LITERAL(18, 164, 14), // "setDeinterlace"
QT_MOC_LITERAL(19, 179, 11), // "deinterlace"
QT_MOC_LITERAL(20, 191, 16), // "stripTimeChanged"
QT_MOC_LITERAL(21, 208, 10), // "stripFrame"
QT_MOC_LITERAL(22, 219, 8), // "backward"
QT_MOC_LITERAL(23, 228, 18), // "stripFrameIsInPool"
QT_MOC_LITERAL(24, 247, 4), // "stop"
QT_MOC_LITERAL(25, 252, 12), // "recycleFrame"
QT_MOC_LITERAL(26, 265, 5) // "frame"

    },
    "PhVideoDecoder\0frameAvailable\0\0"
    "PhVideoBuffer*\0buffer\0opened\0"
    "PhTimeCodeType\0tcType\0PhFrame\0frameIn\0"
    "frameLength\0width\0height\0codecName\0"
    "openFailed\0open\0fileName\0close\0"
    "setDeinterlace\0deinterlace\0stripTimeChanged\0"
    "stripFrame\0backward\0stripFrameIsInPool\0"
    "stop\0recycleFrame\0frame"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PhVideoDecoder[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   59,    2, 0x06 /* Public */,
       5,    6,   62,    2, 0x06 /* Public */,
      14,    0,   75,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      15,    1,   76,    2, 0x0a /* Public */,
      17,    0,   79,    2, 0x0a /* Public */,
      18,    1,   80,    2, 0x0a /* Public */,
      20,    3,   83,    2, 0x0a /* Public */,
      24,    0,   90,    2, 0x0a /* Public */,
      25,    1,   91,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 6, 0x80000000 | 8, 0x80000000 | 8, QMetaType::Int, QMetaType::Int, QMetaType::QString,    7,    9,   10,   11,   12,   13,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,   16,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   19,
    QMetaType::Void, 0x80000000 | 8, QMetaType::Bool, QMetaType::Bool,   21,   22,   23,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 3,   26,

       0        // eod
};

void PhVideoDecoder::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        PhVideoDecoder *_t = static_cast<PhVideoDecoder *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->frameAvailable((*reinterpret_cast< PhVideoBuffer*(*)>(_a[1]))); break;
        case 1: _t->opened((*reinterpret_cast< PhTimeCodeType(*)>(_a[1])),(*reinterpret_cast< PhFrame(*)>(_a[2])),(*reinterpret_cast< PhFrame(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5])),(*reinterpret_cast< QString(*)>(_a[6]))); break;
        case 2: _t->openFailed(); break;
        case 3: _t->open((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: _t->close(); break;
        case 5: _t->setDeinterlace((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->stripTimeChanged((*reinterpret_cast< PhFrame(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3]))); break;
        case 7: _t->stop(); break;
        case 8: _t->recycleFrame((*reinterpret_cast< PhVideoBuffer*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (PhVideoDecoder::*_t)(PhVideoBuffer * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PhVideoDecoder::frameAvailable)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (PhVideoDecoder::*_t)(PhTimeCodeType , PhFrame , PhFrame , int , int , QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PhVideoDecoder::opened)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (PhVideoDecoder::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PhVideoDecoder::openFailed)) {
                *result = 2;
                return;
            }
        }
    }
}

const QMetaObject PhVideoDecoder::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_PhVideoDecoder.data,
      qt_meta_data_PhVideoDecoder,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *PhVideoDecoder::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PhVideoDecoder::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PhVideoDecoder.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int PhVideoDecoder::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void PhVideoDecoder::frameAvailable(PhVideoBuffer * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void PhVideoDecoder::opened(PhTimeCodeType _t1, PhFrame _t2, PhFrame _t3, int _t4, int _t5, QString _t6)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)), const_cast<void*>(reinterpret_cast<const void*>(&_t5)), const_cast<void*>(reinterpret_cast<const void*>(&_t6)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void PhVideoDecoder::openFailed()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE

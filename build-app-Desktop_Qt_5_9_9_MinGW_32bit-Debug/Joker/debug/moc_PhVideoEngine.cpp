/****************************************************************************
** Meta object code from reading C++ file 'PhVideoEngine.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.9)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../libs/PhVideo/PhVideoEngine.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'PhVideoEngine.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.9. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_PhVideoEngine_t {
    QByteArrayData data[31];
    char stringdata0[355];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PhVideoEngine_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PhVideoEngine_t qt_meta_stringdata_PhVideoEngine = {
    {
QT_MOC_LITERAL(0, 0, 13), // "PhVideoEngine"
QT_MOC_LITERAL(1, 14, 19), // "timeCodeTypeChanged"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 14), // "PhTimeCodeType"
QT_MOC_LITERAL(4, 50, 6), // "tcType"
QT_MOC_LITERAL(5, 57, 13), // "openInDecoder"
QT_MOC_LITERAL(6, 71, 8), // "fileName"
QT_MOC_LITERAL(7, 80, 14), // "closeInDecoder"
QT_MOC_LITERAL(8, 95, 6), // "opened"
QT_MOC_LITERAL(9, 102, 7), // "success"
QT_MOC_LITERAL(10, 110, 18), // "deinterlaceChanged"
QT_MOC_LITERAL(11, 129, 11), // "deinterlace"
QT_MOC_LITERAL(12, 141, 15), // "newFrameDecoded"
QT_MOC_LITERAL(13, 157, 7), // "PhFrame"
QT_MOC_LITERAL(14, 165, 5), // "frame"
QT_MOC_LITERAL(15, 171, 11), // "stopDecoder"
QT_MOC_LITERAL(16, 183, 16), // "stripTimeChanged"
QT_MOC_LITERAL(17, 200, 10), // "stripFrame"
QT_MOC_LITERAL(18, 211, 8), // "backward"
QT_MOC_LITERAL(19, 220, 14), // "frameAvailable"
QT_MOC_LITERAL(20, 235, 14), // "PhVideoBuffer*"
QT_MOC_LITERAL(21, 250, 6), // "buffer"
QT_MOC_LITERAL(22, 257, 13), // "decoderOpened"
QT_MOC_LITERAL(23, 271, 7), // "frameIn"
QT_MOC_LITERAL(24, 279, 11), // "frameLength"
QT_MOC_LITERAL(25, 291, 5), // "width"
QT_MOC_LITERAL(26, 297, 6), // "height"
QT_MOC_LITERAL(27, 304, 9), // "codecName"
QT_MOC_LITERAL(28, 314, 19), // "openInDecoderFailed"
QT_MOC_LITERAL(29, 334, 13), // "onTimeChanged"
QT_MOC_LITERAL(30, 348, 6) // "PhTime"

    },
    "PhVideoEngine\0timeCodeTypeChanged\0\0"
    "PhTimeCodeType\0tcType\0openInDecoder\0"
    "fileName\0closeInDecoder\0opened\0success\0"
    "deinterlaceChanged\0deinterlace\0"
    "newFrameDecoded\0PhFrame\0frame\0stopDecoder\0"
    "stripTimeChanged\0stripFrame\0backward\0"
    "frameAvailable\0PhVideoBuffer*\0buffer\0"
    "decoderOpened\0frameIn\0frameLength\0"
    "width\0height\0codecName\0openInDecoderFailed\0"
    "onTimeChanged\0PhTime"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PhVideoEngine[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       8,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   74,    2, 0x06 /* Public */,
       5,    1,   77,    2, 0x06 /* Public */,
       7,    0,   80,    2, 0x06 /* Public */,
       8,    1,   81,    2, 0x06 /* Public */,
      10,    1,   84,    2, 0x06 /* Public */,
      12,    1,   87,    2, 0x06 /* Public */,
      15,    0,   90,    2, 0x06 /* Public */,
      16,    2,   91,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      19,    1,   96,    2, 0x0a /* Public */,
      22,    6,   99,    2, 0x0a /* Public */,
      28,    0,  112,    2, 0x0a /* Public */,
      29,    1,  113,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::QString,    6,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    9,
    QMetaType::Void, QMetaType::Bool,   11,
    QMetaType::Void, 0x80000000 | 13,   14,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 13, QMetaType::Bool,   17,   18,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 20,   21,
    QMetaType::Void, 0x80000000 | 3, 0x80000000 | 13, 0x80000000 | 13, QMetaType::Int, QMetaType::Int, QMetaType::QString,    4,   23,   24,   25,   26,   27,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 30,    2,

       0        // eod
};

void PhVideoEngine::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        PhVideoEngine *_t = static_cast<PhVideoEngine *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->timeCodeTypeChanged((*reinterpret_cast< PhTimeCodeType(*)>(_a[1]))); break;
        case 1: _t->openInDecoder((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->closeInDecoder(); break;
        case 3: _t->opened((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->deinterlaceChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->newFrameDecoded((*reinterpret_cast< PhFrame(*)>(_a[1]))); break;
        case 6: _t->stopDecoder(); break;
        case 7: _t->stripTimeChanged((*reinterpret_cast< PhFrame(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 8: _t->frameAvailable((*reinterpret_cast< PhVideoBuffer*(*)>(_a[1]))); break;
        case 9: _t->decoderOpened((*reinterpret_cast< PhTimeCodeType(*)>(_a[1])),(*reinterpret_cast< PhFrame(*)>(_a[2])),(*reinterpret_cast< PhFrame(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5])),(*reinterpret_cast< QString(*)>(_a[6]))); break;
        case 10: _t->openInDecoderFailed(); break;
        case 11: _t->onTimeChanged((*reinterpret_cast< PhTime(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (PhVideoEngine::*_t)(PhTimeCodeType );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PhVideoEngine::timeCodeTypeChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (PhVideoEngine::*_t)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PhVideoEngine::openInDecoder)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (PhVideoEngine::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PhVideoEngine::closeInDecoder)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (PhVideoEngine::*_t)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PhVideoEngine::opened)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (PhVideoEngine::*_t)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PhVideoEngine::deinterlaceChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (PhVideoEngine::*_t)(PhFrame );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PhVideoEngine::newFrameDecoded)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (PhVideoEngine::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PhVideoEngine::stopDecoder)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (PhVideoEngine::*_t)(PhFrame , bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PhVideoEngine::stripTimeChanged)) {
                *result = 7;
                return;
            }
        }
    }
}

const QMetaObject PhVideoEngine::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_PhVideoEngine.data,
      qt_meta_data_PhVideoEngine,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *PhVideoEngine::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PhVideoEngine::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PhVideoEngine.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int PhVideoEngine::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 12)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 12;
    }
    return _id;
}

// SIGNAL 0
void PhVideoEngine::timeCodeTypeChanged(PhTimeCodeType _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void PhVideoEngine::openInDecoder(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void PhVideoEngine::closeInDecoder()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void PhVideoEngine::opened(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void PhVideoEngine::deinterlaceChanged(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void PhVideoEngine::newFrameDecoded(PhFrame _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void PhVideoEngine::stopDecoder()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void PhVideoEngine::stripTimeChanged(PhFrame _t1, bool _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE

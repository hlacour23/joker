/********************************************************************************
** Form generated from reading UI file 'PeopleEditionDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PEOPLEEDITIONDIALOG_H
#define UI_PEOPLEEDITIONDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>
#include "PhCommonUI/PhDialogButtonBox.h"

QT_BEGIN_NAMESPACE

class Ui_PhColorPickerDialog
{
public:
    PhDialogButtonBox *buttonBox;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QLabel *labelName;
    QPushButton *pbColor;

    void setupUi(QDialog *PhColorPickerDialog)
    {
        if (PhColorPickerDialog->objectName().isEmpty())
            PhColorPickerDialog->setObjectName(QStringLiteral("PhColorPickerDialog"));
        PhColorPickerDialog->resize(275, 120);
        PhColorPickerDialog->setStyleSheet(QStringLiteral(""));
        buttonBox = new PhDialogButtonBox(PhColorPickerDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(-70, 80, 341, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        horizontalLayoutWidget = new QWidget(PhColorPickerDialog);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(10, 0, 257, 80));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        labelName = new QLabel(horizontalLayoutWidget);
        labelName->setObjectName(QStringLiteral("labelName"));
        labelName->setText(QStringLiteral("TextLabel"));

        horizontalLayout->addWidget(labelName);

        pbColor = new QPushButton(horizontalLayoutWidget);
        pbColor->setObjectName(QStringLiteral("pbColor"));
        pbColor->setStyleSheet(QStringLiteral(""));

        horizontalLayout->addWidget(pbColor);


        retranslateUi(PhColorPickerDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), PhColorPickerDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), PhColorPickerDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(PhColorPickerDialog);
    } // setupUi

    void retranslateUi(QDialog *PhColorPickerDialog)
    {
        PhColorPickerDialog->setWindowTitle(QApplication::translate("PhColorPickerDialog", "Modify", Q_NULLPTR));
        pbColor->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class PhColorPickerDialog: public Ui_PhColorPickerDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PEOPLEEDITIONDIALOG_H

/********************************************************************************
** Form generated from reading UI file 'PreferencesDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PREFERENCESDIALOG_H
#define UI_PREFERENCESDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>

QT_BEGIN_NAMESPACE

class Ui_PreferencesDialog
{
public:
    QFormLayout *formLayout;
    QLabel *videoSyncFrameRateLabel;
    QComboBox *videoSyncFrameRateComboBox;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *PreferencesDialog)
    {
        if (PreferencesDialog->objectName().isEmpty())
            PreferencesDialog->setObjectName(QStringLiteral("PreferencesDialog"));
        PreferencesDialog->resize(277, 114);
        formLayout = new QFormLayout(PreferencesDialog);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        videoSyncFrameRateLabel = new QLabel(PreferencesDialog);
        videoSyncFrameRateLabel->setObjectName(QStringLiteral("videoSyncFrameRateLabel"));

        formLayout->setWidget(2, QFormLayout::LabelRole, videoSyncFrameRateLabel);

        videoSyncFrameRateComboBox = new QComboBox(PreferencesDialog);
        videoSyncFrameRateComboBox->setObjectName(QStringLiteral("videoSyncFrameRateComboBox"));

        formLayout->setWidget(2, QFormLayout::FieldRole, videoSyncFrameRateComboBox);

        buttonBox = new QDialogButtonBox(PreferencesDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        formLayout->setWidget(3, QFormLayout::SpanningRole, buttonBox);


        retranslateUi(PreferencesDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), PreferencesDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), PreferencesDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(PreferencesDialog);
    } // setupUi

    void retranslateUi(QDialog *PreferencesDialog)
    {
        PreferencesDialog->setWindowTitle(QApplication::translate("PreferencesDialog", "Dialog", Q_NULLPTR));
        videoSyncFrameRateLabel->setText(QApplication::translate("PreferencesDialog", "Video sync framerate:", Q_NULLPTR));
        videoSyncFrameRateComboBox->clear();
        videoSyncFrameRateComboBox->insertItems(0, QStringList()
         << QApplication::translate("PreferencesDialog", "23.98 fps", Q_NULLPTR)
         << QApplication::translate("PreferencesDialog", "24 fps", Q_NULLPTR)
         << QApplication::translate("PreferencesDialog", "25 fps", Q_NULLPTR)
         << QApplication::translate("PreferencesDialog", "29.97 fps", Q_NULLPTR)
         << QApplication::translate("PreferencesDialog", "30 fps", Q_NULLPTR)
        );
    } // retranslateUi

};

namespace Ui {
    class PreferencesDialog: public Ui_PreferencesDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PREFERENCESDIALOG_H

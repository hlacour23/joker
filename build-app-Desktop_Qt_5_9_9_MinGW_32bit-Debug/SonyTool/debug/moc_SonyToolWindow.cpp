/****************************************************************************
** Meta object code from reading C++ file 'SonyToolWindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.9)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../app/SonyTool/SonyToolWindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'SonyToolWindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.9. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SonyToolWindow_t {
    QByteArrayData data[21];
    char stringdata0[375];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SonyToolWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SonyToolWindow_t qt_meta_stringdata_SonyToolWindow = {
    {
QT_MOC_LITERAL(0, 0, 14), // "SonyToolWindow"
QT_MOC_LITERAL(1, 15, 15), // "masterNextFrame"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 19), // "masterPreviousFrame"
QT_MOC_LITERAL(4, 52, 14), // "onDeviceIdData"
QT_MOC_LITERAL(5, 67, 3), // "id1"
QT_MOC_LITERAL(6, 71, 3), // "id2"
QT_MOC_LITERAL(7, 75, 12), // "onStatusData"
QT_MOC_LITERAL(8, 88, 14), // "unsigned char*"
QT_MOC_LITERAL(9, 103, 10), // "statusData"
QT_MOC_LITERAL(10, 114, 6), // "offset"
QT_MOC_LITERAL(11, 121, 6), // "length"
QT_MOC_LITERAL(12, 128, 28), // "on_masterActiveCheck_clicked"
QT_MOC_LITERAL(13, 157, 7), // "checked"
QT_MOC_LITERAL(14, 165, 27), // "on_slaveActiveCheck_clicked"
QT_MOC_LITERAL(15, 193, 30), // "on_actionMaster_GoTo_triggered"
QT_MOC_LITERAL(16, 224, 29), // "on_actionSlave_GoTo_triggered"
QT_MOC_LITERAL(17, 254, 39), // "on_actionSlave_Use_video_sync..."
QT_MOC_LITERAL(18, 294, 8), // "useVideo"
QT_MOC_LITERAL(19, 303, 40), // "on_actionMaster_Use_video_syn..."
QT_MOC_LITERAL(20, 344, 30) // "on_actionPreferences_triggered"

    },
    "SonyToolWindow\0masterNextFrame\0\0"
    "masterPreviousFrame\0onDeviceIdData\0"
    "id1\0id2\0onStatusData\0unsigned char*\0"
    "statusData\0offset\0length\0"
    "on_masterActiveCheck_clicked\0checked\0"
    "on_slaveActiveCheck_clicked\0"
    "on_actionMaster_GoTo_triggered\0"
    "on_actionSlave_GoTo_triggered\0"
    "on_actionSlave_Use_video_sync_triggered\0"
    "useVideo\0on_actionMaster_Use_video_sync_triggered\0"
    "on_actionPreferences_triggered"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SonyToolWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   69,    2, 0x08 /* Private */,
       3,    0,   70,    2, 0x08 /* Private */,
       4,    2,   71,    2, 0x08 /* Private */,
       7,    3,   76,    2, 0x08 /* Private */,
      12,    1,   83,    2, 0x08 /* Private */,
      14,    1,   86,    2, 0x08 /* Private */,
      15,    0,   89,    2, 0x08 /* Private */,
      16,    0,   90,    2, 0x08 /* Private */,
      17,    1,   91,    2, 0x08 /* Private */,
      19,    1,   94,    2, 0x08 /* Private */,
      20,    0,   97,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::UChar, QMetaType::UChar,    5,    6,
    QMetaType::Void, 0x80000000 | 8, QMetaType::Int, QMetaType::Int,    9,   10,   11,
    QMetaType::Void, QMetaType::Bool,   13,
    QMetaType::Void, QMetaType::Bool,   13,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   18,
    QMetaType::Void, QMetaType::Bool,   18,
    QMetaType::Void,

       0        // eod
};

void SonyToolWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SonyToolWindow *_t = static_cast<SonyToolWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->masterNextFrame(); break;
        case 1: _t->masterPreviousFrame(); break;
        case 2: _t->onDeviceIdData((*reinterpret_cast< unsigned char(*)>(_a[1])),(*reinterpret_cast< unsigned char(*)>(_a[2]))); break;
        case 3: _t->onStatusData((*reinterpret_cast< unsigned char*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 4: _t->on_masterActiveCheck_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->on_slaveActiveCheck_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->on_actionMaster_GoTo_triggered(); break;
        case 7: _t->on_actionSlave_GoTo_triggered(); break;
        case 8: _t->on_actionSlave_Use_video_sync_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 9: _t->on_actionMaster_Use_video_sync_triggered((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: _t->on_actionPreferences_triggered(); break;
        default: ;
        }
    }
}

const QMetaObject SonyToolWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_SonyToolWindow.data,
      qt_meta_data_SonyToolWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *SonyToolWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SonyToolWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SonyToolWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int SonyToolWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 11;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE

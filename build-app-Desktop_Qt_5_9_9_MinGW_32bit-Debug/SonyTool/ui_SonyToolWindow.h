/********************************************************************************
** Form generated from reading UI file 'SonyToolWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SONYTOOLWINDOW_H
#define UI_SONYTOOLWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "PhCommonUI/PhMediaPanel.h"

QT_BEGIN_NAMESPACE

class Ui_SonyToolWindow
{
public:
    QAction *actionMaster_GoTo;
    QAction *actionSlave_GoTo;
    QAction *actionSlave_Use_video_sync;
    QAction *actionMaster_Use_video_sync;
    QAction *actionPreferences;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QCheckBox *masterActiveCheck;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *queryIdButton;
    QLabel *idLabel;
    QHBoxLayout *horizontalLayout;
    QPushButton *statusSenseButton;
    QLabel *statusLabel;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *timeSenseButton;
    QPushButton *speedSenseButton;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_3;
    PhMediaPanel *masterPanel;
    QCheckBox *slaveActiveCheck;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_4;
    PhMediaPanel *slavePanel;
    QMenuBar *menuBar;
    QMenu *menuMaster;
    QMenu *menuSlave;
    QMenu *menuTools;

    void setupUi(QMainWindow *SonyToolWindow)
    {
        if (SonyToolWindow->objectName().isEmpty())
            SonyToolWindow->setObjectName(QStringLiteral("SonyToolWindow"));
        SonyToolWindow->resize(424, 506);
        SonyToolWindow->setStyleSheet(QLatin1String("QGroupBox {\n"
"	background: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 rgba(60, 60, 60, 255), stop:1 rgba(0, 0, 0, 255));\n"
"    border: 1px solid gray;\n"
"    border-radius: 5px;\n"
"    margin-top: 3ex; /* leave space at the top for the title */\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    subcontrol-position: top center; /* position at the top center */\n"
"    padding: 0 10px;\n"
"}\n"
""));
        actionMaster_GoTo = new QAction(SonyToolWindow);
        actionMaster_GoTo->setObjectName(QStringLiteral("actionMaster_GoTo"));
        actionSlave_GoTo = new QAction(SonyToolWindow);
        actionSlave_GoTo->setObjectName(QStringLiteral("actionSlave_GoTo"));
        actionSlave_Use_video_sync = new QAction(SonyToolWindow);
        actionSlave_Use_video_sync->setObjectName(QStringLiteral("actionSlave_Use_video_sync"));
        actionSlave_Use_video_sync->setCheckable(true);
        actionSlave_Use_video_sync->setChecked(false);
        actionMaster_Use_video_sync = new QAction(SonyToolWindow);
        actionMaster_Use_video_sync->setObjectName(QStringLiteral("actionMaster_Use_video_sync"));
        actionMaster_Use_video_sync->setCheckable(true);
        actionMaster_Use_video_sync->setChecked(false);
        actionPreferences = new QAction(SonyToolWindow);
        actionPreferences->setObjectName(QStringLiteral("actionPreferences"));
        centralWidget = new QWidget(SonyToolWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        masterActiveCheck = new QCheckBox(centralWidget);
        masterActiveCheck->setObjectName(QStringLiteral("masterActiveCheck"));

        verticalLayout->addWidget(masterActiveCheck);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        queryIdButton = new QPushButton(centralWidget);
        queryIdButton->setObjectName(QStringLiteral("queryIdButton"));

        horizontalLayout_3->addWidget(queryIdButton);

        idLabel = new QLabel(centralWidget);
        idLabel->setObjectName(QStringLiteral("idLabel"));

        horizontalLayout_3->addWidget(idLabel);


        verticalLayout_2->addLayout(horizontalLayout_3);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        statusSenseButton = new QPushButton(centralWidget);
        statusSenseButton->setObjectName(QStringLiteral("statusSenseButton"));

        horizontalLayout->addWidget(statusSenseButton);

        statusLabel = new QLabel(centralWidget);
        statusLabel->setObjectName(QStringLiteral("statusLabel"));

        horizontalLayout->addWidget(statusLabel);


        verticalLayout_2->addLayout(horizontalLayout);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        timeSenseButton = new QPushButton(centralWidget);
        timeSenseButton->setObjectName(QStringLiteral("timeSenseButton"));

        horizontalLayout_4->addWidget(timeSenseButton);

        speedSenseButton = new QPushButton(centralWidget);
        speedSenseButton->setObjectName(QStringLiteral("speedSenseButton"));

        horizontalLayout_4->addWidget(speedSenseButton);


        verticalLayout_2->addLayout(horizontalLayout_4);


        verticalLayout->addLayout(verticalLayout_2);

        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        verticalLayout_3 = new QVBoxLayout(groupBox);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        masterPanel = new PhMediaPanel(groupBox);
        masterPanel->setObjectName(QStringLiteral("masterPanel"));

        verticalLayout_3->addWidget(masterPanel);


        verticalLayout->addWidget(groupBox);

        slaveActiveCheck = new QCheckBox(centralWidget);
        slaveActiveCheck->setObjectName(QStringLiteral("slaveActiveCheck"));

        verticalLayout->addWidget(slaveActiveCheck);

        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        verticalLayout_4 = new QVBoxLayout(groupBox_2);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        slavePanel = new PhMediaPanel(groupBox_2);
        slavePanel->setObjectName(QStringLiteral("slavePanel"));
        slavePanel->setStyleSheet(QStringLiteral(""));

        verticalLayout_4->addWidget(slavePanel);


        verticalLayout->addWidget(groupBox_2);

        SonyToolWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(SonyToolWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 424, 22));
        menuMaster = new QMenu(menuBar);
        menuMaster->setObjectName(QStringLiteral("menuMaster"));
        menuSlave = new QMenu(menuBar);
        menuSlave->setObjectName(QStringLiteral("menuSlave"));
        menuTools = new QMenu(menuBar);
        menuTools->setObjectName(QStringLiteral("menuTools"));
        SonyToolWindow->setMenuBar(menuBar);

        menuBar->addAction(menuMaster->menuAction());
        menuBar->addAction(menuSlave->menuAction());
        menuBar->addAction(menuTools->menuAction());
        menuMaster->addAction(actionMaster_GoTo);
        menuMaster->addSeparator();
        menuMaster->addAction(actionMaster_Use_video_sync);
        menuSlave->addAction(actionSlave_GoTo);
        menuSlave->addSeparator();
        menuSlave->addAction(actionSlave_Use_video_sync);
        menuTools->addAction(actionPreferences);

        retranslateUi(SonyToolWindow);

        QMetaObject::connectSlotsByName(SonyToolWindow);
    } // setupUi

    void retranslateUi(QMainWindow *SonyToolWindow)
    {
        SonyToolWindow->setWindowTitle(QApplication::translate("SonyToolWindow", "Sony Tool", Q_NULLPTR));
        actionMaster_GoTo->setText(QApplication::translate("SonyToolWindow", "Go to...", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionMaster_GoTo->setShortcut(QApplication::translate("SonyToolWindow", "Ctrl+G", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionSlave_GoTo->setText(QApplication::translate("SonyToolWindow", "Go to...", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionSlave_GoTo->setShortcut(QApplication::translate("SonyToolWindow", "Ctrl+K", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionSlave_Use_video_sync->setText(QApplication::translate("SonyToolWindow", "Use video sync", Q_NULLPTR));
        actionMaster_Use_video_sync->setText(QApplication::translate("SonyToolWindow", "Use video sync", Q_NULLPTR));
        actionPreferences->setText(QApplication::translate("SonyToolWindow", "Preferences...", Q_NULLPTR));
        masterActiveCheck->setText(QApplication::translate("SonyToolWindow", "Send Sony command", Q_NULLPTR));
        queryIdButton->setText(QApplication::translate("SonyToolWindow", "Query ID", Q_NULLPTR));
        idLabel->setText(QApplication::translate("SonyToolWindow", "Unknown ID", Q_NULLPTR));
        statusSenseButton->setText(QApplication::translate("SonyToolWindow", "Status sense", Q_NULLPTR));
        statusLabel->setText(QApplication::translate("SonyToolWindow", "Unknown status", Q_NULLPTR));
        timeSenseButton->setText(QApplication::translate("SonyToolWindow", "Time sense", Q_NULLPTR));
        speedSenseButton->setText(QApplication::translate("SonyToolWindow", "Speed sense", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("SonyToolWindow", "Master", Q_NULLPTR));
        slaveActiveCheck->setText(QApplication::translate("SonyToolWindow", "Receive Sony command", Q_NULLPTR));
        groupBox_2->setTitle(QApplication::translate("SonyToolWindow", "Slave", Q_NULLPTR));
        menuMaster->setTitle(QApplication::translate("SonyToolWindow", "Master", Q_NULLPTR));
        menuSlave->setTitle(QApplication::translate("SonyToolWindow", "Slave", Q_NULLPTR));
        menuTools->setTitle(QApplication::translate("SonyToolWindow", "Tools", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class SonyToolWindow: public Ui_SonyToolWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SONYTOOLWINDOW_H

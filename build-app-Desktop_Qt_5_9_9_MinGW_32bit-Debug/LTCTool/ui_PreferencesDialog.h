/********************************************************************************
** Form generated from reading UI file 'PreferencesDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PREFERENCESDIALOG_H
#define UI_PREFERENCESDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PreferencesDialog
{
public:
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QFormLayout *formLayout;
    QLabel *lblDeviceOut;
    QComboBox *comboBoxOutput;
    QLabel *lblDeviceIn;
    QComboBox *comboBoxInput;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *PreferencesDialog)
    {
        if (PreferencesDialog->objectName().isEmpty())
            PreferencesDialog->setObjectName(QStringLiteral("PreferencesDialog"));
        PreferencesDialog->resize(275, 118);
        layoutWidget = new QWidget(PreferencesDialog);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 10, 261, 101));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        formLayout = new QFormLayout();
        formLayout->setObjectName(QStringLiteral("formLayout"));
        lblDeviceOut = new QLabel(layoutWidget);
        lblDeviceOut->setObjectName(QStringLiteral("lblDeviceOut"));

        formLayout->setWidget(0, QFormLayout::LabelRole, lblDeviceOut);

        comboBoxOutput = new QComboBox(layoutWidget);
        comboBoxOutput->setObjectName(QStringLiteral("comboBoxOutput"));

        formLayout->setWidget(0, QFormLayout::FieldRole, comboBoxOutput);

        lblDeviceIn = new QLabel(layoutWidget);
        lblDeviceIn->setObjectName(QStringLiteral("lblDeviceIn"));

        formLayout->setWidget(1, QFormLayout::LabelRole, lblDeviceIn);

        comboBoxInput = new QComboBox(layoutWidget);
        comboBoxInput->setObjectName(QStringLiteral("comboBoxInput"));

        formLayout->setWidget(1, QFormLayout::FieldRole, comboBoxInput);


        verticalLayout->addLayout(formLayout);

        buttonBox = new QDialogButtonBox(layoutWidget);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(PreferencesDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), PreferencesDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), PreferencesDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(PreferencesDialog);
    } // setupUi

    void retranslateUi(QDialog *PreferencesDialog)
    {
        PreferencesDialog->setWindowTitle(QApplication::translate("PreferencesDialog", "Preferences", Q_NULLPTR));
        lblDeviceOut->setText(QApplication::translate("PreferencesDialog", "Sortie audio", Q_NULLPTR));
        lblDeviceIn->setText(QApplication::translate("PreferencesDialog", "Entr\303\251e audio", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class PreferencesDialog: public Ui_PreferencesDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PREFERENCESDIALOG_H

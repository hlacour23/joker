/********************************************************************************
** Form generated from reading UI file 'LTCToolWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LTCTOOLWINDOW_H
#define UI_LTCTOOLWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "PhCommonUI/PhMediaPanel.h"

QT_BEGIN_NAMESPACE

class Ui_LTCToolWindow
{
public:
    QAction *actionSet_TC_In;
    QAction *actionSet_TC_Out;
    QAction *actionPreferences;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QCheckBox *generateCheckBox;
    QGroupBox *generatorGroupBox;
    QVBoxLayout *verticalLayout_3;
    QLabel *inOutInfoLabel;
    PhMediaPanel *widgetMaster;
    QCheckBox *cBoxLoop;
    QCheckBox *readCheckBox;
    QGroupBox *readerGroupBox;
    QVBoxLayout *verticalLayout_2;
    QLabel *readInfoLabel;
    QLabel *readerTimeCodeLabel;
    QLabel *minMaxLevelLabel;
    QLabel *tcTypelabel;
    QMenuBar *menuBar;
    QMenu *menuControls;

    void setupUi(QMainWindow *LTCToolWindow)
    {
        if (LTCToolWindow->objectName().isEmpty())
            LTCToolWindow->setObjectName(QStringLiteral("LTCToolWindow"));
        LTCToolWindow->resize(523, 376);
        actionSet_TC_In = new QAction(LTCToolWindow);
        actionSet_TC_In->setObjectName(QStringLiteral("actionSet_TC_In"));
        actionSet_TC_Out = new QAction(LTCToolWindow);
        actionSet_TC_Out->setObjectName(QStringLiteral("actionSet_TC_Out"));
        actionPreferences = new QAction(LTCToolWindow);
        actionPreferences->setObjectName(QStringLiteral("actionPreferences"));
        centralWidget = new QWidget(LTCToolWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        generateCheckBox = new QCheckBox(centralWidget);
        generateCheckBox->setObjectName(QStringLiteral("generateCheckBox"));
        generateCheckBox->setChecked(false);

        verticalLayout->addWidget(generateCheckBox);

        generatorGroupBox = new QGroupBox(centralWidget);
        generatorGroupBox->setObjectName(QStringLiteral("generatorGroupBox"));
        verticalLayout_3 = new QVBoxLayout(generatorGroupBox);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        inOutInfoLabel = new QLabel(generatorGroupBox);
        inOutInfoLabel->setObjectName(QStringLiteral("inOutInfoLabel"));
        inOutInfoLabel->setMaximumSize(QSize(16777215, 20));
        inOutInfoLabel->setAlignment(Qt::AlignCenter);

        verticalLayout_3->addWidget(inOutInfoLabel);

        widgetMaster = new PhMediaPanel(generatorGroupBox);
        widgetMaster->setObjectName(QStringLiteral("widgetMaster"));

        verticalLayout_3->addWidget(widgetMaster);

        cBoxLoop = new QCheckBox(generatorGroupBox);
        cBoxLoop->setObjectName(QStringLiteral("cBoxLoop"));

        verticalLayout_3->addWidget(cBoxLoop);


        verticalLayout->addWidget(generatorGroupBox);

        readCheckBox = new QCheckBox(centralWidget);
        readCheckBox->setObjectName(QStringLiteral("readCheckBox"));
        readCheckBox->setChecked(false);

        verticalLayout->addWidget(readCheckBox);

        readerGroupBox = new QGroupBox(centralWidget);
        readerGroupBox->setObjectName(QStringLiteral("readerGroupBox"));
        verticalLayout_2 = new QVBoxLayout(readerGroupBox);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        readInfoLabel = new QLabel(readerGroupBox);
        readInfoLabel->setObjectName(QStringLiteral("readInfoLabel"));
        readInfoLabel->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(readInfoLabel);

        readerTimeCodeLabel = new QLabel(readerGroupBox);
        readerTimeCodeLabel->setObjectName(QStringLiteral("readerTimeCodeLabel"));
        QFont font;
        font.setFamily(QStringLiteral("Arial Unicode MS"));
        font.setPointSize(45);
        font.setStrikeOut(false);
        readerTimeCodeLabel->setFont(font);
        readerTimeCodeLabel->setText(QStringLiteral("00:00:00:00"));
        readerTimeCodeLabel->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(readerTimeCodeLabel);

        minMaxLevelLabel = new QLabel(readerGroupBox);
        minMaxLevelLabel->setObjectName(QStringLiteral("minMaxLevelLabel"));
        minMaxLevelLabel->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(minMaxLevelLabel);

        tcTypelabel = new QLabel(readerGroupBox);
        tcTypelabel->setObjectName(QStringLiteral("tcTypelabel"));
        tcTypelabel->setText(QStringLiteral("??fps"));
        tcTypelabel->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(tcTypelabel);


        verticalLayout->addWidget(readerGroupBox);

        LTCToolWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(LTCToolWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 523, 22));
        menuControls = new QMenu(menuBar);
        menuControls->setObjectName(QStringLiteral("menuControls"));
        LTCToolWindow->setMenuBar(menuBar);

        menuBar->addAction(menuControls->menuAction());
        menuControls->addAction(actionSet_TC_In);
        menuControls->addAction(actionSet_TC_Out);
        menuControls->addAction(actionPreferences);

        retranslateUi(LTCToolWindow);

        QMetaObject::connectSlotsByName(LTCToolWindow);
    } // setupUi

    void retranslateUi(QMainWindow *LTCToolWindow)
    {
        LTCToolWindow->setWindowTitle(QApplication::translate("LTCToolWindow", "LTCToolWindow", Q_NULLPTR));
        actionSet_TC_In->setText(QApplication::translate("LTCToolWindow", "Set TC In...", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionSet_TC_In->setShortcut(QApplication::translate("LTCToolWindow", "Ctrl+Shift+I", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionSet_TC_Out->setText(QApplication::translate("LTCToolWindow", "Set TC Out...", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionSet_TC_Out->setShortcut(QApplication::translate("LTCToolWindow", "Ctrl+Shift+O", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionPreferences->setText(QApplication::translate("LTCToolWindow", "Preferences...", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionPreferences->setShortcut(QApplication::translate("LTCToolWindow", "Ctrl+P", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        generateCheckBox->setText(QApplication::translate("LTCToolWindow", "Generate LTC", Q_NULLPTR));
        generatorGroupBox->setTitle(QApplication::translate("LTCToolWindow", "Generator:", Q_NULLPTR));
        inOutInfoLabel->setText(QApplication::translate("LTCToolWindow", "00:00:00:00 => 00:00:00:00", Q_NULLPTR));
        cBoxLoop->setText(QApplication::translate("LTCToolWindow", "Loop", Q_NULLPTR));
        readCheckBox->setText(QApplication::translate("LTCToolWindow", "Read LTC", Q_NULLPTR));
        readerGroupBox->setTitle(QApplication::translate("LTCToolWindow", "Reader:", Q_NULLPTR));
        readInfoLabel->setText(QApplication::translate("LTCToolWindow", "0x since 00:00:00:00", Q_NULLPTR));
        minMaxLevelLabel->setText(QApplication::translate("LTCToolWindow", "- / -", Q_NULLPTR));
        menuControls->setTitle(QApplication::translate("LTCToolWindow", "Controls", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class LTCToolWindow: public Ui_LTCToolWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LTCTOOLWINDOW_H

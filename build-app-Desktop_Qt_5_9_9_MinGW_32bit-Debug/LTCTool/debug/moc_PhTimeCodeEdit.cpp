/****************************************************************************
** Meta object code from reading C++ file 'PhTimeCodeEdit.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.9)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../libs/PhCommonUI/PhTimeCodeEdit.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'PhTimeCodeEdit.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.9. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_PhTimeCodeEdit_t {
    QByteArrayData data[9];
    char stringdata0[84];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PhTimeCodeEdit_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PhTimeCodeEdit_t qt_meta_stringdata_PhTimeCodeEdit = {
    {
QT_MOC_LITERAL(0, 0, 14), // "PhTimeCodeEdit"
QT_MOC_LITERAL(1, 15, 12), // "frameChanged"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 7), // "PhFrame"
QT_MOC_LITERAL(4, 37, 5), // "frame"
QT_MOC_LITERAL(5, 43, 14), // "PhTimeCodeType"
QT_MOC_LITERAL(6, 58, 6), // "tcType"
QT_MOC_LITERAL(7, 65, 13), // "onTextChanged"
QT_MOC_LITERAL(8, 79, 4) // "text"

    },
    "PhTimeCodeEdit\0frameChanged\0\0PhFrame\0"
    "frame\0PhTimeCodeType\0tcType\0onTextChanged\0"
    "text"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PhTimeCodeEdit[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,   24,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       7,    1,   29,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3, 0x80000000 | 5,    4,    6,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    8,

       0        // eod
};

void PhTimeCodeEdit::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        PhTimeCodeEdit *_t = static_cast<PhTimeCodeEdit *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->frameChanged((*reinterpret_cast< PhFrame(*)>(_a[1])),(*reinterpret_cast< PhTimeCodeType(*)>(_a[2]))); break;
        case 1: _t->onTextChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (PhTimeCodeEdit::*_t)(PhFrame , PhTimeCodeType );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PhTimeCodeEdit::frameChanged)) {
                *result = 0;
                return;
            }
        }
    }
}

const QMetaObject PhTimeCodeEdit::staticMetaObject = {
    { &QLineEdit::staticMetaObject, qt_meta_stringdata_PhTimeCodeEdit.data,
      qt_meta_data_PhTimeCodeEdit,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *PhTimeCodeEdit::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PhTimeCodeEdit::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PhTimeCodeEdit.stringdata0))
        return static_cast<void*>(this);
    return QLineEdit::qt_metacast(_clname);
}

int PhTimeCodeEdit::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QLineEdit::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void PhTimeCodeEdit::frameChanged(PhFrame _t1, PhTimeCodeType _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE

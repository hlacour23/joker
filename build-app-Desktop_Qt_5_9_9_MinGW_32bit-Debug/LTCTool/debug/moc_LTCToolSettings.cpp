/****************************************************************************
** Meta object code from reading C++ file 'LTCToolSettings.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.9)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../app/LTCTool/LTCToolSettings.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'LTCToolSettings.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.9. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_LTCToolSettings_t {
    QByteArrayData data[24];
    char stringdata0[404];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_LTCToolSettings_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_LTCToolSettings_t qt_meta_stringdata_LTCToolSettings = {
    {
QT_MOC_LITERAL(0, 0, 15), // "LTCToolSettings"
QT_MOC_LITERAL(1, 16, 15), // "generateChanged"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 11), // "readChanged"
QT_MOC_LITERAL(4, 45, 25), // "writerTimeCodeTypeChanged"
QT_MOC_LITERAL(5, 71, 19), // "writerTimeInChanged"
QT_MOC_LITERAL(6, 91, 23), // "writerLoopLengthChanged"
QT_MOC_LITERAL(7, 115, 18), // "audioOutputChanged"
QT_MOC_LITERAL(8, 134, 17), // "audioInputChanged"
QT_MOC_LITERAL(9, 152, 14), // "logMaskChanged"
QT_MOC_LITERAL(10, 167, 32), // "ltcAutoDetectTimeCodeTypeChanged"
QT_MOC_LITERAL(11, 200, 19), // "ltcInputPortChanged"
QT_MOC_LITERAL(12, 220, 28), // "ltcReaderTimeCodeTypeChanged"
QT_MOC_LITERAL(13, 249, 8), // "generate"
QT_MOC_LITERAL(14, 258, 4), // "read"
QT_MOC_LITERAL(15, 263, 18), // "writerTimeCodeType"
QT_MOC_LITERAL(16, 282, 12), // "writerTimeIn"
QT_MOC_LITERAL(17, 295, 16), // "writerLoopLength"
QT_MOC_LITERAL(18, 312, 11), // "audioOutput"
QT_MOC_LITERAL(19, 324, 10), // "audioInput"
QT_MOC_LITERAL(20, 335, 7), // "logMask"
QT_MOC_LITERAL(21, 343, 25), // "ltcAutoDetectTimeCodeType"
QT_MOC_LITERAL(22, 369, 12), // "ltcInputPort"
QT_MOC_LITERAL(23, 382, 21) // "ltcReaderTimeCodeType"

    },
    "LTCToolSettings\0generateChanged\0\0"
    "readChanged\0writerTimeCodeTypeChanged\0"
    "writerTimeInChanged\0writerLoopLengthChanged\0"
    "audioOutputChanged\0audioInputChanged\0"
    "logMaskChanged\0ltcAutoDetectTimeCodeTypeChanged\0"
    "ltcInputPortChanged\0ltcReaderTimeCodeTypeChanged\0"
    "generate\0read\0writerTimeCodeType\0"
    "writerTimeIn\0writerLoopLength\0audioOutput\0"
    "audioInput\0logMask\0ltcAutoDetectTimeCodeType\0"
    "ltcInputPort\0ltcReaderTimeCodeType"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_LTCToolSettings[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
      11,   80, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      11,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   69,    2, 0x06 /* Public */,
       3,    0,   70,    2, 0x06 /* Public */,
       4,    0,   71,    2, 0x06 /* Public */,
       5,    0,   72,    2, 0x06 /* Public */,
       6,    0,   73,    2, 0x06 /* Public */,
       7,    0,   74,    2, 0x06 /* Public */,
       8,    0,   75,    2, 0x06 /* Public */,
       9,    0,   76,    2, 0x06 /* Public */,
      10,    0,   77,    2, 0x06 /* Public */,
      11,    0,   78,    2, 0x06 /* Public */,
      12,    0,   79,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      13, QMetaType::Bool, 0x00495103,
      14, QMetaType::Bool, 0x00495103,
      15, QMetaType::Int, 0x00495103,
      16, QMetaType::Int, 0x00495103,
      17, QMetaType::Int, 0x00495103,
      18, QMetaType::QString, 0x00495103,
      19, QMetaType::QString, 0x00495103,
      20, QMetaType::Int, 0x00495103,
      21, QMetaType::Bool, 0x00495103,
      22, QMetaType::QString, 0x00495103,
      23, QMetaType::Int, 0x00495103,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       6,
       7,
       8,
       9,
      10,

       0        // eod
};

void LTCToolSettings::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        LTCToolSettings *_t = static_cast<LTCToolSettings *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->generateChanged(); break;
        case 1: _t->readChanged(); break;
        case 2: _t->writerTimeCodeTypeChanged(); break;
        case 3: _t->writerTimeInChanged(); break;
        case 4: _t->writerLoopLengthChanged(); break;
        case 5: _t->audioOutputChanged(); break;
        case 6: _t->audioInputChanged(); break;
        case 7: _t->logMaskChanged(); break;
        case 8: _t->ltcAutoDetectTimeCodeTypeChanged(); break;
        case 9: _t->ltcInputPortChanged(); break;
        case 10: _t->ltcReaderTimeCodeTypeChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (LTCToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&LTCToolSettings::generateChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (LTCToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&LTCToolSettings::readChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (LTCToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&LTCToolSettings::writerTimeCodeTypeChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (LTCToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&LTCToolSettings::writerTimeInChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (LTCToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&LTCToolSettings::writerLoopLengthChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (LTCToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&LTCToolSettings::audioOutputChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (LTCToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&LTCToolSettings::audioInputChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (LTCToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&LTCToolSettings::logMaskChanged)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (LTCToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&LTCToolSettings::ltcAutoDetectTimeCodeTypeChanged)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (LTCToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&LTCToolSettings::ltcInputPortChanged)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (LTCToolSettings::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&LTCToolSettings::ltcReaderTimeCodeTypeChanged)) {
                *result = 10;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        LTCToolSettings *_t = static_cast<LTCToolSettings *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->generate(); break;
        case 1: *reinterpret_cast< bool*>(_v) = _t->read(); break;
        case 2: *reinterpret_cast< int*>(_v) = _t->writerTimeCodeType(); break;
        case 3: *reinterpret_cast< int*>(_v) = _t->writerTimeIn(); break;
        case 4: *reinterpret_cast< int*>(_v) = _t->writerLoopLength(); break;
        case 5: *reinterpret_cast< QString*>(_v) = _t->audioOutput(); break;
        case 6: *reinterpret_cast< QString*>(_v) = _t->audioInput(); break;
        case 7: *reinterpret_cast< int*>(_v) = _t->logMask(); break;
        case 8: *reinterpret_cast< bool*>(_v) = _t->ltcAutoDetectTimeCodeType(); break;
        case 9: *reinterpret_cast< QString*>(_v) = _t->ltcInputPort(); break;
        case 10: *reinterpret_cast< int*>(_v) = _t->ltcReaderTimeCodeType(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        LTCToolSettings *_t = static_cast<LTCToolSettings *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setGenerate(*reinterpret_cast< bool*>(_v)); break;
        case 1: _t->setRead(*reinterpret_cast< bool*>(_v)); break;
        case 2: _t->setWriterTimeCodeType(*reinterpret_cast< int*>(_v)); break;
        case 3: _t->setWriterTimeIn(*reinterpret_cast< int*>(_v)); break;
        case 4: _t->setWriterLoopLength(*reinterpret_cast< int*>(_v)); break;
        case 5: _t->setAudioOutput(*reinterpret_cast< QString*>(_v)); break;
        case 6: _t->setAudioInput(*reinterpret_cast< QString*>(_v)); break;
        case 7: _t->setLogMask(*reinterpret_cast< int*>(_v)); break;
        case 8: _t->setLtcAutoDetectTimeCodeType(*reinterpret_cast< bool*>(_v)); break;
        case 9: _t->setLtcInputPort(*reinterpret_cast< QString*>(_v)); break;
        case 10: _t->setLtcReaderTimeCodeType(*reinterpret_cast< int*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_a);
}

const QMetaObject LTCToolSettings::staticMetaObject = {
    { &PhGenericSettings::staticMetaObject, qt_meta_stringdata_LTCToolSettings.data,
      qt_meta_data_LTCToolSettings,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *LTCToolSettings::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *LTCToolSettings::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_LTCToolSettings.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "PhLtcReaderSettings"))
        return static_cast< PhLtcReaderSettings*>(this);
    return PhGenericSettings::qt_metacast(_clname);
}

int LTCToolSettings::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = PhGenericSettings::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 11;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 11;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void LTCToolSettings::generateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void LTCToolSettings::readChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void LTCToolSettings::writerTimeCodeTypeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void LTCToolSettings::writerTimeInChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void LTCToolSettings::writerLoopLengthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void LTCToolSettings::audioOutputChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void LTCToolSettings::audioInputChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void LTCToolSettings::logMaskChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void LTCToolSettings::ltcAutoDetectTimeCodeTypeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void LTCToolSettings::ltcInputPortChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, nullptr);
}

// SIGNAL 10
void LTCToolSettings::ltcReaderTimeCodeTypeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE

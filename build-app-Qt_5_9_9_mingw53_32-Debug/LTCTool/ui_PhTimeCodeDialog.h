/********************************************************************************
** Form generated from reading UI file 'PhTimeCodeDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PHTIMECODEDIALOG_H
#define UI_PHTIMECODEDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include "../../libs/PhCommonUI/PhTimeCodeEdit.h"

QT_BEGIN_NAMESPACE

class Ui_PhTimeCodeDialog
{
public:
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    PhTimeCodeEdit *_timecodeEdit;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *cancelButton;
    QPushButton *okButton;

    void setupUi(QDialog *PhTimeCodeDialog)
    {
        if (PhTimeCodeDialog->objectName().isEmpty())
            PhTimeCodeDialog->setObjectName(QStringLiteral("PhTimeCodeDialog"));
        PhTimeCodeDialog->resize(425, 101);
        horizontalLayout = new QHBoxLayout(PhTimeCodeDialog);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        _timecodeEdit = new PhTimeCodeEdit(PhTimeCodeDialog);
        _timecodeEdit->setObjectName(QStringLiteral("_timecodeEdit"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(_timecodeEdit->sizePolicy().hasHeightForWidth());
        _timecodeEdit->setSizePolicy(sizePolicy);
        QFont font;
        font.setPointSize(25);
        _timecodeEdit->setFont(font);
        _timecodeEdit->setInputMask(QStringLiteral(""));
        _timecodeEdit->setText(QStringLiteral(""));
        _timecodeEdit->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(_timecodeEdit);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        cancelButton = new QPushButton(PhTimeCodeDialog);
        cancelButton->setObjectName(QStringLiteral("cancelButton"));

        horizontalLayout_2->addWidget(cancelButton);

        okButton = new QPushButton(PhTimeCodeDialog);
        okButton->setObjectName(QStringLiteral("okButton"));

        horizontalLayout_2->addWidget(okButton);


        verticalLayout->addLayout(horizontalLayout_2);


        horizontalLayout->addLayout(verticalLayout);


        retranslateUi(PhTimeCodeDialog);

        QMetaObject::connectSlotsByName(PhTimeCodeDialog);
    } // setupUi

    void retranslateUi(QDialog *PhTimeCodeDialog)
    {
        PhTimeCodeDialog->setWindowTitle(QApplication::translate("PhTimeCodeDialog", "Go To", Q_NULLPTR));
        cancelButton->setText(QApplication::translate("PhTimeCodeDialog", "Cancel", Q_NULLPTR));
        okButton->setText(QApplication::translate("PhTimeCodeDialog", "Ok", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class PhTimeCodeDialog: public Ui_PhTimeCodeDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PHTIMECODEDIALOG_H

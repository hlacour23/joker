/********************************************************************************
** Form generated from reading UI file 'JokerWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_JOKERWINDOW_H
#define UI_JOKERWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "../../libs/PhCommonUI/PhHelpMenu.h"
#include "PhGraphic/PhGraphicView.h"

QT_BEGIN_NAMESPACE

class Ui_JokerWindow
{
public:
    QAction *actionOpen;
    QAction *actionPlay_pause;
    QAction *actionPlay_backward;
    QAction *actionStep_forward;
    QAction *actionStep_backward;
    QAction *actionStep_time_forward;
    QAction *actionStep_time_backward;
    QAction *action_3;
    QAction *action_1;
    QAction *action_0_5;
    QAction *action0;
    QAction *action0_5;
    QAction *action1;
    QAction *action3;
    QAction *actionOpen_Video;
    QAction *actionChange_timestamp;
    QAction *actionChange_font;
    QAction *actionAbout;
    QAction *actionPreferences;
    QAction *actionProperties;
    QAction *actionTest_mode;
    QAction *actionTimecode;
    QAction *actionNext_text;
    QAction *actionPrevious_text;
    QAction *actionNext_element;
    QAction *actionPrevious_element;
    QAction *actionSave;
    QAction *actionSave_as;
    QAction *actionSelect_character;
    QAction *actionForce_16_9_ratio;
    QAction *actionFullscreen;
    QAction *actionEmpty;
    QAction *actionInvert_colors;
    QAction *actionNew;
    QAction *actionClose_video;
    QAction *actionSend_feedback;
    QAction *actionDeinterlace_video;
    QAction *actionHide_the_rythmo;
    QAction *actionNext_loop;
    QAction *actionPrevious_loop;
    QAction *actionDisplay_the_cuts;
    QAction *actionDisplay_the_vertical_scale;
    QAction *actionDisplay_the_information_panel;
    QAction *actionDisplay_the_control_panel;
    QAction *actionHide_selected_peoples;
    QAction *actionDisplay_feet;
    QAction *actionSet_first_foot_timecode;
    QAction *actionSet_distance_between_two_feet;
    QAction *actionUse_native_video_size;
    QAction *actionSet_TC_in;
    QAction *actionSet_TC_out;
    QAction *actionLoop;
    QAction *actionPicture_in_picture;
    QAction *actionSecond_screen;
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    PhGraphicView *videoStripView;
    QMenuBar *menubar;
    QMenu *menuFile;
    QMenu *menuOpen_recent;
    QMenu *menuControl;
    QMenu *menuVarial_Speed;
    QMenu *menuGo_to;
    QMenu *menuView;
    PhHelpMenu *menuHelp;
    QMenu *menuVideo;
    QMenu *menuRythmo;

    void setupUi(QMainWindow *JokerWindow)
    {
        if (JokerWindow->objectName().isEmpty())
            JokerWindow->setObjectName(QStringLiteral("JokerWindow"));
        JokerWindow->resize(820, 648);
        actionOpen = new QAction(JokerWindow);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        actionPlay_pause = new QAction(JokerWindow);
        actionPlay_pause->setObjectName(QStringLiteral("actionPlay_pause"));
        actionPlay_backward = new QAction(JokerWindow);
        actionPlay_backward->setObjectName(QStringLiteral("actionPlay_backward"));
        actionStep_forward = new QAction(JokerWindow);
        actionStep_forward->setObjectName(QStringLiteral("actionStep_forward"));
        actionStep_backward = new QAction(JokerWindow);
        actionStep_backward->setObjectName(QStringLiteral("actionStep_backward"));
        actionStep_time_forward = new QAction(JokerWindow);
        actionStep_time_forward->setObjectName(QStringLiteral("actionStep_time_forward"));
        actionStep_time_backward = new QAction(JokerWindow);
        actionStep_time_backward->setObjectName(QStringLiteral("actionStep_time_backward"));
        action_3 = new QAction(JokerWindow);
        action_3->setObjectName(QStringLiteral("action_3"));
        action_3->setText(QStringLiteral("-3"));
        action_1 = new QAction(JokerWindow);
        action_1->setObjectName(QStringLiteral("action_1"));
        action_1->setText(QStringLiteral("-1"));
        action_0_5 = new QAction(JokerWindow);
        action_0_5->setObjectName(QStringLiteral("action_0_5"));
        action_0_5->setText(QStringLiteral("-0.5"));
        action0 = new QAction(JokerWindow);
        action0->setObjectName(QStringLiteral("action0"));
        action0->setText(QStringLiteral("0"));
        action0_5 = new QAction(JokerWindow);
        action0_5->setObjectName(QStringLiteral("action0_5"));
        action0_5->setText(QStringLiteral("0.5"));
        action1 = new QAction(JokerWindow);
        action1->setObjectName(QStringLiteral("action1"));
        action1->setText(QStringLiteral("1"));
        action3 = new QAction(JokerWindow);
        action3->setObjectName(QStringLiteral("action3"));
        action3->setText(QStringLiteral("3"));
        actionOpen_Video = new QAction(JokerWindow);
        actionOpen_Video->setObjectName(QStringLiteral("actionOpen_Video"));
        actionChange_timestamp = new QAction(JokerWindow);
        actionChange_timestamp->setObjectName(QStringLiteral("actionChange_timestamp"));
        actionChange_font = new QAction(JokerWindow);
        actionChange_font->setObjectName(QStringLiteral("actionChange_font"));
        actionAbout = new QAction(JokerWindow);
        actionAbout->setObjectName(QStringLiteral("actionAbout"));
        actionPreferences = new QAction(JokerWindow);
        actionPreferences->setObjectName(QStringLiteral("actionPreferences"));
        actionProperties = new QAction(JokerWindow);
        actionProperties->setObjectName(QStringLiteral("actionProperties"));
        actionTest_mode = new QAction(JokerWindow);
        actionTest_mode->setObjectName(QStringLiteral("actionTest_mode"));
        actionTest_mode->setCheckable(true);
        actionTimecode = new QAction(JokerWindow);
        actionTimecode->setObjectName(QStringLiteral("actionTimecode"));
        actionNext_text = new QAction(JokerWindow);
        actionNext_text->setObjectName(QStringLiteral("actionNext_text"));
        actionPrevious_text = new QAction(JokerWindow);
        actionPrevious_text->setObjectName(QStringLiteral("actionPrevious_text"));
        actionNext_element = new QAction(JokerWindow);
        actionNext_element->setObjectName(QStringLiteral("actionNext_element"));
        actionPrevious_element = new QAction(JokerWindow);
        actionPrevious_element->setObjectName(QStringLiteral("actionPrevious_element"));
        actionSave = new QAction(JokerWindow);
        actionSave->setObjectName(QStringLiteral("actionSave"));
        actionSave->setEnabled(true);
        actionSave_as = new QAction(JokerWindow);
        actionSave_as->setObjectName(QStringLiteral("actionSave_as"));
        actionSave_as->setEnabled(true);
        actionSelect_character = new QAction(JokerWindow);
        actionSelect_character->setObjectName(QStringLiteral("actionSelect_character"));
        actionForce_16_9_ratio = new QAction(JokerWindow);
        actionForce_16_9_ratio->setObjectName(QStringLiteral("actionForce_16_9_ratio"));
        actionForce_16_9_ratio->setCheckable(true);
        actionFullscreen = new QAction(JokerWindow);
        actionFullscreen->setObjectName(QStringLiteral("actionFullscreen"));
        actionFullscreen->setCheckable(true);
        actionEmpty = new QAction(JokerWindow);
        actionEmpty->setObjectName(QStringLiteral("actionEmpty"));
        actionEmpty->setEnabled(false);
        actionInvert_colors = new QAction(JokerWindow);
        actionInvert_colors->setObjectName(QStringLiteral("actionInvert_colors"));
        actionInvert_colors->setCheckable(true);
        actionNew = new QAction(JokerWindow);
        actionNew->setObjectName(QStringLiteral("actionNew"));
        actionClose_video = new QAction(JokerWindow);
        actionClose_video->setObjectName(QStringLiteral("actionClose_video"));
        actionSend_feedback = new QAction(JokerWindow);
        actionSend_feedback->setObjectName(QStringLiteral("actionSend_feedback"));
        actionDeinterlace_video = new QAction(JokerWindow);
        actionDeinterlace_video->setObjectName(QStringLiteral("actionDeinterlace_video"));
        actionDeinterlace_video->setCheckable(true);
        actionHide_the_rythmo = new QAction(JokerWindow);
        actionHide_the_rythmo->setObjectName(QStringLiteral("actionHide_the_rythmo"));
        actionHide_the_rythmo->setCheckable(true);
        actionNext_loop = new QAction(JokerWindow);
        actionNext_loop->setObjectName(QStringLiteral("actionNext_loop"));
        actionPrevious_loop = new QAction(JokerWindow);
        actionPrevious_loop->setObjectName(QStringLiteral("actionPrevious_loop"));
        actionDisplay_the_cuts = new QAction(JokerWindow);
        actionDisplay_the_cuts->setObjectName(QStringLiteral("actionDisplay_the_cuts"));
        actionDisplay_the_cuts->setCheckable(true);
        actionDisplay_the_vertical_scale = new QAction(JokerWindow);
        actionDisplay_the_vertical_scale->setObjectName(QStringLiteral("actionDisplay_the_vertical_scale"));
        actionDisplay_the_vertical_scale->setCheckable(true);
        actionDisplay_the_information_panel = new QAction(JokerWindow);
        actionDisplay_the_information_panel->setObjectName(QStringLiteral("actionDisplay_the_information_panel"));
        actionDisplay_the_information_panel->setCheckable(true);
        actionDisplay_the_control_panel = new QAction(JokerWindow);
        actionDisplay_the_control_panel->setObjectName(QStringLiteral("actionDisplay_the_control_panel"));
        actionDisplay_the_control_panel->setCheckable(true);
        actionHide_selected_peoples = new QAction(JokerWindow);
        actionHide_selected_peoples->setObjectName(QStringLiteral("actionHide_selected_peoples"));
        actionHide_selected_peoples->setCheckable(true);
        actionDisplay_feet = new QAction(JokerWindow);
        actionDisplay_feet->setObjectName(QStringLiteral("actionDisplay_feet"));
        actionDisplay_feet->setCheckable(true);
        actionSet_first_foot_timecode = new QAction(JokerWindow);
        actionSet_first_foot_timecode->setObjectName(QStringLiteral("actionSet_first_foot_timecode"));
        actionSet_distance_between_two_feet = new QAction(JokerWindow);
        actionSet_distance_between_two_feet->setObjectName(QStringLiteral("actionSet_distance_between_two_feet"));
        actionUse_native_video_size = new QAction(JokerWindow);
        actionUse_native_video_size->setObjectName(QStringLiteral("actionUse_native_video_size"));
        actionUse_native_video_size->setCheckable(true);
        actionSet_TC_in = new QAction(JokerWindow);
        actionSet_TC_in->setObjectName(QStringLiteral("actionSet_TC_in"));
        actionSet_TC_out = new QAction(JokerWindow);
        actionSet_TC_out->setObjectName(QStringLiteral("actionSet_TC_out"));
        actionLoop = new QAction(JokerWindow);
        actionLoop->setObjectName(QStringLiteral("actionLoop"));
        actionLoop->setCheckable(true);
        actionPicture_in_picture = new QAction(JokerWindow);
        actionPicture_in_picture->setObjectName(QStringLiteral("actionPicture_in_picture"));
        actionPicture_in_picture->setCheckable(true);
        actionSecond_screen = new QAction(JokerWindow);
        actionSecond_screen->setObjectName(QStringLiteral("actionSecond_screen"));
        actionSecond_screen->setCheckable(true);
        centralwidget = new QWidget(JokerWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        verticalLayout = new QVBoxLayout(centralwidget);
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        videoStripView = new PhGraphicView(centralwidget);
        videoStripView->setObjectName(QStringLiteral("videoStripView"));

        verticalLayout->addWidget(videoStripView);

        verticalLayout->setStretch(0, 1);
        JokerWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(JokerWindow);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 820, 22));
        menuFile = new QMenu(menubar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuOpen_recent = new QMenu(menuFile);
        menuOpen_recent->setObjectName(QStringLiteral("menuOpen_recent"));
        menuControl = new QMenu(menubar);
        menuControl->setObjectName(QStringLiteral("menuControl"));
        menuVarial_Speed = new QMenu(menuControl);
        menuVarial_Speed->setObjectName(QStringLiteral("menuVarial_Speed"));
        menuGo_to = new QMenu(menuControl);
        menuGo_to->setObjectName(QStringLiteral("menuGo_to"));
        menuView = new QMenu(menubar);
        menuView->setObjectName(QStringLiteral("menuView"));
        menuHelp = new PhHelpMenu(menubar);
        menuHelp->setObjectName(QStringLiteral("menuHelp"));
        menuVideo = new QMenu(menubar);
        menuVideo->setObjectName(QStringLiteral("menuVideo"));
        menuRythmo = new QMenu(menubar);
        menuRythmo->setObjectName(QStringLiteral("menuRythmo"));
        JokerWindow->setMenuBar(menubar);

        menubar->addAction(menuFile->menuAction());
        menubar->addAction(menuControl->menuAction());
        menubar->addAction(menuRythmo->menuAction());
        menubar->addAction(menuVideo->menuAction());
        menubar->addAction(menuView->menuAction());
        menubar->addAction(menuHelp->menuAction());
        menuFile->addAction(actionNew);
        menuFile->addSeparator();
        menuFile->addAction(actionOpen);
        menuFile->addAction(menuOpen_recent->menuAction());
        menuFile->addSeparator();
        menuFile->addAction(actionOpen_Video);
        menuFile->addAction(actionClose_video);
        menuFile->addSeparator();
        menuFile->addAction(actionSave);
        menuFile->addAction(actionSave_as);
        menuFile->addSeparator();
        menuFile->addAction(actionPreferences);
        menuFile->addAction(actionProperties);
        menuOpen_recent->addAction(actionEmpty);
        menuControl->addAction(actionPlay_pause);
        menuControl->addAction(actionPlay_backward);
        menuControl->addAction(actionStep_forward);
        menuControl->addAction(actionStep_backward);
        menuControl->addAction(actionStep_time_forward);
        menuControl->addAction(actionStep_time_backward);
        menuControl->addAction(menuVarial_Speed->menuAction());
        menuControl->addAction(actionChange_timestamp);
        menuControl->addAction(menuGo_to->menuAction());
        menuControl->addSeparator();
        menuControl->addAction(actionSet_TC_in);
        menuControl->addAction(actionSet_TC_out);
        menuControl->addAction(actionLoop);
        menuVarial_Speed->addAction(action_3);
        menuVarial_Speed->addAction(action_1);
        menuVarial_Speed->addAction(action_0_5);
        menuVarial_Speed->addAction(action0);
        menuVarial_Speed->addAction(action0_5);
        menuVarial_Speed->addAction(action1);
        menuVarial_Speed->addAction(action3);
        menuGo_to->addAction(actionTimecode);
        menuGo_to->addAction(actionNext_element);
        menuGo_to->addAction(actionPrevious_element);
        menuGo_to->addAction(actionNext_loop);
        menuGo_to->addAction(actionPrevious_loop);
        menuView->addAction(actionFullscreen);
        menuView->addAction(actionDisplay_the_control_panel);
        menuView->addAction(actionDisplay_the_information_panel);
        menuView->addSeparator();
        menuHelp->addAction(actionSend_feedback);
        menuHelp->addAction(actionAbout);
        menuVideo->addAction(actionForce_16_9_ratio);
        menuVideo->addAction(actionDeinterlace_video);
        menuVideo->addAction(actionUse_native_video_size);
        menuVideo->addAction(actionPicture_in_picture);
        menuVideo->addAction(actionSecond_screen);
        menuRythmo->addAction(actionSelect_character);
        menuRythmo->addAction(actionHide_selected_peoples);
        menuRythmo->addSeparator();
        menuRythmo->addAction(actionHide_the_rythmo);
        menuRythmo->addAction(actionInvert_colors);
        menuRythmo->addAction(actionDisplay_the_cuts);
        menuRythmo->addAction(actionDisplay_the_vertical_scale);
        menuRythmo->addSeparator();
        menuRythmo->addAction(actionDisplay_feet);
        menuRythmo->addAction(actionSet_first_foot_timecode);
        menuRythmo->addAction(actionSet_distance_between_two_feet);

        retranslateUi(JokerWindow);

        QMetaObject::connectSlotsByName(JokerWindow);
    } // setupUi

    void retranslateUi(QMainWindow *JokerWindow)
    {
        JokerWindow->setWindowTitle(QApplication::translate("JokerWindow", "Joker", Q_NULLPTR));
        actionOpen->setText(QApplication::translate("JokerWindow", "Open...", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionOpen->setShortcut(QApplication::translate("JokerWindow", "Ctrl+O", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionPlay_pause->setText(QApplication::translate("JokerWindow", "Play/pause", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionPlay_pause->setShortcut(QApplication::translate("JokerWindow", "Space", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionPlay_backward->setText(QApplication::translate("JokerWindow", "Play backward", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionPlay_backward->setShortcut(QApplication::translate("JokerWindow", "Down", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionStep_forward->setText(QApplication::translate("JokerWindow", "Step forward", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionStep_forward->setShortcut(QApplication::translate("JokerWindow", "Right", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionStep_backward->setText(QApplication::translate("JokerWindow", "Step backward", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionStep_backward->setShortcut(QApplication::translate("JokerWindow", "Left", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionStep_time_forward->setText(QApplication::translate("JokerWindow", "Step time forward", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionStep_time_forward->setShortcut(QApplication::translate("JokerWindow", "Alt+Right", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionStep_time_backward->setText(QApplication::translate("JokerWindow", "Step time backward", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionStep_time_backward->setShortcut(QApplication::translate("JokerWindow", "Alt+Left", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionOpen_Video->setText(QApplication::translate("JokerWindow", "Open video file...", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionOpen_Video->setShortcut(QApplication::translate("JokerWindow", "Ctrl+Shift+O", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionChange_timestamp->setText(QApplication::translate("JokerWindow", "Change timestamp...", Q_NULLPTR));
        actionChange_font->setText(QApplication::translate("JokerWindow", "Change font...", Q_NULLPTR));
        actionAbout->setText(QApplication::translate("JokerWindow", "About...", Q_NULLPTR));
        actionPreferences->setText(QApplication::translate("JokerWindow", "Preferences...", Q_NULLPTR));
        actionProperties->setText(QApplication::translate("JokerWindow", "Properties...", Q_NULLPTR));
        actionTest_mode->setText(QApplication::translate("JokerWindow", "Test mode", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        actionTest_mode->setToolTip(QApplication::translate("JokerWindow", "Set Joker on Test mode for syncheck", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionTest_mode->setShortcut(QApplication::translate("JokerWindow", "Ctrl+Shift+T", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionTimecode->setText(QApplication::translate("JokerWindow", "Timecode...", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionTimecode->setShortcut(QApplication::translate("JokerWindow", "Ctrl+G", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionNext_text->setText(QApplication::translate("JokerWindow", "Next text", Q_NULLPTR));
        actionPrevious_text->setText(QApplication::translate("JokerWindow", "Previous text", Q_NULLPTR));
        actionNext_element->setText(QApplication::translate("JokerWindow", "Next element", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionNext_element->setShortcut(QApplication::translate("JokerWindow", "Ctrl+Right", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionPrevious_element->setText(QApplication::translate("JokerWindow", "Previous element", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionPrevious_element->setShortcut(QApplication::translate("JokerWindow", "Ctrl+Left", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionSave->setText(QApplication::translate("JokerWindow", "Save", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionSave->setShortcut(QApplication::translate("JokerWindow", "Ctrl+S", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionSave_as->setText(QApplication::translate("JokerWindow", "Save as...", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionSave_as->setShortcut(QApplication::translate("JokerWindow", "Ctrl+Shift+S", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionSelect_character->setText(QApplication::translate("JokerWindow", "Select character...", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionSelect_character->setShortcut(QApplication::translate("JokerWindow", "Ctrl+R", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionForce_16_9_ratio->setText(QApplication::translate("JokerWindow", "Force 16/9 ratio", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionForce_16_9_ratio->setShortcut(QApplication::translate("JokerWindow", "F8", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionFullscreen->setText(QApplication::translate("JokerWindow", "Fullscreen", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionFullscreen->setShortcut(QApplication::translate("JokerWindow", "F10", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionEmpty->setText(QApplication::translate("JokerWindow", "Empty", Q_NULLPTR));
        actionInvert_colors->setText(QApplication::translate("JokerWindow", "Invert colors", Q_NULLPTR));
        actionNew->setText(QApplication::translate("JokerWindow", "New", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionNew->setShortcut(QApplication::translate("JokerWindow", "Ctrl+N", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionClose_video->setText(QApplication::translate("JokerWindow", "Close video", Q_NULLPTR));
        actionSend_feedback->setText(QApplication::translate("JokerWindow", "Send feedback...", Q_NULLPTR));
        actionDeinterlace_video->setText(QApplication::translate("JokerWindow", "Deinterlace video", Q_NULLPTR));
        actionHide_the_rythmo->setText(QApplication::translate("JokerWindow", "Hide the rythmo", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionHide_the_rythmo->setShortcut(QApplication::translate("JokerWindow", "F9", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionNext_loop->setText(QApplication::translate("JokerWindow", "Next loop", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionNext_loop->setShortcut(QApplication::translate("JokerWindow", "Ctrl+Up", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionPrevious_loop->setText(QApplication::translate("JokerWindow", "Previous loop", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionPrevious_loop->setShortcut(QApplication::translate("JokerWindow", "Ctrl+Down", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionDisplay_the_cuts->setText(QApplication::translate("JokerWindow", "Display the cuts", Q_NULLPTR));
        actionDisplay_the_vertical_scale->setText(QApplication::translate("JokerWindow", "Display the vertical scale", Q_NULLPTR));
        actionDisplay_the_information_panel->setText(QApplication::translate("JokerWindow", "Display the information panel", Q_NULLPTR));
        actionDisplay_the_control_panel->setText(QApplication::translate("JokerWindow", "Display the control panel", Q_NULLPTR));
        actionHide_selected_peoples->setText(QApplication::translate("JokerWindow", "Hide selected peoples", Q_NULLPTR));
        actionDisplay_feet->setText(QApplication::translate("JokerWindow", "Display feet", Q_NULLPTR));
        actionSet_first_foot_timecode->setText(QApplication::translate("JokerWindow", "Set first foot timecode...", Q_NULLPTR));
        actionSet_distance_between_two_feet->setText(QApplication::translate("JokerWindow", "Set distance between two feet...", Q_NULLPTR));
        actionUse_native_video_size->setText(QApplication::translate("JokerWindow", "Use native video size", Q_NULLPTR));
        actionSet_TC_in->setText(QApplication::translate("JokerWindow", "Set TC in", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionSet_TC_in->setShortcut(QApplication::translate("JokerWindow", "Ctrl+J", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionSet_TC_out->setText(QApplication::translate("JokerWindow", "Set TC out", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionSet_TC_out->setShortcut(QApplication::translate("JokerWindow", "Ctrl+K", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionLoop->setText(QApplication::translate("JokerWindow", "Loop", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionLoop->setShortcut(QApplication::translate("JokerWindow", "Ctrl+L", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionPicture_in_picture->setText(QApplication::translate("JokerWindow", "Picture in picture", Q_NULLPTR));
        actionSecond_screen->setText(QApplication::translate("JokerWindow", "Second screen", Q_NULLPTR));
        menuFile->setTitle(QApplication::translate("JokerWindow", "File", Q_NULLPTR));
        menuOpen_recent->setTitle(QApplication::translate("JokerWindow", "Open recent", Q_NULLPTR));
        menuControl->setTitle(QApplication::translate("JokerWindow", "Control", Q_NULLPTR));
        menuVarial_Speed->setTitle(QApplication::translate("JokerWindow", "Varispeed", Q_NULLPTR));
        menuGo_to->setTitle(QApplication::translate("JokerWindow", "Go to", Q_NULLPTR));
        menuView->setTitle(QApplication::translate("JokerWindow", "View", Q_NULLPTR));
        menuHelp->setTitle(QApplication::translate("JokerWindow", "Help", Q_NULLPTR));
        menuVideo->setTitle(QApplication::translate("JokerWindow", "Video", Q_NULLPTR));
        menuRythmo->setTitle(QApplication::translate("JokerWindow", "Rythmo", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class JokerWindow: public Ui_JokerWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_JOKERWINDOW_H

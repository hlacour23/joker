/********************************************************************************
** Form generated from reading UI file 'PeopleDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PEOPLEDIALOG_H
#define UI_PEOPLEDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include "PhCommonUI/PhDialogButtonBox.h"

QT_BEGIN_NAMESPACE

class Ui_PeopleDialog
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QListWidget *peopleList;
    QPushButton *deselectAllButton;
    QPushButton *changeCharButton;
    PhDialogButtonBox *buttonBox;

    void setupUi(QDialog *PeopleDialog)
    {
        if (PeopleDialog->objectName().isEmpty())
            PeopleDialog->setObjectName(QStringLiteral("PeopleDialog"));
        PeopleDialog->resize(392, 356);
        verticalLayout = new QVBoxLayout(PeopleDialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label = new QLabel(PeopleDialog);
        label->setObjectName(QStringLiteral("label"));
        label->setWordWrap(true);

        verticalLayout->addWidget(label);

        peopleList = new QListWidget(PeopleDialog);
        peopleList->setObjectName(QStringLiteral("peopleList"));
        peopleList->setSelectionMode(QAbstractItemView::ExtendedSelection);

        verticalLayout->addWidget(peopleList);

        deselectAllButton = new QPushButton(PeopleDialog);
        deselectAllButton->setObjectName(QStringLiteral("deselectAllButton"));

        verticalLayout->addWidget(deselectAllButton);

        changeCharButton = new QPushButton(PeopleDialog);
        changeCharButton->setObjectName(QStringLiteral("changeCharButton"));

        verticalLayout->addWidget(changeCharButton);

        buttonBox = new PhDialogButtonBox(PeopleDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(PeopleDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), PeopleDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), PeopleDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(PeopleDialog);
    } // setupUi

    void retranslateUi(QDialog *PeopleDialog)
    {
        PeopleDialog->setWindowTitle(QApplication::translate("PeopleDialog", "Select a character", Q_NULLPTR));
        label->setText(QApplication::translate("PeopleDialog", "Use command key to select multiple character:", Q_NULLPTR));
        deselectAllButton->setText(QApplication::translate("PeopleDialog", "Unselect all", Q_NULLPTR));
        changeCharButton->setText(QApplication::translate("PeopleDialog", "Modify", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class PeopleDialog: public Ui_PeopleDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PEOPLEDIALOG_H
